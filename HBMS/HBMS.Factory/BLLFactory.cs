﻿// 
// 创建人：宋欣
// 创建时间：2015-01-30
// 功能：工厂模式创建bll
// web.config 需要加入配置：(利用工厂模式+反射机制+缓存机制,实现动态创建不同的数据层对象接口) 
// DataCache类在导出代码的文件夹里
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Reflection;
using HBMS.Common;
using HBMS.BLL;

namespace HBMS.Factory
{
    public class BLLFactory
    {
        private static readonly string AssemblyPath = ConfigHelper.GetAppSettingString("BLL");

        /// <summary>
        ///     创建对象或从缓存获取
        ///   <appSettings>
        ///         <add key="BLL" value="HBMS.BLL" /> (这里的命名空间根据实际情况更改为自己项目的命名空间)
        ///     </appSettings>
        /// </summary>
        public static object CreateObject(string ClassNamespace)
        {
            object objType = DataCache.GetCache(ClassNamespace); //从缓存读取
            if (objType == null)
            {
                try
                {
                    objType = Assembly.Load(AssemblyPath).CreateInstance(ClassNamespace); //反射创建
                    DataCache.SetCache(ClassNamespace, objType); // 写入缓存
                }
                catch
                {
                }
            }
            return objType;
        }

        /// <summary>
        /// 创建无构造函数实例化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T CreateInstance<T>()
        {
            return System.Activator.CreateInstance<T>();
        }
    }
}