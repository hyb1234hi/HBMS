﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_RoleMenu
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/5 23:51:29   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_RoleMenu
	/// </summary>
	public partial class View_RoleMenu
	{
		public View_RoleMenu()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_RoleMenu model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_RoleMenu(");
			strSql.Append("RoleID,MenuID,spras,RoleName,IsRoleUse,IsAdmin,RoleType,MenuName,URL,ParentID,OrderIndex,IsHide,IsMenuUse)");
			strSql.Append(" values (");
			strSql.Append("@RoleID,@MenuID,@spras,@RoleName,@IsRoleUse,@IsAdmin,@RoleType,@MenuName,@URL,@ParentID,@OrderIndex,@IsHide,@IsMenuUse)");
			SqlParameter[] parameters = {
					new SqlParameter("@RoleID", SqlDbType.Int,4),
					new SqlParameter("@MenuID", SqlDbType.VarChar,10),
					new SqlParameter("@spras", SqlDbType.Int,4),
					new SqlParameter("@RoleName", SqlDbType.VarChar,100),
					new SqlParameter("@IsRoleUse", SqlDbType.Bit,1),
					new SqlParameter("@IsAdmin", SqlDbType.Bit,1),
					new SqlParameter("@RoleType", SqlDbType.Int,4),
					new SqlParameter("@MenuName", SqlDbType.VarChar,200),
					new SqlParameter("@URL", SqlDbType.VarChar,200),
					new SqlParameter("@ParentID", SqlDbType.VarChar,10),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@IsHide", SqlDbType.Bit,1),
					new SqlParameter("@IsMenuUse", SqlDbType.Bit,1)};
			parameters[0].Value = model.RoleID;
			parameters[1].Value = model.MenuID;
			parameters[2].Value = model.spras;
			parameters[3].Value = model.RoleName;
			parameters[4].Value = model.IsRoleUse;
			parameters[5].Value = model.IsAdmin;
			parameters[6].Value = model.RoleType;
			parameters[7].Value = model.MenuName;
			parameters[8].Value = model.URL;
			parameters[9].Value = model.ParentID;
			parameters[10].Value = model.OrderIndex;
			parameters[11].Value = model.IsHide;
			parameters[12].Value = model.IsMenuUse;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_RoleMenu model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_RoleMenu set ");
			strSql.Append("RoleID=@RoleID,");
			strSql.Append("MenuID=@MenuID,");
			strSql.Append("spras=@spras,");
			strSql.Append("RoleName=@RoleName,");
			strSql.Append("IsRoleUse=@IsRoleUse,");
			strSql.Append("IsAdmin=@IsAdmin,");
			strSql.Append("RoleType=@RoleType,");
			strSql.Append("MenuName=@MenuName,");
			strSql.Append("URL=@URL,");
			strSql.Append("ParentID=@ParentID,");
			strSql.Append("OrderIndex=@OrderIndex,");
			strSql.Append("IsHide=@IsHide,");
			strSql.Append("IsMenuUse=@IsMenuUse");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@RoleID", SqlDbType.Int,4),
					new SqlParameter("@MenuID", SqlDbType.VarChar,10),
					new SqlParameter("@spras", SqlDbType.Int,4),
					new SqlParameter("@RoleName", SqlDbType.VarChar,100),
					new SqlParameter("@IsRoleUse", SqlDbType.Bit,1),
					new SqlParameter("@IsAdmin", SqlDbType.Bit,1),
					new SqlParameter("@RoleType", SqlDbType.Int,4),
					new SqlParameter("@MenuName", SqlDbType.VarChar,200),
					new SqlParameter("@URL", SqlDbType.VarChar,200),
					new SqlParameter("@ParentID", SqlDbType.VarChar,10),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@IsHide", SqlDbType.Bit,1),
					new SqlParameter("@IsMenuUse", SqlDbType.Bit,1)};
			parameters[0].Value = model.RoleID;
			parameters[1].Value = model.MenuID;
			parameters[2].Value = model.spras;
			parameters[3].Value = model.RoleName;
			parameters[4].Value = model.IsRoleUse;
			parameters[5].Value = model.IsAdmin;
			parameters[6].Value = model.RoleType;
			parameters[7].Value = model.MenuName;
			parameters[8].Value = model.URL;
			parameters[9].Value = model.ParentID;
			parameters[10].Value = model.OrderIndex;
			parameters[11].Value = model.IsHide;
			parameters[12].Value = model.IsMenuUse;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_RoleMenu ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_RoleMenu GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 RoleID,MenuID,spras,RoleName,IsRoleUse,IsAdmin,RoleType,MenuName,URL,ParentID,OrderIndex,IsHide,IsMenuUse from View_RoleMenu ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_RoleMenu model=new HBMS.Model.View_RoleMenu();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_RoleMenu DataRowToModel(DataRow row)
		{
			HBMS.Model.View_RoleMenu model=new HBMS.Model.View_RoleMenu();
			if (row != null)
			{
				if(row["RoleID"]!=null && row["RoleID"].ToString()!="")
				{
					model.RoleID=int.Parse(row["RoleID"].ToString());
				}
				if(row["MenuID"]!=null)
				{
					model.MenuID=row["MenuID"].ToString();
				}
				if(row["spras"]!=null && row["spras"].ToString()!="")
				{
					model.spras=int.Parse(row["spras"].ToString());
				}
				if(row["RoleName"]!=null)
				{
					model.RoleName=row["RoleName"].ToString();
				}
				if(row["IsRoleUse"]!=null && row["IsRoleUse"].ToString()!="")
				{
					if((row["IsRoleUse"].ToString()=="1")||(row["IsRoleUse"].ToString().ToLower()=="true"))
					{
						model.IsRoleUse=true;
					}
					else
					{
						model.IsRoleUse=false;
					}
				}
				if(row["IsAdmin"]!=null && row["IsAdmin"].ToString()!="")
				{
					if((row["IsAdmin"].ToString()=="1")||(row["IsAdmin"].ToString().ToLower()=="true"))
					{
						model.IsAdmin=true;
					}
					else
					{
						model.IsAdmin=false;
					}
				}
				if(row["RoleType"]!=null && row["RoleType"].ToString()!="")
				{
					model.RoleType=int.Parse(row["RoleType"].ToString());
				}
				if(row["MenuName"]!=null)
				{
					model.MenuName=row["MenuName"].ToString();
				}
				if(row["URL"]!=null)
				{
					model.URL=row["URL"].ToString();
				}
				if(row["ParentID"]!=null)
				{
					model.ParentID=row["ParentID"].ToString();
				}
				if(row["OrderIndex"]!=null && row["OrderIndex"].ToString()!="")
				{
					model.OrderIndex=int.Parse(row["OrderIndex"].ToString());
				}
				if(row["IsHide"]!=null && row["IsHide"].ToString()!="")
				{
					if((row["IsHide"].ToString()=="1")||(row["IsHide"].ToString().ToLower()=="true"))
					{
						model.IsHide=true;
					}
					else
					{
						model.IsHide=false;
					}
				}
				if(row["IsMenuUse"]!=null && row["IsMenuUse"].ToString()!="")
				{
					if((row["IsMenuUse"].ToString()=="1")||(row["IsMenuUse"].ToString().ToLower()=="true"))
					{
						model.IsMenuUse=true;
					}
					else
					{
						model.IsMenuUse=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select RoleID,MenuID,spras,RoleName,IsRoleUse,IsAdmin,RoleType,MenuName,URL,ParentID,OrderIndex,IsHide,IsMenuUse ");
			strSql.Append(" FROM View_RoleMenu ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" RoleID,MenuID,spras,RoleName,IsRoleUse,IsAdmin,RoleType,MenuName,URL,ParentID,OrderIndex,IsHide,IsMenuUse ");
			strSql.Append(" FROM View_RoleMenu ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_RoleMenu ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_RoleMenu T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_RoleMenu";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

