﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_ProvinceDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/2/13 0:48:59   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_ProvinceDesc
	/// </summary>
	public partial class View_ProvinceDesc
	{
		public View_ProvinceDesc()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_ProvinceDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_ProvinceDesc(");
			strSql.Append("ProvinceID,ProvinceName,CountryID,IsUse,SPRAS)");
			strSql.Append(" values (");
			strSql.Append("@ProvinceID,@ProvinceName,@CountryID,@IsUse,@SPRAS)");
			SqlParameter[] parameters = {
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,5),
					new SqlParameter("@ProvinceName", SqlDbType.VarChar,100),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@SPRAS", SqlDbType.Int,4)};
			parameters[0].Value = model.ProvinceID;
			parameters[1].Value = model.ProvinceName;
			parameters[2].Value = model.CountryID;
			parameters[3].Value = model.IsUse;
			parameters[4].Value = model.SPRAS;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_ProvinceDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_ProvinceDesc set ");
			strSql.Append("ProvinceID=@ProvinceID,");
			strSql.Append("ProvinceName=@ProvinceName,");
			strSql.Append("CountryID=@CountryID,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("SPRAS=@SPRAS");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,5),
					new SqlParameter("@ProvinceName", SqlDbType.VarChar,100),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@SPRAS", SqlDbType.Int,4)};
			parameters[0].Value = model.ProvinceID;
			parameters[1].Value = model.ProvinceName;
			parameters[2].Value = model.CountryID;
			parameters[3].Value = model.IsUse;
			parameters[4].Value = model.SPRAS;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_ProvinceDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_ProvinceDesc GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ProvinceID,ProvinceName,CountryID,IsUse,SPRAS from View_ProvinceDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_ProvinceDesc model=new HBMS.Model.View_ProvinceDesc();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_ProvinceDesc DataRowToModel(DataRow row)
		{
			HBMS.Model.View_ProvinceDesc model=new HBMS.Model.View_ProvinceDesc();
			if (row != null)
			{
				if(row["ProvinceID"]!=null)
				{
					model.ProvinceID=row["ProvinceID"].ToString();
				}
				if(row["ProvinceName"]!=null)
				{
					model.ProvinceName=row["ProvinceName"].ToString();
				}
				if(row["CountryID"]!=null)
				{
					model.CountryID=row["CountryID"].ToString();
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ProvinceID,ProvinceName,CountryID,IsUse,SPRAS ");
			strSql.Append(" FROM View_ProvinceDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ProvinceID,ProvinceName,CountryID,IsUse,SPRAS ");
			strSql.Append(" FROM View_ProvinceDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_ProvinceDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_ProvinceDesc T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_ProvinceDesc";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

