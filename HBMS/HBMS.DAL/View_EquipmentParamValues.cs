﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentParamValues
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/8/15 1:00:02   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_EquipmentParamValues
	/// </summary>
	public partial class View_EquipmentParamValues
	{
		public View_EquipmentParamValues()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_EquipmentParamValues model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_EquipmentParamValues(");
			strSql.Append("ID,SubDatetime,ParamValue,ParamState,RecordTime,DataPack,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,TagType,TagName,TagID,TagBitn,WarnLevel,NormalValue,Multiple,MinValue,MaxValue,LowLevelValue,HighLevelValue,EPSubDatetime,MethodID,Units,ParamType,PartsParamNo,ParamTypeName,PartsID,PartsName,SystemID,SystemName)");
			strSql.Append(" values (");
			strSql.Append("@ID,@SubDatetime,@ParamValue,@ParamState,@RecordTime,@DataPack,@AirPortID,@AirPortCode,@AirPortName,@Dimension,@Longitude,@EquipmentID,@EquipmentCode,@EquipmentName,@SPRAS,@ParamID,@ParamCode,@ParamName,@TagType,@TagName,@TagID,@TagBitn,@WarnLevel,@NormalValue,@Multiple,@MinValue,@MaxValue,@LowLevelValue,@HighLevelValue,@EPSubDatetime,@MethodID,@Units,@ParamType,@PartsParamNo,@ParamTypeName,@PartsID,@PartsName,@SystemID,@SystemName)");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@SubDatetime", SqlDbType.DateTime),
					new SqlParameter("@ParamValue", SqlDbType.Int,4),
					new SqlParameter("@ParamState", SqlDbType.Int,4),
					new SqlParameter("@RecordTime", SqlDbType.DateTime),
					new SqlParameter("@DataPack", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@EquipmentCode", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.Int,4),
					new SqlParameter("@ParamCode", SqlDbType.Int,4),
					new SqlParameter("@ParamName", SqlDbType.NVarChar,100),
					new SqlParameter("@TagType", SqlDbType.Int,4),
					new SqlParameter("@TagName", SqlDbType.VarChar,50),
					new SqlParameter("@TagID", SqlDbType.Int,4),
					new SqlParameter("@TagBitn", SqlDbType.Int,4),
					new SqlParameter("@WarnLevel", SqlDbType.Int,4),
					new SqlParameter("@NormalValue", SqlDbType.Int,4),
					new SqlParameter("@Multiple", SqlDbType.Int,4),
					new SqlParameter("@MinValue", SqlDbType.Int,4),
					new SqlParameter("@MaxValue", SqlDbType.Int,4),
					new SqlParameter("@LowLevelValue", SqlDbType.Int,4),
					new SqlParameter("@HighLevelValue", SqlDbType.Int,4),
					new SqlParameter("@EPSubDatetime", SqlDbType.DateTime),
					new SqlParameter("@MethodID", SqlDbType.Int,4),
					new SqlParameter("@Units", SqlDbType.NChar,10),
					new SqlParameter("@ParamType", SqlDbType.Int,4),
					new SqlParameter("@PartsParamNo", SqlDbType.Int,4),
					new SqlParameter("@ParamTypeName", SqlDbType.VarChar,50),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.SubDatetime;
			parameters[2].Value = model.ParamValue;
			parameters[3].Value = model.ParamState;
			parameters[4].Value = model.RecordTime;
			parameters[5].Value = model.DataPack;
			parameters[6].Value = model.AirPortID;
			parameters[7].Value = model.AirPortCode;
			parameters[8].Value = model.AirPortName;
			parameters[9].Value = model.Dimension;
			parameters[10].Value = model.Longitude;
			parameters[11].Value = model.EquipmentID;
			parameters[12].Value = model.EquipmentCode;
			parameters[13].Value = model.EquipmentName;
			parameters[14].Value = model.SPRAS;
			parameters[15].Value = model.ParamID;
			parameters[16].Value = model.ParamCode;
			parameters[17].Value = model.ParamName;
			parameters[18].Value = model.TagType;
			parameters[19].Value = model.TagName;
			parameters[20].Value = model.TagID;
			parameters[21].Value = model.TagBitn;
			parameters[22].Value = model.WarnLevel;
			parameters[23].Value = model.NormalValue;
			parameters[24].Value = model.Multiple;
			parameters[25].Value = model.MinValue;
			parameters[26].Value = model.MaxValue;
			parameters[27].Value = model.LowLevelValue;
			parameters[28].Value = model.HighLevelValue;
			parameters[29].Value = model.EPSubDatetime;
			parameters[30].Value = model.MethodID;
			parameters[31].Value = model.Units;
			parameters[32].Value = model.ParamType;
			parameters[33].Value = model.PartsParamNo;
			parameters[34].Value = model.ParamTypeName;
			parameters[35].Value = model.PartsID;
			parameters[36].Value = model.PartsName;
			parameters[37].Value = model.SystemID;
			parameters[38].Value = model.SystemName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_EquipmentParamValues model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_EquipmentParamValues set ");
			strSql.Append("ID=@ID,");
			strSql.Append("SubDatetime=@SubDatetime,");
			strSql.Append("ParamValue=@ParamValue,");
			strSql.Append("ParamState=@ParamState,");
			strSql.Append("RecordTime=@RecordTime,");
			strSql.Append("DataPack=@DataPack,");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("AirPortCode=@AirPortCode,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("Dimension=@Dimension,");
			strSql.Append("Longitude=@Longitude,");
			strSql.Append("EquipmentID=@EquipmentID,");
			strSql.Append("EquipmentCode=@EquipmentCode,");
			strSql.Append("EquipmentName=@EquipmentName,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("ParamID=@ParamID,");
			strSql.Append("ParamCode=@ParamCode,");
			strSql.Append("ParamName=@ParamName,");
			strSql.Append("TagType=@TagType,");
			strSql.Append("TagName=@TagName,");
			strSql.Append("TagID=@TagID,");
			strSql.Append("TagBitn=@TagBitn,");
			strSql.Append("WarnLevel=@WarnLevel,");
			strSql.Append("NormalValue=@NormalValue,");
			strSql.Append("Multiple=@Multiple,");
			strSql.Append("MinValue=@MinValue,");
			strSql.Append("MaxValue=@MaxValue,");
			strSql.Append("LowLevelValue=@LowLevelValue,");
			strSql.Append("HighLevelValue=@HighLevelValue,");
			strSql.Append("EPSubDatetime=@EPSubDatetime,");
			strSql.Append("MethodID=@MethodID,");
			strSql.Append("Units=@Units,");
			strSql.Append("ParamType=@ParamType,");
			strSql.Append("PartsParamNo=@PartsParamNo,");
			strSql.Append("ParamTypeName=@ParamTypeName,");
			strSql.Append("PartsID=@PartsID,");
			strSql.Append("PartsName=@PartsName,");
			strSql.Append("SystemID=@SystemID,");
			strSql.Append("SystemName=@SystemName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@SubDatetime", SqlDbType.DateTime),
					new SqlParameter("@ParamValue", SqlDbType.Int,4),
					new SqlParameter("@ParamState", SqlDbType.Int,4),
					new SqlParameter("@RecordTime", SqlDbType.DateTime),
					new SqlParameter("@DataPack", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@EquipmentCode", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.Int,4),
					new SqlParameter("@ParamCode", SqlDbType.Int,4),
					new SqlParameter("@ParamName", SqlDbType.NVarChar,100),
					new SqlParameter("@TagType", SqlDbType.Int,4),
					new SqlParameter("@TagName", SqlDbType.VarChar,50),
					new SqlParameter("@TagID", SqlDbType.Int,4),
					new SqlParameter("@TagBitn", SqlDbType.Int,4),
					new SqlParameter("@WarnLevel", SqlDbType.Int,4),
					new SqlParameter("@NormalValue", SqlDbType.Int,4),
					new SqlParameter("@Multiple", SqlDbType.Int,4),
					new SqlParameter("@MinValue", SqlDbType.Int,4),
					new SqlParameter("@MaxValue", SqlDbType.Int,4),
					new SqlParameter("@LowLevelValue", SqlDbType.Int,4),
					new SqlParameter("@HighLevelValue", SqlDbType.Int,4),
					new SqlParameter("@EPSubDatetime", SqlDbType.DateTime),
					new SqlParameter("@MethodID", SqlDbType.Int,4),
					new SqlParameter("@Units", SqlDbType.NChar,10),
					new SqlParameter("@ParamType", SqlDbType.Int,4),
					new SqlParameter("@PartsParamNo", SqlDbType.Int,4),
					new SqlParameter("@ParamTypeName", SqlDbType.VarChar,50),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.SubDatetime;
			parameters[2].Value = model.ParamValue;
			parameters[3].Value = model.ParamState;
			parameters[4].Value = model.RecordTime;
			parameters[5].Value = model.DataPack;
			parameters[6].Value = model.AirPortID;
			parameters[7].Value = model.AirPortCode;
			parameters[8].Value = model.AirPortName;
			parameters[9].Value = model.Dimension;
			parameters[10].Value = model.Longitude;
			parameters[11].Value = model.EquipmentID;
			parameters[12].Value = model.EquipmentCode;
			parameters[13].Value = model.EquipmentName;
			parameters[14].Value = model.SPRAS;
			parameters[15].Value = model.ParamID;
			parameters[16].Value = model.ParamCode;
			parameters[17].Value = model.ParamName;
			parameters[18].Value = model.TagType;
			parameters[19].Value = model.TagName;
			parameters[20].Value = model.TagID;
			parameters[21].Value = model.TagBitn;
			parameters[22].Value = model.WarnLevel;
			parameters[23].Value = model.NormalValue;
			parameters[24].Value = model.Multiple;
			parameters[25].Value = model.MinValue;
			parameters[26].Value = model.MaxValue;
			parameters[27].Value = model.LowLevelValue;
			parameters[28].Value = model.HighLevelValue;
			parameters[29].Value = model.EPSubDatetime;
			parameters[30].Value = model.MethodID;
			parameters[31].Value = model.Units;
			parameters[32].Value = model.ParamType;
			parameters[33].Value = model.PartsParamNo;
			parameters[34].Value = model.ParamTypeName;
			parameters[35].Value = model.PartsID;
			parameters[36].Value = model.PartsName;
			parameters[37].Value = model.SystemID;
			parameters[38].Value = model.SystemName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_EquipmentParamValues ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentParamValues GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,SubDatetime,ParamValue,ParamState,RecordTime,DataPack,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,TagType,TagName,TagID,TagBitn,WarnLevel,NormalValue,Multiple,MinValue,MaxValue,LowLevelValue,HighLevelValue,EPSubDatetime,MethodID,Units,ParamType,PartsParamNo,ParamTypeName,PartsID,PartsName,SystemID,SystemName from View_EquipmentParamValues ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_EquipmentParamValues model=new HBMS.Model.View_EquipmentParamValues();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentParamValues DataRowToModel(DataRow row)
		{
			HBMS.Model.View_EquipmentParamValues model=new HBMS.Model.View_EquipmentParamValues();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["SubDatetime"]!=null && row["SubDatetime"].ToString()!="")
				{
					model.SubDatetime=DateTime.Parse(row["SubDatetime"].ToString());
				}
				if(row["ParamValue"]!=null && row["ParamValue"].ToString()!="")
				{
					model.ParamValue=int.Parse(row["ParamValue"].ToString());
				}
				if(row["ParamState"]!=null && row["ParamState"].ToString()!="")
				{
					model.ParamState=int.Parse(row["ParamState"].ToString());
				}
				if(row["RecordTime"]!=null && row["RecordTime"].ToString()!="")
				{
					model.RecordTime=DateTime.Parse(row["RecordTime"].ToString());
				}
				if(row["DataPack"]!=null && row["DataPack"].ToString()!="")
				{
					model.DataPack=int.Parse(row["DataPack"].ToString());
				}
				if(row["AirPortID"]!=null)
				{
					model.AirPortID=row["AirPortID"].ToString();
				}
				if(row["AirPortCode"]!=null)
				{
					model.AirPortCode=row["AirPortCode"].ToString();
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["Dimension"]!=null)
				{
					model.Dimension=row["Dimension"].ToString();
				}
				if(row["Longitude"]!=null)
				{
					model.Longitude=row["Longitude"].ToString();
				}
				if(row["EquipmentID"]!=null && row["EquipmentID"].ToString()!="")
				{
					model.EquipmentID=int.Parse(row["EquipmentID"].ToString());
				}
				if(row["EquipmentCode"]!=null)
				{
					model.EquipmentCode=row["EquipmentCode"].ToString();
				}
				if(row["EquipmentName"]!=null)
				{
					model.EquipmentName=row["EquipmentName"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["ParamID"]!=null && row["ParamID"].ToString()!="")
				{
					model.ParamID=int.Parse(row["ParamID"].ToString());
				}
				if(row["ParamCode"]!=null && row["ParamCode"].ToString()!="")
				{
					model.ParamCode=int.Parse(row["ParamCode"].ToString());
				}
				if(row["ParamName"]!=null)
				{
					model.ParamName=row["ParamName"].ToString();
				}
				if(row["TagType"]!=null && row["TagType"].ToString()!="")
				{
					model.TagType=int.Parse(row["TagType"].ToString());
				}
				if(row["TagName"]!=null)
				{
					model.TagName=row["TagName"].ToString();
				}
				if(row["TagID"]!=null && row["TagID"].ToString()!="")
				{
					model.TagID=int.Parse(row["TagID"].ToString());
				}
				if(row["TagBitn"]!=null && row["TagBitn"].ToString()!="")
				{
					model.TagBitn=int.Parse(row["TagBitn"].ToString());
				}
				if(row["WarnLevel"]!=null && row["WarnLevel"].ToString()!="")
				{
					model.WarnLevel=int.Parse(row["WarnLevel"].ToString());
				}
				if(row["NormalValue"]!=null && row["NormalValue"].ToString()!="")
				{
					model.NormalValue=int.Parse(row["NormalValue"].ToString());
				}
				if(row["Multiple"]!=null && row["Multiple"].ToString()!="")
				{
					model.Multiple=int.Parse(row["Multiple"].ToString());
				}
				if(row["MinValue"]!=null && row["MinValue"].ToString()!="")
				{
					model.MinValue=int.Parse(row["MinValue"].ToString());
				}
				if(row["MaxValue"]!=null && row["MaxValue"].ToString()!="")
				{
					model.MaxValue=int.Parse(row["MaxValue"].ToString());
				}
				if(row["LowLevelValue"]!=null && row["LowLevelValue"].ToString()!="")
				{
					model.LowLevelValue=int.Parse(row["LowLevelValue"].ToString());
				}
				if(row["HighLevelValue"]!=null && row["HighLevelValue"].ToString()!="")
				{
					model.HighLevelValue=int.Parse(row["HighLevelValue"].ToString());
				}
				if(row["EPSubDatetime"]!=null && row["EPSubDatetime"].ToString()!="")
				{
					model.EPSubDatetime=DateTime.Parse(row["EPSubDatetime"].ToString());
				}
				if(row["MethodID"]!=null && row["MethodID"].ToString()!="")
				{
					model.MethodID=int.Parse(row["MethodID"].ToString());
				}
				if(row["Units"]!=null)
				{
					model.Units=row["Units"].ToString();
				}
				if(row["ParamType"]!=null && row["ParamType"].ToString()!="")
				{
					model.ParamType=int.Parse(row["ParamType"].ToString());
				}
				if(row["PartsParamNo"]!=null && row["PartsParamNo"].ToString()!="")
				{
					model.PartsParamNo=int.Parse(row["PartsParamNo"].ToString());
				}
				if(row["ParamTypeName"]!=null)
				{
					model.ParamTypeName=row["ParamTypeName"].ToString();
				}
				if(row["PartsID"]!=null && row["PartsID"].ToString()!="")
				{
					model.PartsID=int.Parse(row["PartsID"].ToString());
				}
				if(row["PartsName"]!=null)
				{
					model.PartsName=row["PartsName"].ToString();
				}
				if(row["SystemID"]!=null && row["SystemID"].ToString()!="")
				{
					model.SystemID=int.Parse(row["SystemID"].ToString());
				}
				if(row["SystemName"]!=null)
				{
					model.SystemName=row["SystemName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,SubDatetime,ParamValue,ParamState,RecordTime,DataPack,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,TagType,TagName,TagID,TagBitn,WarnLevel,NormalValue,Multiple,MinValue,MaxValue,LowLevelValue,HighLevelValue,EPSubDatetime,MethodID,Units,ParamType,PartsParamNo,ParamTypeName,PartsID,PartsName,SystemID,SystemName ");
			strSql.Append(" FROM View_EquipmentParamValues ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,SubDatetime,ParamValue,ParamState,RecordTime,DataPack,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,TagType,TagName,TagID,TagBitn,WarnLevel,NormalValue,Multiple,MinValue,MaxValue,LowLevelValue,HighLevelValue,EPSubDatetime,MethodID,Units,ParamType,PartsParamNo,ParamTypeName,PartsID,PartsName,SystemID,SystemName ");
			strSql.Append(" FROM View_EquipmentParamValues ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_EquipmentParamValues ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_EquipmentParamValues T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_EquipmentParamValues";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

