﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentParams
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 22:43:54   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_EquipmentParams
	/// </summary>
	public partial class View_EquipmentParams
	{
		public View_EquipmentParams()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_EquipmentParams model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_EquipmentParams(");
			strSql.Append("ID,SubDatetime,OrderIndex,AirPortID,EquipmentID,EquipmentName,SPRAS,ParamID,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName)");
			strSql.Append(" values (");
			strSql.Append("@ID,@SubDatetime,@OrderIndex,@AirPortID,@EquipmentID,@EquipmentName,@SPRAS,@ParamID,@ParamName,@ParamTypeID,@boolValue,@PromptMin,@PromptMax,@WarningMin,@WarningMax,@ErrorMin,@ErrorMax,@PartsID,@PartsName,@SystemID,@SystemName)");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@SubDatetime", SqlDbType.DateTime),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentID", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.VarChar,10),
					new SqlParameter("@ParamName", SqlDbType.NChar,10),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.SubDatetime;
			parameters[2].Value = model.OrderIndex;
			parameters[3].Value = model.AirPortID;
			parameters[4].Value = model.EquipmentID;
			parameters[5].Value = model.EquipmentName;
			parameters[6].Value = model.SPRAS;
			parameters[7].Value = model.ParamID;
			parameters[8].Value = model.ParamName;
			parameters[9].Value = model.ParamTypeID;
			parameters[10].Value = model.boolValue;
			parameters[11].Value = model.PromptMin;
			parameters[12].Value = model.PromptMax;
			parameters[13].Value = model.WarningMin;
			parameters[14].Value = model.WarningMax;
			parameters[15].Value = model.ErrorMin;
			parameters[16].Value = model.ErrorMax;
			parameters[17].Value = model.PartsID;
			parameters[18].Value = model.PartsName;
			parameters[19].Value = model.SystemID;
			parameters[20].Value = model.SystemName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_EquipmentParams model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_EquipmentParams set ");
			strSql.Append("ID=@ID,");
			strSql.Append("SubDatetime=@SubDatetime,");
			strSql.Append("OrderIndex=@OrderIndex,");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("EquipmentID=@EquipmentID,");
			strSql.Append("EquipmentName=@EquipmentName,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("ParamID=@ParamID,");
			strSql.Append("ParamName=@ParamName,");
			strSql.Append("ParamTypeID=@ParamTypeID,");
			strSql.Append("boolValue=@boolValue,");
			strSql.Append("PromptMin=@PromptMin,");
			strSql.Append("PromptMax=@PromptMax,");
			strSql.Append("WarningMin=@WarningMin,");
			strSql.Append("WarningMax=@WarningMax,");
			strSql.Append("ErrorMin=@ErrorMin,");
			strSql.Append("ErrorMax=@ErrorMax,");
			strSql.Append("PartsID=@PartsID,");
			strSql.Append("PartsName=@PartsName,");
			strSql.Append("SystemID=@SystemID,");
			strSql.Append("SystemName=@SystemName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@SubDatetime", SqlDbType.DateTime),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentID", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.VarChar,10),
					new SqlParameter("@ParamName", SqlDbType.NChar,10),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.SubDatetime;
			parameters[2].Value = model.OrderIndex;
			parameters[3].Value = model.AirPortID;
			parameters[4].Value = model.EquipmentID;
			parameters[5].Value = model.EquipmentName;
			parameters[6].Value = model.SPRAS;
			parameters[7].Value = model.ParamID;
			parameters[8].Value = model.ParamName;
			parameters[9].Value = model.ParamTypeID;
			parameters[10].Value = model.boolValue;
			parameters[11].Value = model.PromptMin;
			parameters[12].Value = model.PromptMax;
			parameters[13].Value = model.WarningMin;
			parameters[14].Value = model.WarningMax;
			parameters[15].Value = model.ErrorMin;
			parameters[16].Value = model.ErrorMax;
			parameters[17].Value = model.PartsID;
			parameters[18].Value = model.PartsName;
			parameters[19].Value = model.SystemID;
			parameters[20].Value = model.SystemName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_EquipmentParams ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentParams GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,SubDatetime,OrderIndex,AirPortID,EquipmentID,EquipmentName,SPRAS,ParamID,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName from View_EquipmentParams ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_EquipmentParams model=new HBMS.Model.View_EquipmentParams();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentParams DataRowToModel(DataRow row)
		{
			HBMS.Model.View_EquipmentParams model=new HBMS.Model.View_EquipmentParams();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["SubDatetime"]!=null && row["SubDatetime"].ToString()!="")
				{
					model.SubDatetime=DateTime.Parse(row["SubDatetime"].ToString());
				}
				if(row["OrderIndex"]!=null && row["OrderIndex"].ToString()!="")
				{
					model.OrderIndex=int.Parse(row["OrderIndex"].ToString());
				}
				if(row["AirPortID"]!=null)
				{
					model.AirPortID=row["AirPortID"].ToString();
				}
				if(row["EquipmentID"]!=null)
				{
					model.EquipmentID=row["EquipmentID"].ToString();
				}
				if(row["EquipmentName"]!=null)
				{
					model.EquipmentName=row["EquipmentName"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["ParamID"]!=null)
				{
					model.ParamID=row["ParamID"].ToString();
				}
				if(row["ParamName"]!=null)
				{
					model.ParamName=row["ParamName"].ToString();
				}
				if(row["ParamTypeID"]!=null && row["ParamTypeID"].ToString()!="")
				{
					model.ParamTypeID=int.Parse(row["ParamTypeID"].ToString());
				}
				if(row["boolValue"]!=null && row["boolValue"].ToString()!="")
				{
					model.boolValue=decimal.Parse(row["boolValue"].ToString());
				}
				if(row["PromptMin"]!=null && row["PromptMin"].ToString()!="")
				{
					model.PromptMin=decimal.Parse(row["PromptMin"].ToString());
				}
				if(row["PromptMax"]!=null && row["PromptMax"].ToString()!="")
				{
					model.PromptMax=decimal.Parse(row["PromptMax"].ToString());
				}
				if(row["WarningMin"]!=null && row["WarningMin"].ToString()!="")
				{
					model.WarningMin=decimal.Parse(row["WarningMin"].ToString());
				}
				if(row["WarningMax"]!=null && row["WarningMax"].ToString()!="")
				{
					model.WarningMax=decimal.Parse(row["WarningMax"].ToString());
				}
				if(row["ErrorMin"]!=null && row["ErrorMin"].ToString()!="")
				{
					model.ErrorMin=decimal.Parse(row["ErrorMin"].ToString());
				}
				if(row["ErrorMax"]!=null && row["ErrorMax"].ToString()!="")
				{
					model.ErrorMax=decimal.Parse(row["ErrorMax"].ToString());
				}
				if(row["PartsID"]!=null && row["PartsID"].ToString()!="")
				{
					model.PartsID=int.Parse(row["PartsID"].ToString());
				}
				if(row["PartsName"]!=null)
				{
					model.PartsName=row["PartsName"].ToString();
				}
				if(row["SystemID"]!=null && row["SystemID"].ToString()!="")
				{
					model.SystemID=int.Parse(row["SystemID"].ToString());
				}
				if(row["SystemName"]!=null)
				{
					model.SystemName=row["SystemName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,SubDatetime,OrderIndex,AirPortID,EquipmentID,EquipmentName,SPRAS,ParamID,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName ");
			strSql.Append(" FROM View_EquipmentParams ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,SubDatetime,OrderIndex,AirPortID,EquipmentID,EquipmentName,SPRAS,ParamID,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName ");
			strSql.Append(" FROM View_EquipmentParams ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_EquipmentParams ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_EquipmentParams T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_EquipmentParams";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

