﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_Repair
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/7 23:42:30   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_Repair
	/// </summary>
	public partial class View_Repair
	{
		public View_Repair()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_Repair model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_Repair(");
			strSql.Append("ID,AirPortID,EquipmentID,SystemID,PartsID,RepairUse,CheckUser,RepairDate,UserCode,SubDate,Contect,memo,AirPortName,SPRAS,EquipmentName,SystemName,PartsName,CName,EName)");
			strSql.Append(" values (");
			strSql.Append("@ID,@AirPortID,@EquipmentID,@SystemID,@PartsID,@RepairUse,@CheckUser,@RepairDate,@UserCode,@SubDate,@Contect,@memo,@AirPortName,@SPRAS,@EquipmentName,@SystemName,@PartsName,@CName,@EName)");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentID", SqlDbType.VarChar,10),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@RepairUse", SqlDbType.VarChar,50),
					new SqlParameter("@CheckUser", SqlDbType.VarChar,50),
					new SqlParameter("@RepairDate", SqlDbType.DateTime),
					new SqlParameter("@UserCode", SqlDbType.VarChar,20),
					new SqlParameter("@SubDate", SqlDbType.DateTime),
					new SqlParameter("@Contect", SqlDbType.VarChar,8000),
					new SqlParameter("@memo", SqlDbType.VarChar,8000),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@CName", SqlDbType.NVarChar,100),
					new SqlParameter("@EName", SqlDbType.NVarChar,100)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.AirPortID;
			parameters[2].Value = model.EquipmentID;
			parameters[3].Value = model.SystemID;
			parameters[4].Value = model.PartsID;
			parameters[5].Value = model.RepairUse;
			parameters[6].Value = model.CheckUser;
			parameters[7].Value = model.RepairDate;
			parameters[8].Value = model.UserCode;
			parameters[9].Value = model.SubDate;
			parameters[10].Value = model.Contect;
			parameters[11].Value = model.memo;
			parameters[12].Value = model.AirPortName;
			parameters[13].Value = model.SPRAS;
			parameters[14].Value = model.EquipmentName;
			parameters[15].Value = model.SystemName;
			parameters[16].Value = model.PartsName;
			parameters[17].Value = model.CName;
			parameters[18].Value = model.EName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_Repair model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_Repair set ");
			strSql.Append("ID=@ID,");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("EquipmentID=@EquipmentID,");
			strSql.Append("SystemID=@SystemID,");
			strSql.Append("PartsID=@PartsID,");
			strSql.Append("RepairUse=@RepairUse,");
			strSql.Append("CheckUser=@CheckUser,");
			strSql.Append("RepairDate=@RepairDate,");
			strSql.Append("UserCode=@UserCode,");
			strSql.Append("SubDate=@SubDate,");
			strSql.Append("Contect=@Contect,");
			strSql.Append("memo=@memo,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("EquipmentName=@EquipmentName,");
			strSql.Append("SystemName=@SystemName,");
			strSql.Append("PartsName=@PartsName,");
			strSql.Append("CName=@CName,");
			strSql.Append("EName=@EName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentID", SqlDbType.VarChar,10),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@RepairUse", SqlDbType.VarChar,50),
					new SqlParameter("@CheckUser", SqlDbType.VarChar,50),
					new SqlParameter("@RepairDate", SqlDbType.DateTime),
					new SqlParameter("@UserCode", SqlDbType.VarChar,20),
					new SqlParameter("@SubDate", SqlDbType.DateTime),
					new SqlParameter("@Contect", SqlDbType.VarChar,8000),
					new SqlParameter("@memo", SqlDbType.VarChar,8000),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@CName", SqlDbType.NVarChar,100),
					new SqlParameter("@EName", SqlDbType.NVarChar,100)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.AirPortID;
			parameters[2].Value = model.EquipmentID;
			parameters[3].Value = model.SystemID;
			parameters[4].Value = model.PartsID;
			parameters[5].Value = model.RepairUse;
			parameters[6].Value = model.CheckUser;
			parameters[7].Value = model.RepairDate;
			parameters[8].Value = model.UserCode;
			parameters[9].Value = model.SubDate;
			parameters[10].Value = model.Contect;
			parameters[11].Value = model.memo;
			parameters[12].Value = model.AirPortName;
			parameters[13].Value = model.SPRAS;
			parameters[14].Value = model.EquipmentName;
			parameters[15].Value = model.SystemName;
			parameters[16].Value = model.PartsName;
			parameters[17].Value = model.CName;
			parameters[18].Value = model.EName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_Repair ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_Repair GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,AirPortID,EquipmentID,SystemID,PartsID,RepairUse,CheckUser,RepairDate,UserCode,SubDate,Contect,memo,AirPortName,SPRAS,EquipmentName,SystemName,PartsName,CName,EName from View_Repair ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_Repair model=new HBMS.Model.View_Repair();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_Repair DataRowToModel(DataRow row)
		{
			HBMS.Model.View_Repair model=new HBMS.Model.View_Repair();
			if (row != null)
			{
				if(row["ID"]!=null)
				{
					model.ID=row["ID"].ToString();
				}
				if(row["AirPortID"]!=null)
				{
					model.AirPortID=row["AirPortID"].ToString();
				}
				if(row["EquipmentID"]!=null)
				{
					model.EquipmentID=row["EquipmentID"].ToString();
				}
				if(row["SystemID"]!=null && row["SystemID"].ToString()!="")
				{
					model.SystemID=int.Parse(row["SystemID"].ToString());
				}
				if(row["PartsID"]!=null && row["PartsID"].ToString()!="")
				{
					model.PartsID=int.Parse(row["PartsID"].ToString());
				}
				if(row["RepairUse"]!=null)
				{
					model.RepairUse=row["RepairUse"].ToString();
				}
				if(row["CheckUser"]!=null)
				{
					model.CheckUser=row["CheckUser"].ToString();
				}
				if(row["RepairDate"]!=null && row["RepairDate"].ToString()!="")
				{
					model.RepairDate=DateTime.Parse(row["RepairDate"].ToString());
				}
				if(row["UserCode"]!=null)
				{
					model.UserCode=row["UserCode"].ToString();
				}
				if(row["SubDate"]!=null && row["SubDate"].ToString()!="")
				{
					model.SubDate=DateTime.Parse(row["SubDate"].ToString());
				}
				if(row["Contect"]!=null)
				{
					model.Contect=row["Contect"].ToString();
				}
				if(row["memo"]!=null)
				{
					model.memo=row["memo"].ToString();
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["EquipmentName"]!=null)
				{
					model.EquipmentName=row["EquipmentName"].ToString();
				}
				if(row["SystemName"]!=null)
				{
					model.SystemName=row["SystemName"].ToString();
				}
				if(row["PartsName"]!=null)
				{
					model.PartsName=row["PartsName"].ToString();
				}
				if(row["CName"]!=null)
				{
					model.CName=row["CName"].ToString();
				}
				if(row["EName"]!=null)
				{
					model.EName=row["EName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,AirPortID,EquipmentID,SystemID,PartsID,RepairUse,CheckUser,RepairDate,UserCode,SubDate,Contect,memo,AirPortName,SPRAS,EquipmentName,SystemName,PartsName,CName,EName ");
			strSql.Append(" FROM View_Repair ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,AirPortID,EquipmentID,SystemID,PartsID,RepairUse,CheckUser,RepairDate,UserCode,SubDate,Contect,memo,AirPortName,SPRAS,EquipmentName,SystemName,PartsName,CName,EName ");
			strSql.Append(" FROM View_Repair ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_Repair ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_Repair T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_Repair";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

