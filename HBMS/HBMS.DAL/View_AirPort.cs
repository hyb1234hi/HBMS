﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_AirPort
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:22   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_AirPort
	/// </summary>
	public partial class View_AirPort
	{
		public View_AirPort()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_AirPort model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_AirPort(");
			strSql.Append("AirPortID,CountryID,ProvinceID,CityID,Address,Dimension,Longitude,Head,HeadPhone,HeadEmail,SPRAS,AirPortName,CountryName,ProvinceName,CityName)");
			strSql.Append(" values (");
			strSql.Append("@AirPortID,@CountryID,@ProvinceID,@CityID,@Address,@Dimension,@Longitude,@Head,@HeadPhone,@HeadEmail,@SPRAS,@AirPortName,@CountryName,@ProvinceName,@CityName)");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@CountryID", SqlDbType.VarChar,6),
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,6),
					new SqlParameter("@CityID", SqlDbType.VarChar,6),
					new SqlParameter("@Address", SqlDbType.VarChar,500),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@Head", SqlDbType.VarChar,20),
					new SqlParameter("@HeadPhone", SqlDbType.VarChar,20),
					new SqlParameter("@HeadEmail", SqlDbType.VarChar,50),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@AirPortName", SqlDbType.VarChar,200),
					new SqlParameter("@CountryName", SqlDbType.VarChar,100),
					new SqlParameter("@ProvinceName", SqlDbType.VarChar,100),
					new SqlParameter("@CityName", SqlDbType.VarChar,100)};
			parameters[0].Value = model.AirPortID;
			parameters[1].Value = model.CountryID;
			parameters[2].Value = model.ProvinceID;
			parameters[3].Value = model.CityID;
			parameters[4].Value = model.Address;
			parameters[5].Value = model.Dimension;
			parameters[6].Value = model.Longitude;
			parameters[7].Value = model.Head;
			parameters[8].Value = model.HeadPhone;
			parameters[9].Value = model.HeadEmail;
			parameters[10].Value = model.SPRAS;
			parameters[11].Value = model.AirPortName;
			parameters[12].Value = model.CountryName;
			parameters[13].Value = model.ProvinceName;
			parameters[14].Value = model.CityName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_AirPort model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_AirPort set ");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("CountryID=@CountryID,");
			strSql.Append("ProvinceID=@ProvinceID,");
			strSql.Append("CityID=@CityID,");
			strSql.Append("Address=@Address,");
			strSql.Append("Dimension=@Dimension,");
			strSql.Append("Longitude=@Longitude,");
			strSql.Append("Head=@Head,");
			strSql.Append("HeadPhone=@HeadPhone,");
			strSql.Append("HeadEmail=@HeadEmail,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("CountryName=@CountryName,");
			strSql.Append("ProvinceName=@ProvinceName,");
			strSql.Append("CityName=@CityName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@CountryID", SqlDbType.VarChar,6),
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,6),
					new SqlParameter("@CityID", SqlDbType.VarChar,6),
					new SqlParameter("@Address", SqlDbType.VarChar,500),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@Head", SqlDbType.VarChar,20),
					new SqlParameter("@HeadPhone", SqlDbType.VarChar,20),
					new SqlParameter("@HeadEmail", SqlDbType.VarChar,50),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@AirPortName", SqlDbType.VarChar,200),
					new SqlParameter("@CountryName", SqlDbType.VarChar,100),
					new SqlParameter("@ProvinceName", SqlDbType.VarChar,100),
					new SqlParameter("@CityName", SqlDbType.VarChar,100)};
			parameters[0].Value = model.AirPortID;
			parameters[1].Value = model.CountryID;
			parameters[2].Value = model.ProvinceID;
			parameters[3].Value = model.CityID;
			parameters[4].Value = model.Address;
			parameters[5].Value = model.Dimension;
			parameters[6].Value = model.Longitude;
			parameters[7].Value = model.Head;
			parameters[8].Value = model.HeadPhone;
			parameters[9].Value = model.HeadEmail;
			parameters[10].Value = model.SPRAS;
			parameters[11].Value = model.AirPortName;
			parameters[12].Value = model.CountryName;
			parameters[13].Value = model.ProvinceName;
			parameters[14].Value = model.CityName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_AirPort ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_AirPort GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 AirPortID,CountryID,ProvinceID,CityID,Address,Dimension,Longitude,Head,HeadPhone,HeadEmail,SPRAS,AirPortName,CountryName,ProvinceName,CityName from View_AirPort ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_AirPort model=new HBMS.Model.View_AirPort();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_AirPort DataRowToModel(DataRow row)
		{
			HBMS.Model.View_AirPort model=new HBMS.Model.View_AirPort();
			if (row != null)
			{
				if(row["AirPortID"]!=null)
				{
					model.AirPortID=row["AirPortID"].ToString();
				}
				if(row["CountryID"]!=null)
				{
					model.CountryID=row["CountryID"].ToString();
				}
				if(row["ProvinceID"]!=null)
				{
					model.ProvinceID=row["ProvinceID"].ToString();
				}
				if(row["CityID"]!=null)
				{
					model.CityID=row["CityID"].ToString();
				}
				if(row["Address"]!=null)
				{
					model.Address=row["Address"].ToString();
				}
				if(row["Dimension"]!=null)
				{
					model.Dimension=row["Dimension"].ToString();
				}
				if(row["Longitude"]!=null)
				{
					model.Longitude=row["Longitude"].ToString();
				}
				if(row["Head"]!=null)
				{
					model.Head=row["Head"].ToString();
				}
				if(row["HeadPhone"]!=null)
				{
					model.HeadPhone=row["HeadPhone"].ToString();
				}
				if(row["HeadEmail"]!=null)
				{
					model.HeadEmail=row["HeadEmail"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["CountryName"]!=null)
				{
					model.CountryName=row["CountryName"].ToString();
				}
				if(row["ProvinceName"]!=null)
				{
					model.ProvinceName=row["ProvinceName"].ToString();
				}
				if(row["CityName"]!=null)
				{
					model.CityName=row["CityName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select AirPortID,CountryID,ProvinceID,CityID,Address,Dimension,Longitude,Head,HeadPhone,HeadEmail,SPRAS,AirPortName,CountryName,ProvinceName,CityName ");
			strSql.Append(" FROM View_AirPort ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" AirPortID,CountryID,ProvinceID,CityID,Address,Dimension,Longitude,Head,HeadPhone,HeadEmail,SPRAS,AirPortName,CountryName,ProvinceName,CityName ");
			strSql.Append(" FROM View_AirPort ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_AirPort ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.UserCode desc");
			}
			strSql.Append(")AS Row, T.*  from View_AirPort T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_AirPort";
			parameters[1].Value = "UserCode";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

