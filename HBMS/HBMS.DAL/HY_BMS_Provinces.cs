﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Provinces
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:17   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:HY_BMS_Provinces
	/// </summary>
	public partial class HY_BMS_Provinces
	{
		public HY_BMS_Provinces()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string ID,string CountryID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_Provinces");
			strSql.Append(" where ID=@ID and CountryID=@CountryID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.VarChar,5),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5)			};
			parameters[0].Value = ID;
			parameters[1].Value = CountryID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.HY_BMS_Provinces model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_Provinces(");
			strSql.Append("ID,CountryID,IsUse)");
			strSql.Append(" values (");
			strSql.Append("@ID,@CountryID,@IsUse)");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.VarChar,5),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5),
					new SqlParameter("@IsUse", SqlDbType.Bit,1)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.CountryID;
			parameters[2].Value = model.IsUse;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_Provinces model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_Provinces set ");
			strSql.Append("IsUse=@IsUse");
			strSql.Append(" where ID=@ID and CountryID=@CountryID ");
			SqlParameter[] parameters = {
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@ID", SqlDbType.VarChar,5),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5)};
			parameters[0].Value = model.IsUse;
			parameters[1].Value = model.ID;
			parameters[2].Value = model.CountryID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string ID,string CountryID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_Provinces ");
			strSql.Append(" where ID=@ID and CountryID=@CountryID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.VarChar,5),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5)			};
			parameters[0].Value = ID;
			parameters[1].Value = CountryID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Provinces GetModel(string ID,string CountryID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,CountryID,IsUse from HY_BMS_Provinces ");
			strSql.Append(" where ID=@ID and CountryID=@CountryID ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.VarChar,5),
					new SqlParameter("@CountryID", SqlDbType.VarChar,5)			};
			parameters[0].Value = ID;
			parameters[1].Value = CountryID;

			HBMS.Model.HY_BMS_Provinces model=new HBMS.Model.HY_BMS_Provinces();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Provinces DataRowToModel(DataRow row)
		{
			HBMS.Model.HY_BMS_Provinces model=new HBMS.Model.HY_BMS_Provinces();
			if (row != null)
			{
				if(row["ID"]!=null)
				{
					model.ID=row["ID"].ToString();
				}
				if(row["CountryID"]!=null)
				{
					model.CountryID=row["CountryID"].ToString();
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,CountryID,IsUse ");
			strSql.Append(" FROM HY_BMS_Provinces ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,CountryID,IsUse ");
			strSql.Append(" FROM HY_BMS_Provinces ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM HY_BMS_Provinces ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.CountryID desc");
			}
			strSql.Append(")AS Row, T.*  from HY_BMS_Provinces T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "HY_BMS_Provinces";
			parameters[1].Value = "CountryID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

