﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_AirPortDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 23:03:22   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_AirPortDesc
	/// </summary>
	public partial class View_AirPortDesc
	{
		public View_AirPortDesc()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_AirPortDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_AirPortDesc(");
			strSql.Append("AirPortID,AirPortCode,AirPortName,AirPortAddress,SPRAS,CityID,CountryID,ProvinceID,Longitude,Dimension,Head,HeadEmail,HeadPhone,CountryName,ProvinceName,CityName)");
			strSql.Append(" values (");
			strSql.Append("@AirPortID,@AirPortCode,@AirPortName,@AirPortAddress,@SPRAS,@CityID,@CountryID,@ProvinceID,@Longitude,@Dimension,@Head,@HeadEmail,@HeadPhone,@CountryName,@ProvinceName,@CityName)");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.Int,4),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@AirPortAddress", SqlDbType.NVarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@CityID", SqlDbType.VarChar,6),
					new SqlParameter("@CountryID", SqlDbType.VarChar,6),
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,6),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Head", SqlDbType.VarChar,20),
					new SqlParameter("@HeadEmail", SqlDbType.VarChar,50),
					new SqlParameter("@HeadPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CountryName", SqlDbType.VarChar,100),
					new SqlParameter("@ProvinceName", SqlDbType.VarChar,100),
					new SqlParameter("@CityName", SqlDbType.VarChar,100)};
			parameters[0].Value = model.AirPortID;
			parameters[1].Value = model.AirPortCode;
			parameters[2].Value = model.AirPortName;
			parameters[3].Value = model.AirPortAddress;
			parameters[4].Value = model.SPRAS;
			parameters[5].Value = model.CityID;
			parameters[6].Value = model.CountryID;
			parameters[7].Value = model.ProvinceID;
			parameters[8].Value = model.Longitude;
			parameters[9].Value = model.Dimension;
			parameters[10].Value = model.Head;
			parameters[11].Value = model.HeadEmail;
			parameters[12].Value = model.HeadPhone;
			parameters[13].Value = model.CountryName;
			parameters[14].Value = model.ProvinceName;
			parameters[15].Value = model.CityName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_AirPortDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_AirPortDesc set ");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("AirPortCode=@AirPortCode,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("AirPortAddress=@AirPortAddress,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("CityID=@CityID,");
			strSql.Append("CountryID=@CountryID,");
			strSql.Append("ProvinceID=@ProvinceID,");
			strSql.Append("Longitude=@Longitude,");
			strSql.Append("Dimension=@Dimension,");
			strSql.Append("Head=@Head,");
			strSql.Append("HeadEmail=@HeadEmail,");
			strSql.Append("HeadPhone=@HeadPhone,");
			strSql.Append("CountryName=@CountryName,");
			strSql.Append("ProvinceName=@ProvinceName,");
			strSql.Append("CityName=@CityName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.Int,4),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@AirPortAddress", SqlDbType.NVarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@CityID", SqlDbType.VarChar,6),
					new SqlParameter("@CountryID", SqlDbType.VarChar,6),
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,6),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Head", SqlDbType.VarChar,20),
					new SqlParameter("@HeadEmail", SqlDbType.VarChar,50),
					new SqlParameter("@HeadPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CountryName", SqlDbType.VarChar,100),
					new SqlParameter("@ProvinceName", SqlDbType.VarChar,100),
					new SqlParameter("@CityName", SqlDbType.VarChar,100)};
			parameters[0].Value = model.AirPortID;
			parameters[1].Value = model.AirPortCode;
			parameters[2].Value = model.AirPortName;
			parameters[3].Value = model.AirPortAddress;
			parameters[4].Value = model.SPRAS;
			parameters[5].Value = model.CityID;
			parameters[6].Value = model.CountryID;
			parameters[7].Value = model.ProvinceID;
			parameters[8].Value = model.Longitude;
			parameters[9].Value = model.Dimension;
			parameters[10].Value = model.Head;
			parameters[11].Value = model.HeadEmail;
			parameters[12].Value = model.HeadPhone;
			parameters[13].Value = model.CountryName;
			parameters[14].Value = model.ProvinceName;
			parameters[15].Value = model.CityName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_AirPortDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_AirPortDesc GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 AirPortID,AirPortCode,AirPortName,AirPortAddress,SPRAS,CityID,CountryID,ProvinceID,Longitude,Dimension,Head,HeadEmail,HeadPhone,CountryName,ProvinceName,CityName from View_AirPortDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_AirPortDesc model=new HBMS.Model.View_AirPortDesc();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_AirPortDesc DataRowToModel(DataRow row)
		{
			HBMS.Model.View_AirPortDesc model=new HBMS.Model.View_AirPortDesc();
			if (row != null)
			{
				if(row["AirPortID"]!=null && row["AirPortID"].ToString()!="")
				{
					model.AirPortID=int.Parse(row["AirPortID"].ToString());
				}
				if(row["AirPortCode"]!=null)
				{
					model.AirPortCode=row["AirPortCode"].ToString();
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["AirPortAddress"]!=null)
				{
					model.AirPortAddress=row["AirPortAddress"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["CityID"]!=null)
				{
					model.CityID=row["CityID"].ToString();
				}
				if(row["CountryID"]!=null)
				{
					model.CountryID=row["CountryID"].ToString();
				}
				if(row["ProvinceID"]!=null)
				{
					model.ProvinceID=row["ProvinceID"].ToString();
				}
				if(row["Longitude"]!=null)
				{
					model.Longitude=row["Longitude"].ToString();
				}
				if(row["Dimension"]!=null)
				{
					model.Dimension=row["Dimension"].ToString();
				}
				if(row["Head"]!=null)
				{
					model.Head=row["Head"].ToString();
				}
				if(row["HeadEmail"]!=null)
				{
					model.HeadEmail=row["HeadEmail"].ToString();
				}
				if(row["HeadPhone"]!=null)
				{
					model.HeadPhone=row["HeadPhone"].ToString();
				}
				if(row["CountryName"]!=null)
				{
					model.CountryName=row["CountryName"].ToString();
				}
				if(row["ProvinceName"]!=null)
				{
					model.ProvinceName=row["ProvinceName"].ToString();
				}
				if(row["CityName"]!=null)
				{
					model.CityName=row["CityName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select AirPortID,AirPortCode,AirPortName,AirPortAddress,SPRAS,CityID,CountryID,ProvinceID,Longitude,Dimension,Head,HeadEmail,HeadPhone,CountryName,ProvinceName,CityName ");
			strSql.Append(" FROM View_AirPortDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" AirPortID,AirPortCode,AirPortName,AirPortAddress,SPRAS,CityID,CountryID,ProvinceID,Longitude,Dimension,Head,HeadEmail,HeadPhone,CountryName,ProvinceName,CityName ");
			strSql.Append(" FROM View_AirPortDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_AirPortDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SPRAS desc");
			}
			strSql.Append(")AS Row, T.*  from View_AirPortDesc T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_AirPortDesc";
			parameters[1].Value = "SPRAS";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

