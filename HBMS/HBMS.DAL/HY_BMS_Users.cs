﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Users
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/2/13 2:00:09   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:HY_BMS_Users
	/// </summary>
	public partial class HY_BMS_Users
	{
		public HY_BMS_Users()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string UserCode)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_Users");
			strSql.Append(" where UserCode=@UserCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6)			};
			parameters[0].Value = UserCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.HY_BMS_Users model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_Users(");
			strSql.Append("UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID)");
			strSql.Append(" values (");
			strSql.Append("@UserCode,@UserName,@CName,@EName,@PassWord,@PhoneNo,@Email,@Sex,@Captcha,@Captchadatetime,@defaultSpras,@IsUse,@DefaultAirPortID)");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@CName", SqlDbType.NVarChar,100),
					new SqlParameter("@EName", SqlDbType.NVarChar,100),
					new SqlParameter("@PassWord", SqlDbType.VarChar,50),
					new SqlParameter("@PhoneNo", SqlDbType.VarChar,50),
					new SqlParameter("@Email", SqlDbType.VarChar,50),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Captcha", SqlDbType.VarChar,20),
					new SqlParameter("@Captchadatetime", SqlDbType.DateTime),
					new SqlParameter("@defaultSpras", SqlDbType.NChar,10),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@DefaultAirPortID", SqlDbType.VarChar,10)};
			parameters[0].Value = model.UserCode;
			parameters[1].Value = model.UserName;
			parameters[2].Value = model.CName;
			parameters[3].Value = model.EName;
			parameters[4].Value = model.PassWord;
			parameters[5].Value = model.PhoneNo;
			parameters[6].Value = model.Email;
			parameters[7].Value = model.Sex;
			parameters[8].Value = model.Captcha;
			parameters[9].Value = model.Captchadatetime;
			parameters[10].Value = model.defaultSpras;
			parameters[11].Value = model.IsUse;
			parameters[12].Value = model.DefaultAirPortID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_Users model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_Users set ");
			strSql.Append("UserName=@UserName,");
			strSql.Append("CName=@CName,");
			strSql.Append("EName=@EName,");
			strSql.Append("PassWord=@PassWord,");
			strSql.Append("PhoneNo=@PhoneNo,");
			strSql.Append("Email=@Email,");
			strSql.Append("Sex=@Sex,");
			strSql.Append("Captcha=@Captcha,");
			strSql.Append("Captchadatetime=@Captchadatetime,");
			strSql.Append("defaultSpras=@defaultSpras,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("DefaultAirPortID=@DefaultAirPortID");
			strSql.Append(" where UserCode=@UserCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@CName", SqlDbType.NVarChar,100),
					new SqlParameter("@EName", SqlDbType.NVarChar,100),
					new SqlParameter("@PassWord", SqlDbType.VarChar,50),
					new SqlParameter("@PhoneNo", SqlDbType.VarChar,50),
					new SqlParameter("@Email", SqlDbType.VarChar,50),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Captcha", SqlDbType.VarChar,20),
					new SqlParameter("@Captchadatetime", SqlDbType.DateTime),
					new SqlParameter("@defaultSpras", SqlDbType.NChar,10),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@DefaultAirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@UserCode", SqlDbType.VarChar,6)};
			parameters[0].Value = model.UserName;
			parameters[1].Value = model.CName;
			parameters[2].Value = model.EName;
			parameters[3].Value = model.PassWord;
			parameters[4].Value = model.PhoneNo;
			parameters[5].Value = model.Email;
			parameters[6].Value = model.Sex;
			parameters[7].Value = model.Captcha;
			parameters[8].Value = model.Captchadatetime;
			parameters[9].Value = model.defaultSpras;
			parameters[10].Value = model.IsUse;
			parameters[11].Value = model.DefaultAirPortID;
			parameters[12].Value = model.UserCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string UserCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_Users ");
			strSql.Append(" where UserCode=@UserCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6)			};
			parameters[0].Value = UserCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string UserCodelist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_Users ");
			strSql.Append(" where UserCode in ("+UserCodelist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Users GetModel(string UserCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID from HY_BMS_Users ");
			strSql.Append(" where UserCode=@UserCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6)			};
			parameters[0].Value = UserCode;

			HBMS.Model.HY_BMS_Users model=new HBMS.Model.HY_BMS_Users();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Users DataRowToModel(DataRow row)
		{
			HBMS.Model.HY_BMS_Users model=new HBMS.Model.HY_BMS_Users();
			if (row != null)
			{
				if(row["UserCode"]!=null)
				{
					model.UserCode=row["UserCode"].ToString();
				}
				if(row["UserName"]!=null)
				{
					model.UserName=row["UserName"].ToString();
				}
				if(row["CName"]!=null)
				{
					model.CName=row["CName"].ToString();
				}
				if(row["EName"]!=null)
				{
					model.EName=row["EName"].ToString();
				}
				if(row["PassWord"]!=null)
				{
					model.PassWord=row["PassWord"].ToString();
				}
				if(row["PhoneNo"]!=null)
				{
					model.PhoneNo=row["PhoneNo"].ToString();
				}
				if(row["Email"]!=null)
				{
					model.Email=row["Email"].ToString();
				}
				if(row["Sex"]!=null && row["Sex"].ToString()!="")
				{
					model.Sex=int.Parse(row["Sex"].ToString());
				}
				if(row["Captcha"]!=null)
				{
					model.Captcha=row["Captcha"].ToString();
				}
				if(row["Captchadatetime"]!=null && row["Captchadatetime"].ToString()!="")
				{
					model.Captchadatetime=DateTime.Parse(row["Captchadatetime"].ToString());
				}
				if(row["defaultSpras"]!=null)
				{
					model.defaultSpras=row["defaultSpras"].ToString();
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["DefaultAirPortID"]!=null)
				{
					model.DefaultAirPortID=row["DefaultAirPortID"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID ");
			strSql.Append(" FROM HY_BMS_Users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID ");
			strSql.Append(" FROM HY_BMS_Users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM HY_BMS_Users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.UserCode desc");
			}
			strSql.Append(")AS Row, T.*  from HY_BMS_Users T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "HY_BMS_Users";
			parameters[1].Value = "UserCode";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

