﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 23:29:50   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_EquipmentDesc
	/// </summary>
	public partial class View_EquipmentDesc
	{
		public View_EquipmentDesc()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_EquipmentDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_EquipmentDesc(");
			strSql.Append("EquipmentID,EquipmentCode,EquipmentName,SPRAS,AirPortID,AirPortCode,AirPortName,AirPortAddress,Guarantee,ProductDate,UserdDate,IsUse,Memo,AirPortPromptUser,AirPortPromptPhone,AirPortWarningUser,AirPortWarningPhone,AirPortErrorUser,AirPortErrorPhone,CompanyPromptUser,CompanyPromptPhone,CompanyWarningUser,CompanyWarningPhone,CompanyErrorUser,CompanyErrorPhone)");
			strSql.Append(" values (");
			strSql.Append("@EquipmentID,@EquipmentCode,@EquipmentName,@SPRAS,@AirPortID,@AirPortCode,@AirPortName,@AirPortAddress,@Guarantee,@ProductDate,@UserdDate,@IsUse,@Memo,@AirPortPromptUser,@AirPortPromptPhone,@AirPortWarningUser,@AirPortWarningPhone,@AirPortErrorUser,@AirPortErrorPhone,@CompanyPromptUser,@CompanyPromptPhone,@CompanyWarningUser,@CompanyWarningPhone,@CompanyErrorUser,@CompanyErrorPhone)");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@EquipmentCode", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@AirPortAddress", SqlDbType.NVarChar,500),
					new SqlParameter("@Guarantee", SqlDbType.Int,4),
					new SqlParameter("@ProductDate", SqlDbType.DateTime),
					new SqlParameter("@UserdDate", SqlDbType.DateTime),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@Memo", SqlDbType.VarChar,500),
					new SqlParameter("@AirPortPromptUser", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortPromptPhone", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortWarningUser", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortWarningPhone", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortErrorUser", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortErrorPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyPromptUser", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyPromptPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyWarningUser", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyWarningPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyErrorUser", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyErrorPhone", SqlDbType.VarChar,20)};
			parameters[0].Value = model.EquipmentID;
			parameters[1].Value = model.EquipmentCode;
			parameters[2].Value = model.EquipmentName;
			parameters[3].Value = model.SPRAS;
			parameters[4].Value = model.AirPortID;
			parameters[5].Value = model.AirPortCode;
			parameters[6].Value = model.AirPortName;
			parameters[7].Value = model.AirPortAddress;
			parameters[8].Value = model.Guarantee;
			parameters[9].Value = model.ProductDate;
			parameters[10].Value = model.UserdDate;
			parameters[11].Value = model.IsUse;
			parameters[12].Value = model.Memo;
			parameters[13].Value = model.AirPortPromptUser;
			parameters[14].Value = model.AirPortPromptPhone;
			parameters[15].Value = model.AirPortWarningUser;
			parameters[16].Value = model.AirPortWarningPhone;
			parameters[17].Value = model.AirPortErrorUser;
			parameters[18].Value = model.AirPortErrorPhone;
			parameters[19].Value = model.CompanyPromptUser;
			parameters[20].Value = model.CompanyPromptPhone;
			parameters[21].Value = model.CompanyWarningUser;
			parameters[22].Value = model.CompanyWarningPhone;
			parameters[23].Value = model.CompanyErrorUser;
			parameters[24].Value = model.CompanyErrorPhone;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_EquipmentDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_EquipmentDesc set ");
			strSql.Append("EquipmentID=@EquipmentID,");
			strSql.Append("EquipmentCode=@EquipmentCode,");
			strSql.Append("EquipmentName=@EquipmentName,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("AirPortCode=@AirPortCode,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("AirPortAddress=@AirPortAddress,");
			strSql.Append("Guarantee=@Guarantee,");
			strSql.Append("ProductDate=@ProductDate,");
			strSql.Append("UserdDate=@UserdDate,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("Memo=@Memo,");
			strSql.Append("AirPortPromptUser=@AirPortPromptUser,");
			strSql.Append("AirPortPromptPhone=@AirPortPromptPhone,");
			strSql.Append("AirPortWarningUser=@AirPortWarningUser,");
			strSql.Append("AirPortWarningPhone=@AirPortWarningPhone,");
			strSql.Append("AirPortErrorUser=@AirPortErrorUser,");
			strSql.Append("AirPortErrorPhone=@AirPortErrorPhone,");
			strSql.Append("CompanyPromptUser=@CompanyPromptUser,");
			strSql.Append("CompanyPromptPhone=@CompanyPromptPhone,");
			strSql.Append("CompanyWarningUser=@CompanyWarningUser,");
			strSql.Append("CompanyWarningPhone=@CompanyWarningPhone,");
			strSql.Append("CompanyErrorUser=@CompanyErrorUser,");
			strSql.Append("CompanyErrorPhone=@CompanyErrorPhone");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@EquipmentCode", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@AirPortAddress", SqlDbType.NVarChar,500),
					new SqlParameter("@Guarantee", SqlDbType.Int,4),
					new SqlParameter("@ProductDate", SqlDbType.DateTime),
					new SqlParameter("@UserdDate", SqlDbType.DateTime),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@Memo", SqlDbType.VarChar,500),
					new SqlParameter("@AirPortPromptUser", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortPromptPhone", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortWarningUser", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortWarningPhone", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortErrorUser", SqlDbType.VarChar,20),
					new SqlParameter("@AirPortErrorPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyPromptUser", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyPromptPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyWarningUser", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyWarningPhone", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyErrorUser", SqlDbType.VarChar,20),
					new SqlParameter("@CompanyErrorPhone", SqlDbType.VarChar,20)};
			parameters[0].Value = model.EquipmentID;
			parameters[1].Value = model.EquipmentCode;
			parameters[2].Value = model.EquipmentName;
			parameters[3].Value = model.SPRAS;
			parameters[4].Value = model.AirPortID;
			parameters[5].Value = model.AirPortCode;
			parameters[6].Value = model.AirPortName;
			parameters[7].Value = model.AirPortAddress;
			parameters[8].Value = model.Guarantee;
			parameters[9].Value = model.ProductDate;
			parameters[10].Value = model.UserdDate;
			parameters[11].Value = model.IsUse;
			parameters[12].Value = model.Memo;
			parameters[13].Value = model.AirPortPromptUser;
			parameters[14].Value = model.AirPortPromptPhone;
			parameters[15].Value = model.AirPortWarningUser;
			parameters[16].Value = model.AirPortWarningPhone;
			parameters[17].Value = model.AirPortErrorUser;
			parameters[18].Value = model.AirPortErrorPhone;
			parameters[19].Value = model.CompanyPromptUser;
			parameters[20].Value = model.CompanyPromptPhone;
			parameters[21].Value = model.CompanyWarningUser;
			parameters[22].Value = model.CompanyWarningPhone;
			parameters[23].Value = model.CompanyErrorUser;
			parameters[24].Value = model.CompanyErrorPhone;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_EquipmentDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentDesc GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 EquipmentID,EquipmentCode,EquipmentName,SPRAS,AirPortID,AirPortCode,AirPortName,AirPortAddress,Guarantee,ProductDate,UserdDate,IsUse,Memo,AirPortPromptUser,AirPortPromptPhone,AirPortWarningUser,AirPortWarningPhone,AirPortErrorUser,AirPortErrorPhone,CompanyPromptUser,CompanyPromptPhone,CompanyWarningUser,CompanyWarningPhone,CompanyErrorUser,CompanyErrorPhone from View_EquipmentDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_EquipmentDesc model=new HBMS.Model.View_EquipmentDesc();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentDesc DataRowToModel(DataRow row)
		{
			HBMS.Model.View_EquipmentDesc model=new HBMS.Model.View_EquipmentDesc();
			if (row != null)
			{
				if(row["EquipmentID"]!=null && row["EquipmentID"].ToString()!="")
				{
					model.EquipmentID=int.Parse(row["EquipmentID"].ToString());
				}
				if(row["EquipmentCode"]!=null)
				{
					model.EquipmentCode=row["EquipmentCode"].ToString();
				}
				if(row["EquipmentName"]!=null)
				{
					model.EquipmentName=row["EquipmentName"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["AirPortID"]!=null)
				{
					model.AirPortID=row["AirPortID"].ToString();
				}
				if(row["AirPortCode"]!=null)
				{
					model.AirPortCode=row["AirPortCode"].ToString();
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["AirPortAddress"]!=null)
				{
					model.AirPortAddress=row["AirPortAddress"].ToString();
				}
				if(row["Guarantee"]!=null && row["Guarantee"].ToString()!="")
				{
					model.Guarantee=int.Parse(row["Guarantee"].ToString());
				}
				if(row["ProductDate"]!=null && row["ProductDate"].ToString()!="")
				{
					model.ProductDate=DateTime.Parse(row["ProductDate"].ToString());
				}
				if(row["UserdDate"]!=null && row["UserdDate"].ToString()!="")
				{
					model.UserdDate=DateTime.Parse(row["UserdDate"].ToString());
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["Memo"]!=null)
				{
					model.Memo=row["Memo"].ToString();
				}
				if(row["AirPortPromptUser"]!=null)
				{
					model.AirPortPromptUser=row["AirPortPromptUser"].ToString();
				}
				if(row["AirPortPromptPhone"]!=null)
				{
					model.AirPortPromptPhone=row["AirPortPromptPhone"].ToString();
				}
				if(row["AirPortWarningUser"]!=null)
				{
					model.AirPortWarningUser=row["AirPortWarningUser"].ToString();
				}
				if(row["AirPortWarningPhone"]!=null)
				{
					model.AirPortWarningPhone=row["AirPortWarningPhone"].ToString();
				}
				if(row["AirPortErrorUser"]!=null)
				{
					model.AirPortErrorUser=row["AirPortErrorUser"].ToString();
				}
				if(row["AirPortErrorPhone"]!=null)
				{
					model.AirPortErrorPhone=row["AirPortErrorPhone"].ToString();
				}
				if(row["CompanyPromptUser"]!=null)
				{
					model.CompanyPromptUser=row["CompanyPromptUser"].ToString();
				}
				if(row["CompanyPromptPhone"]!=null)
				{
					model.CompanyPromptPhone=row["CompanyPromptPhone"].ToString();
				}
				if(row["CompanyWarningUser"]!=null)
				{
					model.CompanyWarningUser=row["CompanyWarningUser"].ToString();
				}
				if(row["CompanyWarningPhone"]!=null)
				{
					model.CompanyWarningPhone=row["CompanyWarningPhone"].ToString();
				}
				if(row["CompanyErrorUser"]!=null)
				{
					model.CompanyErrorUser=row["CompanyErrorUser"].ToString();
				}
				if(row["CompanyErrorPhone"]!=null)
				{
					model.CompanyErrorPhone=row["CompanyErrorPhone"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select EquipmentID,EquipmentCode,EquipmentName,SPRAS,AirPortID,AirPortCode,AirPortName,AirPortAddress,Guarantee,ProductDate,UserdDate,IsUse,Memo,AirPortPromptUser,AirPortPromptPhone,AirPortWarningUser,AirPortWarningPhone,AirPortErrorUser,AirPortErrorPhone,CompanyPromptUser,CompanyPromptPhone,CompanyWarningUser,CompanyWarningPhone,CompanyErrorUser,CompanyErrorPhone ");
			strSql.Append(" FROM View_EquipmentDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" EquipmentID,EquipmentCode,EquipmentName,SPRAS,AirPortID,AirPortCode,AirPortName,AirPortAddress,Guarantee,ProductDate,UserdDate,IsUse,Memo,AirPortPromptUser,AirPortPromptPhone,AirPortWarningUser,AirPortWarningPhone,AirPortErrorUser,AirPortErrorPhone,CompanyPromptUser,CompanyPromptPhone,CompanyWarningUser,CompanyWarningPhone,CompanyErrorUser,CompanyErrorPhone ");
			strSql.Append(" FROM View_EquipmentDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_EquipmentDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SPRAS desc");
			}
			strSql.Append(")AS Row, T.*  from View_EquipmentDesc T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_EquipmentDesc";
			parameters[1].Value = "SPRAS";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

