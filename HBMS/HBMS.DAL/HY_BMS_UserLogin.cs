﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_UserLogin
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:20   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:HY_BMS_UserLogin
	/// </summary>
	public partial class HY_BMS_UserLogin
	{
		public HY_BMS_UserLogin()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_UserLogin");
			strSql.Append(" where GUID=@GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.VarChar,100)			};
			parameters[0].Value = GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.HY_BMS_UserLogin model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_UserLogin(");
			strSql.Append("GUID,UserCode,MAC,IP,SPRAS,LoginDateTime,ModifyDateTime,QuitDateTime)");
			strSql.Append(" values (");
			strSql.Append("@GUID,@UserCode,@MAC,@IP,@SPRAS,@LoginDateTime,@ModifyDateTime,@QuitDateTime)");
			SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.VarChar,100),
					new SqlParameter("@UserCode", SqlDbType.VarChar,6),
					new SqlParameter("@MAC", SqlDbType.VarChar,50),
					new SqlParameter("@IP", SqlDbType.VarChar,50),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@LoginDateTime", SqlDbType.DateTime),
					new SqlParameter("@ModifyDateTime", SqlDbType.DateTime),
					new SqlParameter("@QuitDateTime", SqlDbType.DateTime)};
			parameters[0].Value = model.GUID;
			parameters[1].Value = model.UserCode;
			parameters[2].Value = model.MAC;
			parameters[3].Value = model.IP;
			parameters[4].Value = model.SPRAS;
			parameters[5].Value = model.LoginDateTime;
			parameters[6].Value = model.ModifyDateTime;
			parameters[7].Value = model.QuitDateTime;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_UserLogin model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_UserLogin set ");
			strSql.Append("UserCode=@UserCode,");
			strSql.Append("MAC=@MAC,");
			strSql.Append("IP=@IP,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("LoginDateTime=@LoginDateTime,");
			strSql.Append("ModifyDateTime=@ModifyDateTime,");
			strSql.Append("QuitDateTime=@QuitDateTime");
			strSql.Append(" where GUID=@GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6),
					new SqlParameter("@MAC", SqlDbType.VarChar,50),
					new SqlParameter("@IP", SqlDbType.VarChar,50),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@LoginDateTime", SqlDbType.DateTime),
					new SqlParameter("@ModifyDateTime", SqlDbType.DateTime),
					new SqlParameter("@QuitDateTime", SqlDbType.DateTime),
					new SqlParameter("@GUID", SqlDbType.VarChar,100)};
			parameters[0].Value = model.UserCode;
			parameters[1].Value = model.MAC;
			parameters[2].Value = model.IP;
			parameters[3].Value = model.SPRAS;
			parameters[4].Value = model.LoginDateTime;
			parameters[5].Value = model.ModifyDateTime;
			parameters[6].Value = model.QuitDateTime;
			parameters[7].Value = model.GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_UserLogin ");
			strSql.Append(" where GUID=@GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.VarChar,100)			};
			parameters[0].Value = GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string GUIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_UserLogin ");
			strSql.Append(" where GUID in ("+GUIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_UserLogin GetModel(string GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 GUID,UserCode,MAC,IP,SPRAS,LoginDateTime,ModifyDateTime,QuitDateTime from HY_BMS_UserLogin ");
			strSql.Append(" where GUID=@GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.VarChar,100)			};
			parameters[0].Value = GUID;

			HBMS.Model.HY_BMS_UserLogin model=new HBMS.Model.HY_BMS_UserLogin();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_UserLogin DataRowToModel(DataRow row)
		{
			HBMS.Model.HY_BMS_UserLogin model=new HBMS.Model.HY_BMS_UserLogin();
			if (row != null)
			{
				if(row["GUID"]!=null)
				{
					model.GUID=row["GUID"].ToString();
				}
				if(row["UserCode"]!=null)
				{
					model.UserCode=row["UserCode"].ToString();
				}
				if(row["MAC"]!=null)
				{
					model.MAC=row["MAC"].ToString();
				}
				if(row["IP"]!=null)
				{
					model.IP=row["IP"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["LoginDateTime"]!=null && row["LoginDateTime"].ToString()!="")
				{
					model.LoginDateTime=DateTime.Parse(row["LoginDateTime"].ToString());
				}
				if(row["ModifyDateTime"]!=null && row["ModifyDateTime"].ToString()!="")
				{
					model.ModifyDateTime=DateTime.Parse(row["ModifyDateTime"].ToString());
				}
				if(row["QuitDateTime"]!=null && row["QuitDateTime"].ToString()!="")
				{
					model.QuitDateTime=DateTime.Parse(row["QuitDateTime"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select GUID,UserCode,MAC,IP,SPRAS,LoginDateTime,ModifyDateTime,QuitDateTime ");
			strSql.Append(" FROM HY_BMS_UserLogin ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" GUID,UserCode,MAC,IP,SPRAS,LoginDateTime,ModifyDateTime,QuitDateTime ");
			strSql.Append(" FROM HY_BMS_UserLogin ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM HY_BMS_UserLogin ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.GUID desc");
			}
			strSql.Append(")AS Row, T.*  from HY_BMS_UserLogin T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "HY_BMS_UserLogin";
			parameters[1].Value = "GUID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

