﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_EquipmentDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 23:25:34   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:HY_BMS_EquipmentDesc
	/// </summary>
	public partial class HY_BMS_EquipmentDesc
	{
		public HY_BMS_EquipmentDesc()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("EquipmentID", "HY_BMS_EquipmentDesc"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int EquipmentID,int SPRAS)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_EquipmentDesc");
			strSql.Append(" where EquipmentID=@EquipmentID and SPRAS=@SPRAS ");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@SPRAS", SqlDbType.Int,4)			};
			parameters[0].Value = EquipmentID;
			parameters[1].Value = SPRAS;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.HY_BMS_EquipmentDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_EquipmentDesc(");
			strSql.Append("EquipmentID,SPRAS,EquipmentName)");
			strSql.Append(" values (");
			strSql.Append("@EquipmentID,@SPRAS,@EquipmentName)");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500)};
			parameters[0].Value = model.EquipmentID;
			parameters[1].Value = model.SPRAS;
			parameters[2].Value = model.EquipmentName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_EquipmentDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_EquipmentDesc set ");
			strSql.Append("EquipmentName=@EquipmentName");
			strSql.Append(" where EquipmentID=@EquipmentID and SPRAS=@SPRAS ");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@SPRAS", SqlDbType.Int,4)};
			parameters[0].Value = model.EquipmentName;
			parameters[1].Value = model.EquipmentID;
			parameters[2].Value = model.SPRAS;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int EquipmentID,int SPRAS)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_EquipmentDesc ");
			strSql.Append(" where EquipmentID=@EquipmentID and SPRAS=@SPRAS ");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@SPRAS", SqlDbType.Int,4)			};
			parameters[0].Value = EquipmentID;
			parameters[1].Value = SPRAS;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_EquipmentDesc GetModel(int EquipmentID,int SPRAS)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 EquipmentID,SPRAS,EquipmentName from HY_BMS_EquipmentDesc ");
			strSql.Append(" where EquipmentID=@EquipmentID and SPRAS=@SPRAS ");
			SqlParameter[] parameters = {
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@SPRAS", SqlDbType.Int,4)			};
			parameters[0].Value = EquipmentID;
			parameters[1].Value = SPRAS;

			HBMS.Model.HY_BMS_EquipmentDesc model=new HBMS.Model.HY_BMS_EquipmentDesc();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_EquipmentDesc DataRowToModel(DataRow row)
		{
			HBMS.Model.HY_BMS_EquipmentDesc model=new HBMS.Model.HY_BMS_EquipmentDesc();
			if (row != null)
			{
				if(row["EquipmentID"]!=null && row["EquipmentID"].ToString()!="")
				{
					model.EquipmentID=int.Parse(row["EquipmentID"].ToString());
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["EquipmentName"]!=null)
				{
					model.EquipmentName=row["EquipmentName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select EquipmentID,SPRAS,EquipmentName ");
			strSql.Append(" FROM HY_BMS_EquipmentDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" EquipmentID,SPRAS,EquipmentName ");
			strSql.Append(" FROM HY_BMS_EquipmentDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM HY_BMS_EquipmentDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SPRAS desc");
			}
			strSql.Append(")AS Row, T.*  from HY_BMS_EquipmentDesc T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "HY_BMS_EquipmentDesc";
			parameters[1].Value = "SPRAS";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

