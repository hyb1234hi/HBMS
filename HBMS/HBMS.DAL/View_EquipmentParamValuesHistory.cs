﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentParamValuesHistory
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/31 0:47:23   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_EquipmentParamValuesHistory
	/// </summary>
	public partial class View_EquipmentParamValuesHistory
	{
		public View_EquipmentParamValuesHistory()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_EquipmentParamValuesHistory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_EquipmentParamValuesHistory(");
			strSql.Append("ID,SubDatetime,ParamValue,ParamState,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName)");
			strSql.Append(" values (");
			strSql.Append("@ID,@SubDatetime,@ParamValue,@ParamState,@AirPortID,@AirPortCode,@AirPortName,@Dimension,@Longitude,@EquipmentID,@EquipmentCode,@EquipmentName,@SPRAS,@ParamID,@ParamCode,@ParamName,@ParamTypeID,@boolValue,@PromptMin,@PromptMax,@WarningMin,@WarningMax,@ErrorMin,@ErrorMax,@PartsID,@PartsName,@SystemID,@SystemName)");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@SubDatetime", SqlDbType.DateTime),
					new SqlParameter("@ParamValue", SqlDbType.Decimal,9),
					new SqlParameter("@ParamState", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@EquipmentCode", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.Int,4),
					new SqlParameter("@ParamCode", SqlDbType.VarChar,50),
					new SqlParameter("@ParamName", SqlDbType.NChar,10),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.SubDatetime;
			parameters[2].Value = model.ParamValue;
			parameters[3].Value = model.ParamState;
			parameters[4].Value = model.AirPortID;
			parameters[5].Value = model.AirPortCode;
			parameters[6].Value = model.AirPortName;
			parameters[7].Value = model.Dimension;
			parameters[8].Value = model.Longitude;
			parameters[9].Value = model.EquipmentID;
			parameters[10].Value = model.EquipmentCode;
			parameters[11].Value = model.EquipmentName;
			parameters[12].Value = model.SPRAS;
			parameters[13].Value = model.ParamID;
			parameters[14].Value = model.ParamCode;
			parameters[15].Value = model.ParamName;
			parameters[16].Value = model.ParamTypeID;
			parameters[17].Value = model.boolValue;
			parameters[18].Value = model.PromptMin;
			parameters[19].Value = model.PromptMax;
			parameters[20].Value = model.WarningMin;
			parameters[21].Value = model.WarningMax;
			parameters[22].Value = model.ErrorMin;
			parameters[23].Value = model.ErrorMax;
			parameters[24].Value = model.PartsID;
			parameters[25].Value = model.PartsName;
			parameters[26].Value = model.SystemID;
			parameters[27].Value = model.SystemName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_EquipmentParamValuesHistory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_EquipmentParamValuesHistory set ");
			strSql.Append("ID=@ID,");
			strSql.Append("SubDatetime=@SubDatetime,");
			strSql.Append("ParamValue=@ParamValue,");
			strSql.Append("ParamState=@ParamState,");
			strSql.Append("AirPortID=@AirPortID,");
			strSql.Append("AirPortCode=@AirPortCode,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("Dimension=@Dimension,");
			strSql.Append("Longitude=@Longitude,");
			strSql.Append("EquipmentID=@EquipmentID,");
			strSql.Append("EquipmentCode=@EquipmentCode,");
			strSql.Append("EquipmentName=@EquipmentName,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("ParamID=@ParamID,");
			strSql.Append("ParamCode=@ParamCode,");
			strSql.Append("ParamName=@ParamName,");
			strSql.Append("ParamTypeID=@ParamTypeID,");
			strSql.Append("boolValue=@boolValue,");
			strSql.Append("PromptMin=@PromptMin,");
			strSql.Append("PromptMax=@PromptMax,");
			strSql.Append("WarningMin=@WarningMin,");
			strSql.Append("WarningMax=@WarningMax,");
			strSql.Append("ErrorMin=@ErrorMin,");
			strSql.Append("ErrorMax=@ErrorMax,");
			strSql.Append("PartsID=@PartsID,");
			strSql.Append("PartsName=@PartsName,");
			strSql.Append("SystemID=@SystemID,");
			strSql.Append("SystemName=@SystemName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@SubDatetime", SqlDbType.DateTime),
					new SqlParameter("@ParamValue", SqlDbType.Decimal,9),
					new SqlParameter("@ParamState", SqlDbType.Int,4),
					new SqlParameter("@AirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@EquipmentID", SqlDbType.Int,4),
					new SqlParameter("@EquipmentCode", SqlDbType.VarChar,10),
					new SqlParameter("@EquipmentName", SqlDbType.VarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.Int,4),
					new SqlParameter("@ParamCode", SqlDbType.VarChar,50),
					new SqlParameter("@ParamName", SqlDbType.NChar,10),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.SubDatetime;
			parameters[2].Value = model.ParamValue;
			parameters[3].Value = model.ParamState;
			parameters[4].Value = model.AirPortID;
			parameters[5].Value = model.AirPortCode;
			parameters[6].Value = model.AirPortName;
			parameters[7].Value = model.Dimension;
			parameters[8].Value = model.Longitude;
			parameters[9].Value = model.EquipmentID;
			parameters[10].Value = model.EquipmentCode;
			parameters[11].Value = model.EquipmentName;
			parameters[12].Value = model.SPRAS;
			parameters[13].Value = model.ParamID;
			parameters[14].Value = model.ParamCode;
			parameters[15].Value = model.ParamName;
			parameters[16].Value = model.ParamTypeID;
			parameters[17].Value = model.boolValue;
			parameters[18].Value = model.PromptMin;
			parameters[19].Value = model.PromptMax;
			parameters[20].Value = model.WarningMin;
			parameters[21].Value = model.WarningMax;
			parameters[22].Value = model.ErrorMin;
			parameters[23].Value = model.ErrorMax;
			parameters[24].Value = model.PartsID;
			parameters[25].Value = model.PartsName;
			parameters[26].Value = model.SystemID;
			parameters[27].Value = model.SystemName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_EquipmentParamValuesHistory ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentParamValuesHistory GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,SubDatetime,ParamValue,ParamState,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName from View_EquipmentParamValuesHistory ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_EquipmentParamValuesHistory model=new HBMS.Model.View_EquipmentParamValuesHistory();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_EquipmentParamValuesHistory DataRowToModel(DataRow row)
		{
			HBMS.Model.View_EquipmentParamValuesHistory model=new HBMS.Model.View_EquipmentParamValuesHistory();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["SubDatetime"]!=null && row["SubDatetime"].ToString()!="")
				{
					model.SubDatetime=DateTime.Parse(row["SubDatetime"].ToString());
				}
				if(row["ParamValue"]!=null && row["ParamValue"].ToString()!="")
				{
					model.ParamValue=decimal.Parse(row["ParamValue"].ToString());
				}
				if(row["ParamState"]!=null && row["ParamState"].ToString()!="")
				{
					model.ParamState=int.Parse(row["ParamState"].ToString());
				}
				if(row["AirPortID"]!=null)
				{
					model.AirPortID=row["AirPortID"].ToString();
				}
				if(row["AirPortCode"]!=null)
				{
					model.AirPortCode=row["AirPortCode"].ToString();
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["Dimension"]!=null)
				{
					model.Dimension=row["Dimension"].ToString();
				}
				if(row["Longitude"]!=null)
				{
					model.Longitude=row["Longitude"].ToString();
				}
				if(row["EquipmentID"]!=null && row["EquipmentID"].ToString()!="")
				{
					model.EquipmentID=int.Parse(row["EquipmentID"].ToString());
				}
				if(row["EquipmentCode"]!=null)
				{
					model.EquipmentCode=row["EquipmentCode"].ToString();
				}
				if(row["EquipmentName"]!=null)
				{
					model.EquipmentName=row["EquipmentName"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["ParamID"]!=null && row["ParamID"].ToString()!="")
				{
					model.ParamID=int.Parse(row["ParamID"].ToString());
				}
				if(row["ParamCode"]!=null)
				{
					model.ParamCode=row["ParamCode"].ToString();
				}
				if(row["ParamName"]!=null)
				{
					model.ParamName=row["ParamName"].ToString();
				}
				if(row["ParamTypeID"]!=null && row["ParamTypeID"].ToString()!="")
				{
					model.ParamTypeID=int.Parse(row["ParamTypeID"].ToString());
				}
				if(row["boolValue"]!=null && row["boolValue"].ToString()!="")
				{
					model.boolValue=decimal.Parse(row["boolValue"].ToString());
				}
				if(row["PromptMin"]!=null && row["PromptMin"].ToString()!="")
				{
					model.PromptMin=decimal.Parse(row["PromptMin"].ToString());
				}
				if(row["PromptMax"]!=null && row["PromptMax"].ToString()!="")
				{
					model.PromptMax=decimal.Parse(row["PromptMax"].ToString());
				}
				if(row["WarningMin"]!=null && row["WarningMin"].ToString()!="")
				{
					model.WarningMin=decimal.Parse(row["WarningMin"].ToString());
				}
				if(row["WarningMax"]!=null && row["WarningMax"].ToString()!="")
				{
					model.WarningMax=decimal.Parse(row["WarningMax"].ToString());
				}
				if(row["ErrorMin"]!=null && row["ErrorMin"].ToString()!="")
				{
					model.ErrorMin=decimal.Parse(row["ErrorMin"].ToString());
				}
				if(row["ErrorMax"]!=null && row["ErrorMax"].ToString()!="")
				{
					model.ErrorMax=decimal.Parse(row["ErrorMax"].ToString());
				}
				if(row["PartsID"]!=null && row["PartsID"].ToString()!="")
				{
					model.PartsID=int.Parse(row["PartsID"].ToString());
				}
				if(row["PartsName"]!=null)
				{
					model.PartsName=row["PartsName"].ToString();
				}
				if(row["SystemID"]!=null && row["SystemID"].ToString()!="")
				{
					model.SystemID=int.Parse(row["SystemID"].ToString());
				}
				if(row["SystemName"]!=null)
				{
					model.SystemName=row["SystemName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,SubDatetime,ParamValue,ParamState,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName ");
			strSql.Append(" FROM View_EquipmentParamValuesHistory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,SubDatetime,ParamValue,ParamState,AirPortID,AirPortCode,AirPortName,Dimension,Longitude,EquipmentID,EquipmentCode,EquipmentName,SPRAS,ParamID,ParamCode,ParamName,ParamTypeID,boolValue,PromptMin,PromptMax,WarningMin,WarningMax,ErrorMin,ErrorMax,PartsID,PartsName,SystemID,SystemName ");
			strSql.Append(" FROM View_EquipmentParamValuesHistory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_EquipmentParamValuesHistory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_EquipmentParamValuesHistory T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_EquipmentParamValuesHistory";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

