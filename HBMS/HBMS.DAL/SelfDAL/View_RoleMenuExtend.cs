﻿// 
// 创建人：宋欣
// 创建时间：2015-03-06
// 功能：角色菜单
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Data;
using System.Data.SqlClient;
using HBMS.DBUtility;

namespace HBMS.DAL
{
    public partial class View_RoleMenu
    {
        /// <summary>
        /// 新增角色菜单
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="menus"></param>
        /// <returns></returns>
        public bool AddRoleMenu(int roleID, string menus)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleID", SqlDbType.Int),
                    new SqlParameter("@Menus", SqlDbType.VarChar)
                };
            parameters[0].Value = roleID;
            parameters[1].Value = menus;
            int rowsAffected = 0;
            DbHelperSQL.RunProcedure("proc_AddRoleMenu", parameters, out rowsAffected);
            return rowsAffected > 0;
        }
    }
}