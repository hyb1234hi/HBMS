﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HBMS.DBUtility;

namespace HBMS.DAL
{
    public partial class HY_BMS_AirPortDesc
    {
        /// <summary>
        /// 保存机场多语言
        /// </summary>
        /// <param name="sqllist"></param>
        /// <returns></returns>
        public bool SaveLanguage(List<string> sqllist)
        {
            return DbHelperSQL.ExecuteSqlTran(sqllist) > 0;
        }
    }
}
