﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：用户菜单
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Data.SqlClient;
using HBMS.DBUtility;

namespace HBMS.DAL
{
    public class UserMenu
    {
        /// <summary>
        /// 获取用户菜单
        /// </summary>
        /// <param name="userCode"></param>
        /// <param name="spras"></param>
        /// <returns></returns>
        public DataSet GetUserMenu(string userCode,int spras)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserCode", SqlDbType.VarChar, 10),
                    new SqlParameter("@SPRAS",SqlDbType.Int) 
                };
            parameters[0].Value = userCode;
            parameters[1].Value = spras;
            DataSet ds = DbHelperSQL.RunProcedure("proc_GetUserMenus", parameters, "ds");
            return ds;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public HBMS.Model.HY_BMS_Menu DataRowToModel(DataRow row)
        {
            HBMS.Model.HY_BMS_Menu model = new HBMS.Model.HY_BMS_Menu();
            if (row != null)
            {
                if (row["MenuID"] != null)
                {
                    model.MenuID = row["MenuID"].ToString();
                }
                if (row["ParentID"] != null)
                {
                    model.ParentID = row["ParentID"].ToString();
                }
                if (row["URL"] != null)
                {
                    model.URL = row["URL"].ToString();
                }
                if (row["OrderIndex"] != null && row["OrderIndex"].ToString() != "")
                {
                    model.OrderIndex = int.Parse(row["OrderIndex"].ToString());
                }
                if (row["Memo"] != null)
                {
                    model.Memo = row["Memo"].ToString();
                }
                if (row["IsHide"] != null && row["IsHide"].ToString() != "")
                {
                    if ((row["IsHide"].ToString() == "1") || (row["IsHide"].ToString().ToLower() == "true"))
                    {
                        model.IsHide = true;
                    }
                    else
                    {
                        model.IsHide = false;
                    }
                }
                if (row["IsUse"] != null && row["IsUse"].ToString() != "")
                {
                    if ((row["IsUse"].ToString() == "1") || (row["IsUse"].ToString().ToLower() == "true"))
                    {
                        model.IsUse = true;
                    }
                    else
                    {
                        model.IsUse = false;
                    }
                }
                if (row["spras"] != null)
                {
                    model.CurSpras =Convert.ToInt32(row["spras"].ToString());
                }
                if (row["MenuName"] != null)
                {
                    model.CureMenuName = row["MenuName"].ToString();
                }
            }
            return model;
        }
    }
}