﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_AirPorts
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 22:57:08   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:HY_BMS_AirPorts
	/// </summary>
	public partial class HY_BMS_AirPorts
	{
		public HY_BMS_AirPorts()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("AirPortID", "HY_BMS_AirPorts"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int AirPortID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_AirPorts");
			strSql.Append(" where AirPortID=@AirPortID");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.Int,4)
			};
			parameters[0].Value = AirPortID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(HBMS.Model.HY_BMS_AirPorts model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_AirPorts(");
			strSql.Append("AirPortCode,CountryID,ProvinceID,CityID,Dimension,Longitude,Head,HeadPhone,HeadEmail)");
			strSql.Append(" values (");
			strSql.Append("@AirPortCode,@CountryID,@ProvinceID,@CityID,@Dimension,@Longitude,@Head,@HeadPhone,@HeadEmail)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@CountryID", SqlDbType.VarChar,6),
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,6),
					new SqlParameter("@CityID", SqlDbType.VarChar,6),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@Head", SqlDbType.VarChar,20),
					new SqlParameter("@HeadPhone", SqlDbType.VarChar,20),
					new SqlParameter("@HeadEmail", SqlDbType.VarChar,50)};
			parameters[0].Value = model.AirPortCode;
			parameters[1].Value = model.CountryID;
			parameters[2].Value = model.ProvinceID;
			parameters[3].Value = model.CityID;
			parameters[4].Value = model.Dimension;
			parameters[5].Value = model.Longitude;
			parameters[6].Value = model.Head;
			parameters[7].Value = model.HeadPhone;
			parameters[8].Value = model.HeadEmail;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_AirPorts model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_AirPorts set ");
			strSql.Append("AirPortCode=@AirPortCode,");
			strSql.Append("CountryID=@CountryID,");
			strSql.Append("ProvinceID=@ProvinceID,");
			strSql.Append("CityID=@CityID,");
			strSql.Append("Dimension=@Dimension,");
			strSql.Append("Longitude=@Longitude,");
			strSql.Append("Head=@Head,");
			strSql.Append("HeadPhone=@HeadPhone,");
			strSql.Append("HeadEmail=@HeadEmail");
			strSql.Append(" where AirPortID=@AirPortID");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortCode", SqlDbType.VarChar,10),
					new SqlParameter("@CountryID", SqlDbType.VarChar,6),
					new SqlParameter("@ProvinceID", SqlDbType.VarChar,6),
					new SqlParameter("@CityID", SqlDbType.VarChar,6),
					new SqlParameter("@Dimension", SqlDbType.VarChar,20),
					new SqlParameter("@Longitude", SqlDbType.VarChar,20),
					new SqlParameter("@Head", SqlDbType.VarChar,20),
					new SqlParameter("@HeadPhone", SqlDbType.VarChar,20),
					new SqlParameter("@HeadEmail", SqlDbType.VarChar,50),
					new SqlParameter("@AirPortID", SqlDbType.Int,4)};
			parameters[0].Value = model.AirPortCode;
			parameters[1].Value = model.CountryID;
			parameters[2].Value = model.ProvinceID;
			parameters[3].Value = model.CityID;
			parameters[4].Value = model.Dimension;
			parameters[5].Value = model.Longitude;
			parameters[6].Value = model.Head;
			parameters[7].Value = model.HeadPhone;
			parameters[8].Value = model.HeadEmail;
			parameters[9].Value = model.AirPortID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int AirPortID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_AirPorts ");
			strSql.Append(" where AirPortID=@AirPortID");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.Int,4)
			};
			parameters[0].Value = AirPortID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string AirPortIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_AirPorts ");
			strSql.Append(" where AirPortID in ("+AirPortIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_AirPorts GetModel(int AirPortID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 AirPortID,AirPortCode,CountryID,ProvinceID,CityID,Dimension,Longitude,Head,HeadPhone,HeadEmail from HY_BMS_AirPorts ");
			strSql.Append(" where AirPortID=@AirPortID");
			SqlParameter[] parameters = {
					new SqlParameter("@AirPortID", SqlDbType.Int,4)
			};
			parameters[0].Value = AirPortID;

			HBMS.Model.HY_BMS_AirPorts model=new HBMS.Model.HY_BMS_AirPorts();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_AirPorts DataRowToModel(DataRow row)
		{
			HBMS.Model.HY_BMS_AirPorts model=new HBMS.Model.HY_BMS_AirPorts();
			if (row != null)
			{
				if(row["AirPortID"]!=null && row["AirPortID"].ToString()!="")
				{
					model.AirPortID=int.Parse(row["AirPortID"].ToString());
				}
				if(row["AirPortCode"]!=null)
				{
					model.AirPortCode=row["AirPortCode"].ToString();
				}
				if(row["CountryID"]!=null)
				{
					model.CountryID=row["CountryID"].ToString();
				}
				if(row["ProvinceID"]!=null)
				{
					model.ProvinceID=row["ProvinceID"].ToString();
				}
				if(row["CityID"]!=null)
				{
					model.CityID=row["CityID"].ToString();
				}
				if(row["Dimension"]!=null)
				{
					model.Dimension=row["Dimension"].ToString();
				}
				if(row["Longitude"]!=null)
				{
					model.Longitude=row["Longitude"].ToString();
				}
				if(row["Head"]!=null)
				{
					model.Head=row["Head"].ToString();
				}
				if(row["HeadPhone"]!=null)
				{
					model.HeadPhone=row["HeadPhone"].ToString();
				}
				if(row["HeadEmail"]!=null)
				{
					model.HeadEmail=row["HeadEmail"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select AirPortID,AirPortCode,CountryID,ProvinceID,CityID,Dimension,Longitude,Head,HeadPhone,HeadEmail ");
			strSql.Append(" FROM HY_BMS_AirPorts ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" AirPortID,AirPortCode,CountryID,ProvinceID,CityID,Dimension,Longitude,Head,HeadPhone,HeadEmail ");
			strSql.Append(" FROM HY_BMS_AirPorts ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM HY_BMS_AirPorts ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.AirPortID desc");
			}
			strSql.Append(")AS Row, T.*  from HY_BMS_AirPorts T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "HY_BMS_AirPorts";
			parameters[1].Value = "AirPortID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

