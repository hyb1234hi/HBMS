﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Param
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/4/8 23:17:32   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:HY_BMS_Param
	/// </summary>
	public partial class HY_BMS_Param
	{
		public HY_BMS_Param()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ParamID", "HY_BMS_Param"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ParamID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_Param");
			strSql.Append(" where ParamID=@ParamID");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamID", SqlDbType.Int,4)
			};
			parameters[0].Value = ParamID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(HBMS.Model.HY_BMS_Param model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_Param(");
			strSql.Append("ParamCode,PartsID,ParamTypeID,IsUse,OrderIndex,Memo,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,InfoLevel)");
			strSql.Append(" values (");
			strSql.Append("@ParamCode,@PartsID,@ParamTypeID,@IsUse,@OrderIndex,@Memo,@boolValue,@PromptMin,@PromptMax,@PromptMsgFW,@WarningMin,@WarningMax,@WarningMsgFW,@ErrorMin,@ErrorMax,@ErrorMsgFW,@MacParamID,@InfoLevel)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamCode", SqlDbType.VarChar,50),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@Memo", SqlDbType.VarChar,500),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMsgFW", SqlDbType.Int,4),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMsgFW", SqlDbType.Int,4),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMsgFW", SqlDbType.Int,4),
					new SqlParameter("@MacParamID", SqlDbType.Int,4),
					new SqlParameter("@InfoLevel", SqlDbType.Int,4)};
			parameters[0].Value = model.ParamCode;
			parameters[1].Value = model.PartsID;
			parameters[2].Value = model.ParamTypeID;
			parameters[3].Value = model.IsUse;
			parameters[4].Value = model.OrderIndex;
			parameters[5].Value = model.Memo;
			parameters[6].Value = model.boolValue;
			parameters[7].Value = model.PromptMin;
			parameters[8].Value = model.PromptMax;
			parameters[9].Value = model.PromptMsgFW;
			parameters[10].Value = model.WarningMin;
			parameters[11].Value = model.WarningMax;
			parameters[12].Value = model.WarningMsgFW;
			parameters[13].Value = model.ErrorMin;
			parameters[14].Value = model.ErrorMax;
			parameters[15].Value = model.ErrorMsgFW;
			parameters[16].Value = model.MacParamID;
			parameters[17].Value = model.InfoLevel;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_Param model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_Param set ");
			strSql.Append("ParamCode=@ParamCode,");
			strSql.Append("PartsID=@PartsID,");
			strSql.Append("ParamTypeID=@ParamTypeID,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("OrderIndex=@OrderIndex,");
			strSql.Append("Memo=@Memo,");
			strSql.Append("boolValue=@boolValue,");
			strSql.Append("PromptMin=@PromptMin,");
			strSql.Append("PromptMax=@PromptMax,");
			strSql.Append("PromptMsgFW=@PromptMsgFW,");
			strSql.Append("WarningMin=@WarningMin,");
			strSql.Append("WarningMax=@WarningMax,");
			strSql.Append("WarningMsgFW=@WarningMsgFW,");
			strSql.Append("ErrorMin=@ErrorMin,");
			strSql.Append("ErrorMax=@ErrorMax,");
			strSql.Append("ErrorMsgFW=@ErrorMsgFW,");
			strSql.Append("MacParamID=@MacParamID,");
			strSql.Append("InfoLevel=@InfoLevel");
			strSql.Append(" where ParamID=@ParamID");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamCode", SqlDbType.VarChar,50),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@Memo", SqlDbType.VarChar,500),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMsgFW", SqlDbType.Int,4),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMsgFW", SqlDbType.Int,4),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMsgFW", SqlDbType.Int,4),
					new SqlParameter("@MacParamID", SqlDbType.Int,4),
					new SqlParameter("@InfoLevel", SqlDbType.Int,4),
					new SqlParameter("@ParamID", SqlDbType.Int,4)};
			parameters[0].Value = model.ParamCode;
			parameters[1].Value = model.PartsID;
			parameters[2].Value = model.ParamTypeID;
			parameters[3].Value = model.IsUse;
			parameters[4].Value = model.OrderIndex;
			parameters[5].Value = model.Memo;
			parameters[6].Value = model.boolValue;
			parameters[7].Value = model.PromptMin;
			parameters[8].Value = model.PromptMax;
			parameters[9].Value = model.PromptMsgFW;
			parameters[10].Value = model.WarningMin;
			parameters[11].Value = model.WarningMax;
			parameters[12].Value = model.WarningMsgFW;
			parameters[13].Value = model.ErrorMin;
			parameters[14].Value = model.ErrorMax;
			parameters[15].Value = model.ErrorMsgFW;
			parameters[16].Value = model.MacParamID;
			parameters[17].Value = model.InfoLevel;
			parameters[18].Value = model.ParamID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ParamID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_Param ");
			strSql.Append(" where ParamID=@ParamID");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamID", SqlDbType.Int,4)
			};
			parameters[0].Value = ParamID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ParamIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_Param ");
			strSql.Append(" where ParamID in ("+ParamIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Param GetModel(int ParamID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ParamID,ParamCode,PartsID,ParamTypeID,IsUse,OrderIndex,Memo,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,InfoLevel from HY_BMS_Param ");
			strSql.Append(" where ParamID=@ParamID");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamID", SqlDbType.Int,4)
			};
			parameters[0].Value = ParamID;

			HBMS.Model.HY_BMS_Param model=new HBMS.Model.HY_BMS_Param();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Param DataRowToModel(DataRow row)
		{
			HBMS.Model.HY_BMS_Param model=new HBMS.Model.HY_BMS_Param();
			if (row != null)
			{
				if(row["ParamID"]!=null && row["ParamID"].ToString()!="")
				{
					model.ParamID=int.Parse(row["ParamID"].ToString());
				}
				if(row["ParamCode"]!=null)
				{
					model.ParamCode=row["ParamCode"].ToString();
				}
				if(row["PartsID"]!=null && row["PartsID"].ToString()!="")
				{
					model.PartsID=int.Parse(row["PartsID"].ToString());
				}
				if(row["ParamTypeID"]!=null && row["ParamTypeID"].ToString()!="")
				{
					model.ParamTypeID=int.Parse(row["ParamTypeID"].ToString());
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["OrderIndex"]!=null && row["OrderIndex"].ToString()!="")
				{
					model.OrderIndex=int.Parse(row["OrderIndex"].ToString());
				}
				if(row["Memo"]!=null)
				{
					model.Memo=row["Memo"].ToString();
				}
				if(row["boolValue"]!=null && row["boolValue"].ToString()!="")
				{
					model.boolValue=decimal.Parse(row["boolValue"].ToString());
				}
				if(row["PromptMin"]!=null && row["PromptMin"].ToString()!="")
				{
					model.PromptMin=decimal.Parse(row["PromptMin"].ToString());
				}
				if(row["PromptMax"]!=null && row["PromptMax"].ToString()!="")
				{
					model.PromptMax=decimal.Parse(row["PromptMax"].ToString());
				}
				if(row["PromptMsgFW"]!=null && row["PromptMsgFW"].ToString()!="")
				{
					model.PromptMsgFW=int.Parse(row["PromptMsgFW"].ToString());
				}
				if(row["WarningMin"]!=null && row["WarningMin"].ToString()!="")
				{
					model.WarningMin=decimal.Parse(row["WarningMin"].ToString());
				}
				if(row["WarningMax"]!=null && row["WarningMax"].ToString()!="")
				{
					model.WarningMax=decimal.Parse(row["WarningMax"].ToString());
				}
				if(row["WarningMsgFW"]!=null && row["WarningMsgFW"].ToString()!="")
				{
					model.WarningMsgFW=int.Parse(row["WarningMsgFW"].ToString());
				}
				if(row["ErrorMin"]!=null && row["ErrorMin"].ToString()!="")
				{
					model.ErrorMin=decimal.Parse(row["ErrorMin"].ToString());
				}
				if(row["ErrorMax"]!=null && row["ErrorMax"].ToString()!="")
				{
					model.ErrorMax=decimal.Parse(row["ErrorMax"].ToString());
				}
				if(row["ErrorMsgFW"]!=null && row["ErrorMsgFW"].ToString()!="")
				{
					model.ErrorMsgFW=int.Parse(row["ErrorMsgFW"].ToString());
				}
				if(row["MacParamID"]!=null && row["MacParamID"].ToString()!="")
				{
					model.MacParamID=int.Parse(row["MacParamID"].ToString());
				}
				if(row["InfoLevel"]!=null && row["InfoLevel"].ToString()!="")
				{
					model.InfoLevel=int.Parse(row["InfoLevel"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ParamID,ParamCode,PartsID,ParamTypeID,IsUse,OrderIndex,Memo,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,InfoLevel ");
			strSql.Append(" FROM HY_BMS_Param ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ParamID,ParamCode,PartsID,ParamTypeID,IsUse,OrderIndex,Memo,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,InfoLevel ");
			strSql.Append(" FROM HY_BMS_Param ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM HY_BMS_Param ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ParamID desc");
			}
			strSql.Append(")AS Row, T.*  from HY_BMS_Param T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "HY_BMS_Param";
			parameters[1].Value = "ParamID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

