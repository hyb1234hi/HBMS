﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_ParamDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/4/8 23:23:24   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_ParamDesc
	/// </summary>
	public partial class View_ParamDesc
	{
		public View_ParamDesc()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_ParamDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_ParamDesc(");
			strSql.Append("ParamID,ParamCode,ParamName,SPRAS,IsUse,OrderIndex,Memo,ParamTypeID,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,PartsID,PartsName,SystemID,SystemName,InfoLevel)");
			strSql.Append(" values (");
			strSql.Append("@ParamID,@ParamCode,@ParamName,@SPRAS,@IsUse,@OrderIndex,@Memo,@ParamTypeID,@boolValue,@PromptMin,@PromptMax,@PromptMsgFW,@WarningMin,@WarningMax,@WarningMsgFW,@ErrorMin,@ErrorMax,@ErrorMsgFW,@MacParamID,@PartsID,@PartsName,@SystemID,@SystemName,@InfoLevel)");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamID", SqlDbType.Int,4),
					new SqlParameter("@ParamCode", SqlDbType.VarChar,50),
					new SqlParameter("@ParamName", SqlDbType.NChar,10),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@Memo", SqlDbType.VarChar,500),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMsgFW", SqlDbType.Int,4),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMsgFW", SqlDbType.Int,4),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMsgFW", SqlDbType.Int,4),
					new SqlParameter("@MacParamID", SqlDbType.Int,4),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200),
					new SqlParameter("@InfoLevel", SqlDbType.Int,4)};
			parameters[0].Value = model.ParamID;
			parameters[1].Value = model.ParamCode;
			parameters[2].Value = model.ParamName;
			parameters[3].Value = model.SPRAS;
			parameters[4].Value = model.IsUse;
			parameters[5].Value = model.OrderIndex;
			parameters[6].Value = model.Memo;
			parameters[7].Value = model.ParamTypeID;
			parameters[8].Value = model.boolValue;
			parameters[9].Value = model.PromptMin;
			parameters[10].Value = model.PromptMax;
			parameters[11].Value = model.PromptMsgFW;
			parameters[12].Value = model.WarningMin;
			parameters[13].Value = model.WarningMax;
			parameters[14].Value = model.WarningMsgFW;
			parameters[15].Value = model.ErrorMin;
			parameters[16].Value = model.ErrorMax;
			parameters[17].Value = model.ErrorMsgFW;
			parameters[18].Value = model.MacParamID;
			parameters[19].Value = model.PartsID;
			parameters[20].Value = model.PartsName;
			parameters[21].Value = model.SystemID;
			parameters[22].Value = model.SystemName;
			parameters[23].Value = model.InfoLevel;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_ParamDesc model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_ParamDesc set ");
			strSql.Append("ParamID=@ParamID,");
			strSql.Append("ParamCode=@ParamCode,");
			strSql.Append("ParamName=@ParamName,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("OrderIndex=@OrderIndex,");
			strSql.Append("Memo=@Memo,");
			strSql.Append("ParamTypeID=@ParamTypeID,");
			strSql.Append("boolValue=@boolValue,");
			strSql.Append("PromptMin=@PromptMin,");
			strSql.Append("PromptMax=@PromptMax,");
			strSql.Append("PromptMsgFW=@PromptMsgFW,");
			strSql.Append("WarningMin=@WarningMin,");
			strSql.Append("WarningMax=@WarningMax,");
			strSql.Append("WarningMsgFW=@WarningMsgFW,");
			strSql.Append("ErrorMin=@ErrorMin,");
			strSql.Append("ErrorMax=@ErrorMax,");
			strSql.Append("ErrorMsgFW=@ErrorMsgFW,");
			strSql.Append("MacParamID=@MacParamID,");
			strSql.Append("PartsID=@PartsID,");
			strSql.Append("PartsName=@PartsName,");
			strSql.Append("SystemID=@SystemID,");
			strSql.Append("SystemName=@SystemName,");
			strSql.Append("InfoLevel=@InfoLevel");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@ParamID", SqlDbType.Int,4),
					new SqlParameter("@ParamCode", SqlDbType.VarChar,50),
					new SqlParameter("@ParamName", SqlDbType.NChar,10),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@OrderIndex", SqlDbType.Int,4),
					new SqlParameter("@Memo", SqlDbType.VarChar,500),
					new SqlParameter("@ParamTypeID", SqlDbType.Int,4),
					new SqlParameter("@boolValue", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMin", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMax", SqlDbType.Decimal,9),
					new SqlParameter("@PromptMsgFW", SqlDbType.Int,4),
					new SqlParameter("@WarningMin", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMax", SqlDbType.Decimal,9),
					new SqlParameter("@WarningMsgFW", SqlDbType.Int,4),
					new SqlParameter("@ErrorMin", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMax", SqlDbType.Decimal,9),
					new SqlParameter("@ErrorMsgFW", SqlDbType.Int,4),
					new SqlParameter("@MacParamID", SqlDbType.Int,4),
					new SqlParameter("@PartsID", SqlDbType.Int,4),
					new SqlParameter("@PartsName", SqlDbType.VarChar,200),
					new SqlParameter("@SystemID", SqlDbType.Int,4),
					new SqlParameter("@SystemName", SqlDbType.VarChar,200),
					new SqlParameter("@InfoLevel", SqlDbType.Int,4)};
			parameters[0].Value = model.ParamID;
			parameters[1].Value = model.ParamCode;
			parameters[2].Value = model.ParamName;
			parameters[3].Value = model.SPRAS;
			parameters[4].Value = model.IsUse;
			parameters[5].Value = model.OrderIndex;
			parameters[6].Value = model.Memo;
			parameters[7].Value = model.ParamTypeID;
			parameters[8].Value = model.boolValue;
			parameters[9].Value = model.PromptMin;
			parameters[10].Value = model.PromptMax;
			parameters[11].Value = model.PromptMsgFW;
			parameters[12].Value = model.WarningMin;
			parameters[13].Value = model.WarningMax;
			parameters[14].Value = model.WarningMsgFW;
			parameters[15].Value = model.ErrorMin;
			parameters[16].Value = model.ErrorMax;
			parameters[17].Value = model.ErrorMsgFW;
			parameters[18].Value = model.MacParamID;
			parameters[19].Value = model.PartsID;
			parameters[20].Value = model.PartsName;
			parameters[21].Value = model.SystemID;
			parameters[22].Value = model.SystemName;
			parameters[23].Value = model.InfoLevel;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_ParamDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_ParamDesc GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ParamID,ParamCode,ParamName,SPRAS,IsUse,OrderIndex,Memo,ParamTypeID,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,PartsID,PartsName,SystemID,SystemName,InfoLevel from View_ParamDesc ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_ParamDesc model=new HBMS.Model.View_ParamDesc();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_ParamDesc DataRowToModel(DataRow row)
		{
			HBMS.Model.View_ParamDesc model=new HBMS.Model.View_ParamDesc();
			if (row != null)
			{
				if(row["ParamID"]!=null && row["ParamID"].ToString()!="")
				{
					model.ParamID=int.Parse(row["ParamID"].ToString());
				}
				if(row["ParamCode"]!=null)
				{
					model.ParamCode=row["ParamCode"].ToString();
				}
				if(row["ParamName"]!=null)
				{
					model.ParamName=row["ParamName"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["OrderIndex"]!=null && row["OrderIndex"].ToString()!="")
				{
					model.OrderIndex=int.Parse(row["OrderIndex"].ToString());
				}
				if(row["Memo"]!=null)
				{
					model.Memo=row["Memo"].ToString();
				}
				if(row["ParamTypeID"]!=null && row["ParamTypeID"].ToString()!="")
				{
					model.ParamTypeID=int.Parse(row["ParamTypeID"].ToString());
				}
				if(row["boolValue"]!=null && row["boolValue"].ToString()!="")
				{
					model.boolValue=decimal.Parse(row["boolValue"].ToString());
				}
				if(row["PromptMin"]!=null && row["PromptMin"].ToString()!="")
				{
					model.PromptMin=decimal.Parse(row["PromptMin"].ToString());
				}
				if(row["PromptMax"]!=null && row["PromptMax"].ToString()!="")
				{
					model.PromptMax=decimal.Parse(row["PromptMax"].ToString());
				}
				if(row["PromptMsgFW"]!=null && row["PromptMsgFW"].ToString()!="")
				{
					model.PromptMsgFW=int.Parse(row["PromptMsgFW"].ToString());
				}
				if(row["WarningMin"]!=null && row["WarningMin"].ToString()!="")
				{
					model.WarningMin=decimal.Parse(row["WarningMin"].ToString());
				}
				if(row["WarningMax"]!=null && row["WarningMax"].ToString()!="")
				{
					model.WarningMax=decimal.Parse(row["WarningMax"].ToString());
				}
				if(row["WarningMsgFW"]!=null && row["WarningMsgFW"].ToString()!="")
				{
					model.WarningMsgFW=int.Parse(row["WarningMsgFW"].ToString());
				}
				if(row["ErrorMin"]!=null && row["ErrorMin"].ToString()!="")
				{
					model.ErrorMin=decimal.Parse(row["ErrorMin"].ToString());
				}
				if(row["ErrorMax"]!=null && row["ErrorMax"].ToString()!="")
				{
					model.ErrorMax=decimal.Parse(row["ErrorMax"].ToString());
				}
				if(row["ErrorMsgFW"]!=null && row["ErrorMsgFW"].ToString()!="")
				{
					model.ErrorMsgFW=int.Parse(row["ErrorMsgFW"].ToString());
				}
				if(row["MacParamID"]!=null && row["MacParamID"].ToString()!="")
				{
					model.MacParamID=int.Parse(row["MacParamID"].ToString());
				}
				if(row["PartsID"]!=null && row["PartsID"].ToString()!="")
				{
					model.PartsID=int.Parse(row["PartsID"].ToString());
				}
				if(row["PartsName"]!=null)
				{
					model.PartsName=row["PartsName"].ToString();
				}
				if(row["SystemID"]!=null && row["SystemID"].ToString()!="")
				{
					model.SystemID=int.Parse(row["SystemID"].ToString());
				}
				if(row["SystemName"]!=null)
				{
					model.SystemName=row["SystemName"].ToString();
				}
				if(row["InfoLevel"]!=null && row["InfoLevel"].ToString()!="")
				{
					model.InfoLevel=int.Parse(row["InfoLevel"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ParamID,ParamCode,ParamName,SPRAS,IsUse,OrderIndex,Memo,ParamTypeID,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,PartsID,PartsName,SystemID,SystemName,InfoLevel ");
			strSql.Append(" FROM View_ParamDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ParamID,ParamCode,ParamName,SPRAS,IsUse,OrderIndex,Memo,ParamTypeID,boolValue,PromptMin,PromptMax,PromptMsgFW,WarningMin,WarningMax,WarningMsgFW,ErrorMin,ErrorMax,ErrorMsgFW,MacParamID,PartsID,PartsName,SystemID,SystemName,InfoLevel ");
			strSql.Append(" FROM View_ParamDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_ParamDesc ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ParamID desc");
			}
			strSql.Append(")AS Row, T.*  from View_ParamDesc T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_ParamDesc";
			parameters[1].Value = "ParamID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

