﻿using System; 
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic; 
using System.Data;
using HBMS.DBUtility;
namespace HBMS.DAL  
{
	 	//HY_BMS_EquipmentParamValue
		public partial class HY_BMS_EquipmentParamValue
	{
   		     
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from HY_BMS_EquipmentParamValue");
			strSql.Append(" where ");
			                                       strSql.Append(" ID = @ID  ");
                            			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(HBMS.Model.HY_BMS_EquipmentParamValue model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into HY_BMS_EquipmentParamValue(");			
            strSql.Append("EquipmentID,paramID,SubDatetime,ParamValue,State");
			strSql.Append(") values (");
            strSql.Append("@EquipmentID,@paramID,@SubDatetime,@ParamValue,@State");            
            strSql.Append(") ");            
            strSql.Append(";select @@IDENTITY");		
			SqlParameter[] parameters = {
			            new SqlParameter("@EquipmentID", SqlDbType.Int,4) ,            
                        new SqlParameter("@paramID", SqlDbType.VarChar,10) ,            
                        new SqlParameter("@SubDatetime", SqlDbType.DateTime) ,            
                        new SqlParameter("@ParamValue", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@State", SqlDbType.Int,4)             
              
            };
			            
            parameters[0].Value = model.EquipmentID;                        
            parameters[1].Value = model.paramID;                        
            parameters[2].Value = model.SubDatetime;                        
            parameters[3].Value = model.ParamValue;                        
            parameters[4].Value = model.State;                        
			   
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt32(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_EquipmentParamValue model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update HY_BMS_EquipmentParamValue set ");
			                                                
            strSql.Append(" EquipmentID = @EquipmentID , ");                                    
            strSql.Append(" paramID = @paramID , ");                                    
            strSql.Append(" SubDatetime = @SubDatetime , ");                                    
            strSql.Append(" ParamValue = @ParamValue , ");                                    
            strSql.Append(" State = @State  ");            			
			strSql.Append(" where ID=@ID ");
						
SqlParameter[] parameters = {
			            new SqlParameter("@ID", SqlDbType.Int,4) ,            
                        new SqlParameter("@EquipmentID", SqlDbType.Int,4) ,            
                        new SqlParameter("@paramID", SqlDbType.VarChar,10) ,            
                        new SqlParameter("@SubDatetime", SqlDbType.DateTime) ,            
                        new SqlParameter("@ParamValue", SqlDbType.Decimal,9) ,            
                        new SqlParameter("@State", SqlDbType.Int,4)             
              
            };
						            
            parameters[0].Value = model.ID;                        
            parameters[1].Value = model.EquipmentID;                        
            parameters[2].Value = model.paramID;                        
            parameters[3].Value = model.SubDatetime;                        
            parameters[4].Value = model.ParamValue;                        
            parameters[5].Value = model.State;                        
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_EquipmentParamValue ");
			strSql.Append(" where ID=@ID");
						SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;


			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
				/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from HY_BMS_EquipmentParamValue ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
				
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_EquipmentParamValue GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID, EquipmentID, paramID, SubDatetime, ParamValue, State  ");			
			strSql.Append("  from HY_BMS_EquipmentParamValue ");
			strSql.Append(" where ID=@ID");
						SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			
			HBMS.Model.HY_BMS_EquipmentParamValue model=new HBMS.Model.HY_BMS_EquipmentParamValue();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["EquipmentID"].ToString()!="")
				{
					model.EquipmentID=int.Parse(ds.Tables[0].Rows[0]["EquipmentID"].ToString());
				}
																																				model.paramID= ds.Tables[0].Rows[0]["paramID"].ToString();
																												if(ds.Tables[0].Rows[0]["SubDatetime"].ToString()!="")
				{
					model.SubDatetime=DateTime.Parse(ds.Tables[0].Rows[0]["SubDatetime"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["ParamValue"].ToString()!="")
				{
					model.ParamValue=decimal.Parse(ds.Tables[0].Rows[0]["ParamValue"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["State"].ToString()!="")
				{
					model.State=int.Parse(ds.Tables[0].Rows[0]["State"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM HY_BMS_EquipmentParamValue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM HY_BMS_EquipmentParamValue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

   
	}
}

