﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_Users
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/12 23:30:38   N/A    初版
// 
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using HBMS.DBUtility;//Please add references
namespace HBMS.DAL
{
	/// <summary>
	/// 数据访问类:View_Users
	/// </summary>
	public partial class View_Users
	{
		public View_Users()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.View_Users model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into View_Users(");
			strSql.Append("UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID,AirPortName,AirPortAddress,SPRAS,RoleID,RoleName)");
			strSql.Append(" values (");
			strSql.Append("@UserCode,@UserName,@CName,@EName,@PassWord,@PhoneNo,@Email,@Sex,@Captcha,@Captchadatetime,@defaultSpras,@IsUse,@DefaultAirPortID,@AirPortName,@AirPortAddress,@SPRAS,@RoleID,@RoleName)");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@CName", SqlDbType.NVarChar,100),
					new SqlParameter("@EName", SqlDbType.NVarChar,100),
					new SqlParameter("@PassWord", SqlDbType.VarChar,50),
					new SqlParameter("@PhoneNo", SqlDbType.VarChar,50),
					new SqlParameter("@Email", SqlDbType.VarChar,50),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Captcha", SqlDbType.VarChar,20),
					new SqlParameter("@Captchadatetime", SqlDbType.DateTime),
					new SqlParameter("@defaultSpras", SqlDbType.NChar,10),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@DefaultAirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@AirPortAddress", SqlDbType.NVarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@RoleID", SqlDbType.Int,4),
					new SqlParameter("@RoleName", SqlDbType.VarChar,100)};
			parameters[0].Value = model.UserCode;
			parameters[1].Value = model.UserName;
			parameters[2].Value = model.CName;
			parameters[3].Value = model.EName;
			parameters[4].Value = model.PassWord;
			parameters[5].Value = model.PhoneNo;
			parameters[6].Value = model.Email;
			parameters[7].Value = model.Sex;
			parameters[8].Value = model.Captcha;
			parameters[9].Value = model.Captchadatetime;
			parameters[10].Value = model.defaultSpras;
			parameters[11].Value = model.IsUse;
			parameters[12].Value = model.DefaultAirPortID;
			parameters[13].Value = model.AirPortName;
			parameters[14].Value = model.AirPortAddress;
			parameters[15].Value = model.SPRAS;
			parameters[16].Value = model.RoleID;
			parameters[17].Value = model.RoleName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.View_Users model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update View_Users set ");
			strSql.Append("UserCode=@UserCode,");
			strSql.Append("UserName=@UserName,");
			strSql.Append("CName=@CName,");
			strSql.Append("EName=@EName,");
			strSql.Append("PassWord=@PassWord,");
			strSql.Append("PhoneNo=@PhoneNo,");
			strSql.Append("Email=@Email,");
			strSql.Append("Sex=@Sex,");
			strSql.Append("Captcha=@Captcha,");
			strSql.Append("Captchadatetime=@Captchadatetime,");
			strSql.Append("defaultSpras=@defaultSpras,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("DefaultAirPortID=@DefaultAirPortID,");
			strSql.Append("AirPortName=@AirPortName,");
			strSql.Append("AirPortAddress=@AirPortAddress,");
			strSql.Append("SPRAS=@SPRAS,");
			strSql.Append("RoleID=@RoleID,");
			strSql.Append("RoleName=@RoleName");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
					new SqlParameter("@UserCode", SqlDbType.VarChar,6),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@CName", SqlDbType.NVarChar,100),
					new SqlParameter("@EName", SqlDbType.NVarChar,100),
					new SqlParameter("@PassWord", SqlDbType.VarChar,50),
					new SqlParameter("@PhoneNo", SqlDbType.VarChar,50),
					new SqlParameter("@Email", SqlDbType.VarChar,50),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Captcha", SqlDbType.VarChar,20),
					new SqlParameter("@Captchadatetime", SqlDbType.DateTime),
					new SqlParameter("@defaultSpras", SqlDbType.NChar,10),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@DefaultAirPortID", SqlDbType.VarChar,10),
					new SqlParameter("@AirPortName", SqlDbType.NVarChar,200),
					new SqlParameter("@AirPortAddress", SqlDbType.NVarChar,500),
					new SqlParameter("@SPRAS", SqlDbType.Int,4),
					new SqlParameter("@RoleID", SqlDbType.Int,4),
					new SqlParameter("@RoleName", SqlDbType.VarChar,100)};
			parameters[0].Value = model.UserCode;
			parameters[1].Value = model.UserName;
			parameters[2].Value = model.CName;
			parameters[3].Value = model.EName;
			parameters[4].Value = model.PassWord;
			parameters[5].Value = model.PhoneNo;
			parameters[6].Value = model.Email;
			parameters[7].Value = model.Sex;
			parameters[8].Value = model.Captcha;
			parameters[9].Value = model.Captchadatetime;
			parameters[10].Value = model.defaultSpras;
			parameters[11].Value = model.IsUse;
			parameters[12].Value = model.DefaultAirPortID;
			parameters[13].Value = model.AirPortName;
			parameters[14].Value = model.AirPortAddress;
			parameters[15].Value = model.SPRAS;
			parameters[16].Value = model.RoleID;
			parameters[17].Value = model.RoleName;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from View_Users ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_Users GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID,AirPortName,AirPortAddress,SPRAS,RoleID,RoleName from View_Users ");
			strSql.Append(" where ");
			SqlParameter[] parameters = {
			};

			HBMS.Model.View_Users model=new HBMS.Model.View_Users();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.View_Users DataRowToModel(DataRow row)
		{
			HBMS.Model.View_Users model=new HBMS.Model.View_Users();
			if (row != null)
			{
				if(row["UserCode"]!=null)
				{
					model.UserCode=row["UserCode"].ToString();
				}
				if(row["UserName"]!=null)
				{
					model.UserName=row["UserName"].ToString();
				}
				if(row["CName"]!=null)
				{
					model.CName=row["CName"].ToString();
				}
				if(row["EName"]!=null)
				{
					model.EName=row["EName"].ToString();
				}
				if(row["PassWord"]!=null)
				{
					model.PassWord=row["PassWord"].ToString();
				}
				if(row["PhoneNo"]!=null)
				{
					model.PhoneNo=row["PhoneNo"].ToString();
				}
				if(row["Email"]!=null)
				{
					model.Email=row["Email"].ToString();
				}
				if(row["Sex"]!=null && row["Sex"].ToString()!="")
				{
					model.Sex=int.Parse(row["Sex"].ToString());
				}
				if(row["Captcha"]!=null)
				{
					model.Captcha=row["Captcha"].ToString();
				}
				if(row["Captchadatetime"]!=null && row["Captchadatetime"].ToString()!="")
				{
					model.Captchadatetime=DateTime.Parse(row["Captchadatetime"].ToString());
				}
				if(row["defaultSpras"]!=null)
				{
					model.defaultSpras=row["defaultSpras"].ToString();
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["DefaultAirPortID"]!=null)
				{
					model.DefaultAirPortID=row["DefaultAirPortID"].ToString();
				}
				if(row["AirPortName"]!=null)
				{
					model.AirPortName=row["AirPortName"].ToString();
				}
				if(row["AirPortAddress"]!=null)
				{
					model.AirPortAddress=row["AirPortAddress"].ToString();
				}
				if(row["SPRAS"]!=null && row["SPRAS"].ToString()!="")
				{
					model.SPRAS=int.Parse(row["SPRAS"].ToString());
				}
				if(row["RoleID"]!=null && row["RoleID"].ToString()!="")
				{
					model.RoleID=int.Parse(row["RoleID"].ToString());
				}
				if(row["RoleName"]!=null)
				{
					model.RoleName=row["RoleName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID,AirPortName,AirPortAddress,SPRAS,RoleID,RoleName ");
			strSql.Append(" FROM View_Users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" UserCode,UserName,CName,EName,PassWord,PhoneNo,Email,Sex,Captcha,Captchadatetime,defaultSpras,IsUse,DefaultAirPortID,AirPortName,AirPortAddress,SPRAS,RoleID,RoleName ");
			strSql.Append(" FROM View_Users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM View_Users ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from View_Users T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "View_Users";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

