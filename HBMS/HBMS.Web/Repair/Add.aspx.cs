﻿// 
// 创建人：宋欣
// 创建时间：2015-03-07
// 功能：新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;

namespace HBMS.Web.Repair
{
    public partial class Add : BasePage
    {
        private string _repairCode;

        protected string RepairCode
        {
            get
            {
                if (string.IsNullOrEmpty(_repairCode))
                {
                    BLL.HY_BMS_Repair bll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_Repair>();
                    string beforeid = string.Format("CNR{0}", DateTime.Now.ToString("yyyyMMdd"));
                    DataSet ds = bll.GetList(string.Format(" ID LIKE '{0}%'  Order By ID Desc ", beforeid));
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        this._repairCode = beforeid + "0001";
                    }
                    else
                    {
                        string id = ds.Tables[0].Rows[0]["ID"].ToString();
                        int endID = Convert.ToInt32(id.Substring(beforeid.Length)) + 1;
                        this._repairCode = beforeid + (endID.ToString().PadLeft(4, '0'));
                    }
                }
                return _repairCode;
            }
                 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        private void GetRepairCode()
        {
        }


        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navRepair + " </a> >> " +
                                Resources.Nav.navRepairAdd);
        }
    }
}