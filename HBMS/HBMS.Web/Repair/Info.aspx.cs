﻿// 
// 创建人：宋欣
// 创建时间：2015-03-08
// 功能：维修明细
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using HBMS.Model;

namespace HBMS.Web.Repair
{
    public partial class Info : BasePage
    {
        /// <summary>
        ///     ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        ///     维修实体
        /// </summary>
        public Model.View_Repair Model { get; set; }

        protected string DateFormat(object obj)
        {
            return obj.yyyy_MM_dd();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                this.ID = Request.QueryString["id"];
                this.BindData();
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }


        private void BindData()
        {
            BLL.View_Repair bll = Factory.BLLFactory.CreateInstance<BLL.View_Repair>();
            DataSet ds =
                bll.GetList(string.Format(" ID='{0}' AND SPRAS={1} ", this.ID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_Repair> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
                BLL.HY_BMS_RepariFiles filesBll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_RepariFiles>();
                Model.Files=new List<HY_BMS_RepariFiles>();
                Model.Files = filesBll.GetModelList(string.Format(" RepairID='{0}'", Model.ID));
                if (Model.Files.Any())
                {
                    this.repeaterFile.DataSource = Model.Files;
                    this.repeaterFile.DataBind();
                }
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navRepair + " </a> >> " +
                                Resources.Nav.navRepairInfo);
        }
    }
}