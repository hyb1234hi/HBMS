﻿// 
// 创建人：宋欣
// 创建时间：2015-03-07
// 功能：维修新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using HBMS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Repair.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_Repair model = new Model.HY_BMS_Repair();
            TryUpdateModel(model);
            model.SubDate = DateTime.Now;
            model.UserCode = this.GoalUserInfo.Users.UserCode;
            BLL.HY_BMS_Repair bll = BLLFactory.CreateInstance<BLL.HY_BMS_Repair>();
            ////判断是否有同名的存在
            DataSet ds = bll.GetList(string.Format(" ID='{0}'", model.ID));
            if (ds.Tables[0].Rows.Count == 0)
            {
                bll.Add(model);
                if (!string.IsNullOrEmpty(Context.Request.Params["rfiles"]))
                {
                    JArray jArr = JArray.Parse(Context.Request.Form["rfiles"]);
                    Model.HY_BMS_RepariFiles mfiles = new HY_BMS_RepariFiles();
                    mfiles.RepairID = model.ID;
                    foreach (var item in jArr)
                    {
                        mfiles.FileName = item["name"].ToString();
                        mfiles.FilePath = item["path"].ToString();
                    }
                    BLLFactory.CreateInstance<BLL.HY_BMS_RepariFiles>().Add(mfiles);
                }
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorCodeExist);
        }
    }
}