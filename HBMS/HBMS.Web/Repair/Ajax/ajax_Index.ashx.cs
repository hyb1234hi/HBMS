﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Repair.Ajax
{
    /// <summary>
    /// ajax_Index 的摘要说明
    /// </summary>
    public class ajax_Index : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.Query();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string Query()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS={0} ", this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["id"]))
            {
                ////编号
                where.AppendFormat(" AND ID LIKE '%{0}%' ", Context.Request.Params["id"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["airport"]))
            {
                ////机场
                where.AppendFormat(" AND AirPortID ='{0}' ", Context.Request.Params["airport"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["equipment"]))
            {
                ////涉笔
                where.AppendFormat(" AND EquipmentID ='{0}' ", Context.Request.Params["equipment"]);
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["system"]))
            {
                ////系统
                where.AppendFormat(" AND SystemID ='{0}' ", Context.Request.Params["system"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["parts"]))
            {
                ////部件
                where.AppendFormat(" AND PartsID ='{0}' ", Context.Request.Params["parts"]);
            }
            BLL.View_Repair bll = Factory.BLLFactory.CreateInstance<BLL.View_Repair>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " ID ", Pager.StartIndex, Pager.EndIndex);
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_Repair> list = bll.DataTableToList(ds.Tables[0]);
                string idWhere = list.Aggregate("", (result, item) => result += ",'" + item.ID + "'");
                if (!string.IsNullOrEmpty(idWhere))
                {
                    idWhere = idWhere.Substring(1);
                }
                BLL.HY_BMS_RepariFiles bllFiles = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_RepariFiles>();
                DataSet ds2 = bllFiles.GetList(" RepairID IN (" + idWhere + ")");
                List<Model.HY_BMS_RepariFiles> listFiles = bllFiles.DataTableToList(ds2.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                    {
                        data = from l in list
                               join f in listFiles on l.ID equals  f.RepairID into  lfjoin
                               from lf in lfjoin.DefaultIfEmpty()
                               select new
                               {
                                   l.ID,
                                   l.AirPortID,
                                   l.AirPortName,
                                   l.EquipmentID,
                                   l.EquipmentName,
                                   l.SystemID,
                                   l.SystemName,
                                   l.PartsID,
                                   l.PartsName,
                                   RepairDate=l.RepairDate.yyyy_MM_dd(),
                                   l.RepairUse,
                                   l.CheckUser,
                                   l.UserCode,
                                   l.CName,
                                   l.EName,
                                   SubDate=l.SubDate.yyyy_MM_dd(),
                                   l.SPRAS,
                                   FilePath=lf!=null?lf.FilePath:"",
                                   FileName = lf != null ? lf.FileName : ""
                               },
                        totalCount = Pager.TotalCount,
                        totalPages = Pager.TotalPage,
                        visiblePages = Pager.VisiblePages,
                        startPage = Pager.PageIndex
                    }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}