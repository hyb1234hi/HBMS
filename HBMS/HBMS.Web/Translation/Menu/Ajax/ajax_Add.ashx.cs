﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HBMS.Common;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Translation.Menu.Ajax
{
    /// <summary>
    /// ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                WriteResult = this.Save();
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Save()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["data"]))
            {
                BLL.HY_BMS_MenuDesc bll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_MenuDesc>();
                JArray data = JArray.Parse(Context.Request.Params["data"]);
                List<Model.HY_BMS_MenuDesc> list = new List<Model.HY_BMS_MenuDesc>();
                Model.HY_BMS_MenuDesc desc = new Model.HY_BMS_MenuDesc();
                foreach (var item in data)
                {
                    if (Convert.ToInt32(item["MenuID"]) > 0 && !string.IsNullOrEmpty(item["MenuName"].ToString()))
                    {
                        desc = new Model.HY_BMS_MenuDesc();
                        desc.MenuID = item["MenuID"].ToString();
                        desc.spras = Convert.ToInt32(item["SPRAS"]);
                        desc.MenuName = item["MenuName"].ToString();
                        list.Add(desc);
                    }
                }
                return bll.SaveLanguage(list) ? JsonSuccess() : JsonError();
            }
            return JsonError(Resources.Msg.errorParam);
        }
    }
}