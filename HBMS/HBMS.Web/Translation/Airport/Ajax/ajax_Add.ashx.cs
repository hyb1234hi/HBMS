﻿// 
// 创建人：宋欣
// 创建时间：2015-03-07
// 功能：保存多语言
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Web;
using HBMS.Common;
using HBMS.Model;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Translation.Airport.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                WriteResult = this.Save();
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Save()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["data"]))
            {
                BLL.HY_BMS_AirPortDesc bll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_AirPortDesc>();
                JArray data = JArray.Parse(Context.Request.Params["data"]);
                List<Model.HY_BMS_AirPortDesc> list = new List<HY_BMS_AirPortDesc>();
                Model.HY_BMS_AirPortDesc desc = new HY_BMS_AirPortDesc();
                foreach (var item in data)
                {
                    if (Convert.ToInt32(item["AirPortID"]) > 0 && !string.IsNullOrEmpty(item["AirPortName"].ToString()))
                    {
                        desc = new HY_BMS_AirPortDesc();
                        desc.AirPortID = Convert.ToInt32(item["AirPortID"]);
                        desc.SPRAS = Convert.ToInt32(item["SPRAS"]);
                        desc.AirPortName = item["AirPortName"].ToString();
                        desc.AirPortAddress = item["AirPortAddress"].ToString();
                        list.Add(desc); 
                    }
                }
                return bll.SaveLanguage(list) ? JsonSuccess() : JsonError();
            }
            return JsonError(Resources.Msg.errorParam);
        }
    }
}