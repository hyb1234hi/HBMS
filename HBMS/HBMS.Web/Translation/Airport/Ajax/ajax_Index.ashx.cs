﻿// 
// 创建人：宋欣
// 创建时间：2015-03-07
// 功能：获取多语言机场
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace HBMS.Web.Translation.Airport.Ajax
{
    /// <summary>
    ///     ajax_Index 的摘要说明
    /// </summary>
    public class ajax_Index : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.Query();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string Query()
        {
            ////表格html
            StringBuilder tb = new StringBuilder();
            BLL.HY_BMS_SPRAS blls = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_SPRAS>();
            ////系统语言数据
            DataSet sprasData = blls.GetList("");
            if (sprasData.Tables[0].Rows.Count > 0)
            {
                List<Model.HY_BMS_SPRAS> spras = blls.DataTableToList(sprasData.Tables[0]);
                BLL.HY_BMS_AirPortDesc bll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_AirPortDesc>();
                DataSet ds = bll.GetAllList();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<Model.HY_BMS_AirPortDesc> list = bll.DataTableToList(ds.Tables[0]);
                    if (list.Count > 0)
                    {
                        tb.Append("<table id=\"tbResult\" class=\"tb-data\">");
                        tb.Append("<thead>");
                        tb.Append("<tr>");
                        tb.AppendFormat("<th>{0}</th>", Resources.Lbl.lblCode);
                        tb.AppendFormat("<th>{0}</th>", Resources.Lbl.lblSpars);
                        tb.AppendFormat("<th>{0}</th>", Resources.Lbl.lblAirportName);
                        tb.AppendFormat("<th>{0}</th>", Resources.Lbl.lblAddress);
                        tb.Append("</tr>");
                        tb.Append("</thead>");
                        tb.Append("<tbody>");
                        var data = from r in list
                                   select r.AirPortID;
                        foreach (var i in data.Distinct())
                        {
                            foreach (var s in spras)
                            {
                                tb.Append("<tr>");
                                tb.AppendFormat("<td><input type=\"hidden\" value=\"{0}\"/>{0}</td>", i);
                                tb.AppendFormat("<td>{0}<input type=\"hidden\" value=\"{1}\"/></td>", s.LangName, s.ID);
                                var airport = from r in list
                                              where r.AirPortID == i && r.SPRAS == s.ID
                                              select new { r.AirPortName, r.AirPortAddress };
                                tb.AppendFormat(
                                    "<td><input class=\"wp100 input-text\" type=\"text\" value=\"{0}\"/></td>",
                                    airport.Any() ? airport.First().AirPortName : "");
                                tb.AppendFormat(
                                    "<td><input class=\"wp100 input-text\" type=\"text\" value=\"{0}\"/></td>",
                                    airport.Any() ? airport.First().AirPortAddress : "");
                                tb.Append("</tr>");
                            }
                        }
                        tb.Append("</tbody>");
                        tb.Append("</table>");
                    }
                }
            }
            return tb.ToString();
        }
    }
}