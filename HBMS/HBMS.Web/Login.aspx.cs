﻿// 
// 创建人：宋欣
// 创建时间：2015-03-06
// 功能：登录页面
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using HBMS.Common;

namespace HBMS.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ////进入登录页面前移出所有session
                Session.RemoveAll();
                BindLanguage();
            }
        }

        private void BindLanguage()
        {
            BLL.HY_BMS_SPRAS bll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_SPRAS>();
            DataSet ds = bll.GetList("IsUse=1");
            this.ddlLanguage.DataSource = ds;
            this.ddlLanguage.DataTextField = "LangName";
            this.ddlLanguage.DataValueField = "ID";
            this.ddlLanguage.DataBind();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtUserName.Text))
            {
                if (!string.IsNullOrEmpty(this.txtPwd.Text))
                {
                    ////用户名
                    string userName = this.txtUserName.Text.Trim();
                    ////密码
                    string pwd = MD5Encrypt.Encrypt(this.txtPwd.Text.Trim());
                    DataSet ds = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_Users>()
                                        .GetList(string.Format(" UserName='{0}' AND PassWord='{1}' AND IsUse=1 ",
                                                               userName, pwd));
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["UserCode"] = ds.Tables[0].Rows[0]["UserCode"].ToString();
                        Session["LanguageID"] = this.ddlLanguage.SelectedValue;
                        Response.Redirect("/Home/Index.aspx");
                    }
                    else
                    {
                        ////用户不存在或密码错误
                        MessageBox.Error(this, Resources.Msg.errorLoginUserNoFind);
                    }
                }
                else
                {
                    ////密码不能为空
                    MessageBox.Error(this, Resources.Msg.errorLoginPwdEmpty);
                }
            }
            else
            {
                ////用户名不能为空
                MessageBox.Error(this, Resources.Msg.errorLoginUserNameEmpty);
            }
        }
    }
}