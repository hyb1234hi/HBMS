﻿// 
// 创建人：宋欣
// 创建时间：2015-03-10
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.Map.Bai
{
    public partial class Params : BasePage
    {
        protected string AID = "";

        protected string EID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["aid"]) && !string.IsNullOrEmpty(Request.QueryString["eid"]))
            {
                this.AID = Request.QueryString["aid"];
                this.EID = Request.QueryString["eid"];
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navDisplayMap + " </a> >> " +
                                Resources.Nav.navEquipmentParams);
        }
    }
}