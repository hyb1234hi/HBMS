﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Map.Bai.Ajax
{
    /// <summary>
    /// ajax_History 的摘要说明
    /// </summary>
    public class ajax_History : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.Query();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string Query()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ", this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["airport"]))
            {
                ////机场名称
                where.AppendFormat(" AND AirPortID ='{0}' ",
                                   Context.Request.Params["airport"].FilterQueryCondition());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["equipment"]))
            {
                ////设备名称
                where.AppendFormat(" AND EquipmentID ='{0}' ",
                                   Context.Request.Params["equipment"].FilterQueryCondition());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["system"]))
            {
                ////系统名称
                where.AppendFormat(" AND SystemID ='{0}' ",
                                   Context.Request.Params["system"].FilterQueryCondition());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["parts"]))
            {
                ////部件
                where.AppendFormat(" AND PartsID ='{0}' ",
                                   Context.Request.Params["parts"].FilterQueryCondition());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["param"]))
            {
                ////参数名称
                where.AppendFormat(" AND ParamID ='{0}' ",
                                   Context.Request.Params["param"].FilterQueryCondition());
            }
            BLL.View_EquipmentParamValuesHistory bll = BLLFactory.CreateInstance<BLL.View_EquipmentParamValuesHistory>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " SubDatetime ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentParamValuesHistory> list = bll.DataTableToList(ds.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                    {
                        data = from l in list
                               select new
                               {
                                   l.ID,
                                   l.AirPortID,
                                   l.AirPortName,
                                   l.EquipmentID,
                                   l.EquipmentName,
                                   l.SystemID,
                                   l.SystemName,
                                   l.PartsID,
                                   l.PartsName,
                                   l.ParamID,
                                   l.ParamName,
                                   l.ParamValue,
                                   SubDatetime = l.SubDatetime.ToDateNoSFM(),
                                   l.ParamState,
                                   ParamStateDesc = l.ParamState.EquipmentState(),
                                   l.SPRAS,
                               },
                        totalCount = Pager.TotalCount,
                        totalPages = Pager.TotalPage,
                        visiblePages = Pager.VisiblePages,
                        startPage = Pager.PageIndex
                    }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}