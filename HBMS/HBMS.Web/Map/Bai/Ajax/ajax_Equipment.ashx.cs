﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Factory;
using Newtonsoft.Json;

namespace HBMS.Web.Map.Bai.Ajax
{
    /// <summary>
    /// ajax_Equipment 的摘要说明
    /// </summary>
    public class ajax_Equipment : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (!string.IsNullOrEmpty(context.Request.Params["airport"]))
            {
                WriteResult = this.QueryAccount();
            }
            else
            {
                WriteResult = "";
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryAccount()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} AND AirPortID={1} ", this.GoalUserInfo.Spras, Context.Request.Params["airport"]);

            BLL.View_EquipmentState bll = BLLFactory.CreateInstance<BLL.View_EquipmentState>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetList(where.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentState> list = bll.DataTableToList(ds.Tables[0]);
                if (list.Count > 0)
                {
                    ////查找机场设备。去重
                    var left = (from l in list
                                select new
                                    {
                                        l.AirPortID,
                                        l.AirPortCode,
                                        l.AirPortName,
                                        l.EquipmentID,
                                        l.EquipmentCode,
                                        l.EquipmentName,
                                        l.State,
                                        EquipmentStateDesc = l.State.EquipmentState(),
                                    }).Distinct();
                    return JsonConvert.SerializeObject(left);
                    //////查找设备参数状态
                    //var right = from i in list
                    //            select new
                    //                {
                    //                    i.EquipmentID,
                    //                    i.ParamID,
                    //                    i.ParamCode,
                    //                    i.ParamName,
                    //                    i.ParamValue,
                    //                };
                    ////返回查询结果json
                    //return JsonConvert.SerializeObject(from l in left
                    //                                   select new
                    //                                       {
                    //                                           l.AirPortID,
                    //                                           l.AirPortCode,
                    //                                           l.AirPortName,
                    //                                           l.EquipmentID,
                    //                                           l.EquipmentCode,
                    //                                           l.EquipmentName,
                    //                                           l.EquipmentState,

                    //                                           Params = from i in right
                    //                                                    where i.EquipmentID == l.EquipmentID
                    //                                                    select new
                    //                                                        {
                    //                                                            i.ParamID,
                    //                                                            i.ParamName,
                    //                                                            i.ParamCode,
                    //                                                            i.ParamValue,
                    //                                                        }
                    //                                       }
                    //  );
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}