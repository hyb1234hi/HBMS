﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using Newtonsoft.Json;

namespace HBMS.Web.Map.Bai.Ajax
{
    /// <summary>
    /// ajax_AlterError 的摘要说明
    /// </summary>
    public class ajax_AlterWarn : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            WriteResult = this.QueryWarn();
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryWarn()
        {

            BLL.View_EquipmentParamValues bllValues = BLLFactory.CreateInstance<BLL.View_EquipmentParamValues>();

            ////查询条件
            string strWhere = " ParamState =" + (int) AirePortStaetEnum.警示;
            DataSet ds2 = bllValues.GetList(strWhere);
            List<Model.View_EquipmentParamValues> list = bllValues.DataTableToList(ds2.Tables[0]);
            if (list.Count > 0)
            {
                ////返回查询结果json
                return JsonConvert.SerializeObject((from l in list
                                                    select new
                                                    {
                                                        l.AirPortID,
                                                        l.AirPortName,
                                                        l.Longitude,
                                                        l.Dimension,
                                                        l.ParamState,
                                                        l.ParamValue,
                                                        ParamStateDesc=l.ParamState.EquipmentState(),
                                                        l.ParamName
                                                    }).Distinct());
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }

}