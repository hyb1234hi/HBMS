﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Map.Bai.Ajax
{
    /// <summary>
    /// ajax_Params 的摘要说明
    /// </summary>
    public class ajax_Params : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.Query();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string Query()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS={0} ", this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["aid"]))
            {
                ////机场ID
                where.AppendFormat(" AND AirPortID='{0}'", Context.Request.Params["aid"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["eid"]))
            {
                ////设备ID
                where.AppendFormat(" AND EquipmentID='{0}' ", Context.Request.Params["eid"].Trim());
            }
            BLL.View_EquipmentParamValues bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentParamValues>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " ID ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentParamValues> list = bll.DataTableToList(ds.Tables[0]);

                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                    {
                        data = from l in list
                               select new 
                               {
                                   l.AirPortID,
                                   l.AirPortCode,
                                   l.AirPortName,
                                   l.EquipmentID,
                                   l.EquipmentCode,
                                   l.EquipmentName,
                                   l.PartsID,
                                   l.PartsName,
                                   l.ParamID,
                                   l.ParamCode,
                                   l.ParamName,
                                   l.ParamType,
                                   l.ParamTypeName,
                                   ParamValue=l.DisplayParamValue!=null? l.DisplayParamValue.ToString():"",
                                   l.DisplayParamValueDesc,
                                   l.Units,
                                   l.TagName,
                                   l.NormalValue,
                                   ConfigParamValue= GetConfigParamValue(l),
                                   l.ParamState,
                                   ParamStateDesc=l.ParamState.EquipmentState(),
                                   l.SubDatetime,
                                   SubDatetimeDesc=l.SubDatetime.ToDateHaveSF()
                               },
                        totalCount = Pager.TotalCount,
                        totalPages = Pager.TotalPage,
                        visiblePages = Pager.VisiblePages,
                        startPage = Pager.PageIndex
                    }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }

        /// <summary>
        /// 获取参数配置值
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string GetConfigParamValue(Model.View_EquipmentParamValues item)
        {
            ////todo 修改
            return "";
            //if (item.TagType == (int) ParamTypeEnum.布尔)
            //{
            //    return Convert.ToBoolean(item.boolValue).ToString();
            //}
            //else
            //{
            //    switch (item.ParamState)
            //    {
            //        case (int)AirePortStaetEnum.提醒:
            //            return item.PromptMin + "-" + item.PromptMax;
            //        case (int)AirePortStaetEnum.警示:
            //            return item.WarningMin + "-" + item.WarningMax;
            //        case (int)AirePortStaetEnum.告急:
            //            return item.ErrorMax + "-" + item.ErrorMax;
            //        default:
            //            return "";
            //    }
            //}
        }
    }
}