﻿// 
// 创建人：宋欣
// 创建时间：2015-02-12
// 功能：查询地图数据
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Map.Bai.Ajax
{
    /// <summary>
    ///     ajax_Map 的摘要说明
    /// </summary>
    public class ajax_Map : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            WriteResult = this.QueryAccount();
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryAccount()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ", this.GoalUserInfo.Spras);

            BLL.View_EquipmentState bll = BLLFactory.CreateInstance<BLL.View_EquipmentState>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetList(where.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentState> list = bll.DataTableToList(ds.Tables[0]);
                BLL.View_EquipmentParamValues bllValues = BLLFactory.CreateInstance<BLL.View_EquipmentParamValues>();
                //// 设备查询条件
                string equipmentIDWhere = list.Aggregate("", (result, item) => result+="," + item.EquipmentID);
                ////查询条件
                string strWhere = " ParamState =" + (int)AirePortStaetEnum.告急;
                if (!string.IsNullOrEmpty(equipmentIDWhere))
                {
                    strWhere += " AND EquipmentID IN (" + equipmentIDWhere.Substring(1) + ")";
                }
                DataSet ds2 = bllValues.GetList(strWhere);
                List<Model.View_EquipmentParamValues> valueses = bllValues.DataTableToList(ds2.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JsonConvert.SerializeObject((from l in list
                                                        select new
                                                            {
                                                                l.AirPortID,
                                                                l.AirPortName,
                                                                l.Longitude,
                                                                l.Dimension,
                                                                EquipmentState = l.State,
                                                                GJ= from v in valueses
                                                                    where v.EquipmentID==l.EquipmentID
                                                                    select new
                                                                        {
                                                                            v.ParamName,
                                                                            v.ParamValue
                                                                        }

                                                            }).Distinct());
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}