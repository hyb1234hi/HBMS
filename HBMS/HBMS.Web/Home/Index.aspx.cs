﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：首页
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.Home
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation = Resources.Nav.navHome;
        }
    }
}