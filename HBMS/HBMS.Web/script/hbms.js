﻿//step01 定义JQuery的作用域
(function ($) {
    ////datatable 查询
    //step03-a 插件的默认值属性
    var defaults = {
        info: false,    ////禁用默认的信息
        searching: false,  ////禁用默认插件的搜索
        paging: false,    ////禁用默认插件的分页
        processing: true, ////启用默认插件的进度条
        serverSide: true,  ////启用后台请求
        destroy: true,  ////销毁旧的table
        sort: false,
        language: {
            zeroRecords: "无相关数据......",
            processing: '<img src="/images/info-loader.gif"/>'  ////设置加载中的等待图标
        },
        initComplete: null
    };
    //step02 插件的扩展方法名称
    $.fn.NewDataTable = function (options, preXhr, xhr) {
        //step03-b 合并用户自定义属性，默认属性
        var _options = $.extend(defaults, options);
        //step4 支持JQuery选择器
        $(this).on('preXhr.dt', function (e, settings, data) {
            /////ajax 请求之前的数据
            if (preXhr != undefined && typeof (preXhr) == "function") {
                preXhr(e, settings, json);
            }
        }).on('xhr.dt', function (e, settings, json) {
            /////ajax 请求完成后的事件
            if (xhr != undefined && typeof (xhr) == "function") {
                xhr(e, settings, json);
            }
        }).DataTable(_options);
    };
})(jQuery);


$(function () {
    ////页面加载时，初始化页面空间name属性。
    ////因为master会自动将服务器控件name属性改变，所以需保留原始的name属性
    $(document).find("input[class*='validate[required']").each(function () {
        $(this).attr("data-prompt-position", "topLeft");
    });
    $(document).find("select[class*='validate[required']").each(function () {
        $(this).attr("data-prompt-position", "topLeft");
    });
    $(document).find("textarea[class*='validate[required']").each(function () {
        $(this).attr("data-prompt-position", "topLeft");
    });
});


SystemParts = {
    ////系统部件方法
    ChangeSystemGetParts: function (obj, partsctrid) {
        ////改变系统下拉框获取部件
        ////系统ID
        var systemid = $(obj).val();
        $ctr = $("#" + partsctrid);
        if (systemid != undefined && systemid != "") {

            $.post("/CommonAjax/ajax_SystemPart.ashx", { action: "changesystemgetparts", systemid: systemid }, function (result) {
                var html = "";
                html += '<option value=""></option>';
                if (result != undefined && result != "") {
                    for (var i in result) {
                        if (result[i].IsUse) {
                            html += '<option value="' + result[i].PartsID + '">' + result[i].PartsName + '</option>';
                        } else {
                            html += '<option class="jinyong" value="' + result[i].PartsID + '">' + result[i].PartsName + '</option>';
                        }
                    }
                }
                $ctr.html(html);
            }, "json");
        } else {
            $ctr.html('<option value=""></option>');
        }
    },
    ////根据部件获取参数方法
    ChangePartsGetParams: function (obj, paramsctrid) {
        ////部件ID
        var partid = $(obj).val();
        $ctr = $("#" + paramsctrid);
        if (partid != undefined && partid != "") {

            $.post("/CommonAjax/ajax_SystemPart.ashx", { action: "changepartgetparams", partid: partid }, function (result) {
                var html = "";
                html += '<option value=""></option>';
                if (result != undefined && result != "") {
                    for (var i in result) {
                        if (result[i].IsUse) {
                            html += '<option value="' + result[i].ParamID + '">' + result[i].ParamName + '</option>';
                        } else {
                            html += '<option class="jinyong" value="' + result[i].ParamID + '">' + result[i].ParamName + '</option>';
                        }
                    }
                }
                $ctr.html(html);
            }, "json");
        } else {
            $ctr.html('<option value=""></option>');
        }
    }
}



Geographic = {
    ////改变国家下拉框获取省份
    ChangeCountryGetProvince: function (obj, provincectrid) {
        ////国家ID
        var countryid = $(obj).val();
        $ctr = $("#" + provincectrid);
        if (countryid != undefined && countryid != "") {

            $.post("/CommonAjax/ajax_Geographic.ashx", { action: "changecountrygetprovince", countryid: countryid }, function (result) {
                var html = "";
                html += '<option value=""></option>';
                if (result != undefined && result != "") {
                    for (var i in result) {
                        if (result[i].IsUse) {
                            html += '<option value="' + result[i].ProvinceID + '">' + result[i].ProvinceName + '</option>';
                        } else {
                            html += '<option class="jinyong" value="' + result[i].ProvinceID + '">' + result[i].ProvinceName + '</option>';
                        }
                    }
                }
                $ctr.html(html);
            }, "json");
        } else {
            $ctr.html('<option value=""></option>');
        }
    },
    ////改变省份下拉框获取城市
    ChangeProvinceGetCity: function (obj, cityctrid) {
        ////省份
        var provinceid = $(obj).val();
        $ctr = $("#" + cityctrid);
        if (provinceid != undefined && provinceid != "") {

            $.post("/CommonAjax/ajax_Geographic.ashx", { action: "changeprovincegetcity", provinceid: provinceid }, function (result) {
                var html = "";
                html += '<option value=""></option>';
                if (result != undefined && result != "") {
                    for (var i in result) {
                        if (result[i].IsUse) {
                            html += '<option value="' + result[i].CityID + '">' + result[i].CityName + '</option>';
                        } else {
                            html += '<option class="jinyong" value="' + result[i].CityID + '">' + result[i].CityName + '</option>';
                        }
                    }
                }
                $ctr.html(html);
            }, "json");
        } else {
            $ctr.html('<option value=""></option>');
        }
    },
}

AirPort = {
    ////机场设备方法
    ChangeAirportGetEquipment: function(obj, equipmentctrid) {
        ////改变机场下拉框获取设备
        ////机场ID
        var airportid = $(obj).val();
        $ctr = $("#" + equipmentctrid);
        if (airportid != undefined && airportid != "") {

            $.post("/CommonAjax/ajax_Airport.ashx", { action: "changeairportgetequipment", airportid: airportid }, function (result) {
                var html = "";
                html += '<option value=""></option>';
                if (result != undefined && result != "") {
                    for (var i in result) {
                        if (result[i].IsUse) {
                            html += '<option value="' + result[i].EquipmentID + '">' + result[i].EquipmentName + '</option>';
                        } else {
                            html += '<option class="jinyong" value="' + result[i].EquipmentID + '">' + result[i].EquipmentName + '</option>';
                        }
                    }
                }
                $ctr.html(html);
            }, "json");
        } else {
            $ctr.html('<option value=""></option>');
        }
    },
    ////设备系统方法
    ChangeEquipmentGetSystem: function (obj, systemctrid) {
        ////改变设备下拉框获取系统
        ////设备ID
        var equipmentid = $(obj).val();
        $ctr = $("#" + systemctrid);
        if (equipmentid != undefined && equipmentid != "") {

            $.post("/CommonAjax/ajax_Airport.ashx", { action: "changeequipmentgetsystem", equipmentid: equipmentid }, function (result) {
                var html = "";
                html += '<option value=""></option>';
                if (result != undefined && result != "") {
                    for (var i in result) {
                        html += '<option value="' + result[i].SystemID + '">' + result[i].SystemName + '</option>';
                    }
                }
                $ctr.html(html);
            }, "json");
        } else {
            $ctr.html('<option value=""></option>');
        }
    },
}


  