﻿
/*
*创建人：宋欣
*创建时间：2014-5-24 
*描述：系统web通用弹出框
*/
var MessageBox = {
    alert: function (msg) {
        art.dialog({
            content: msg
        });
    },
    success: function (option) {
        ////成功弹出框
        var _default = {
            title: "标题",
            icon: 'succeed',
            time: 2,
            content: "成功"
        };
        var _option = $.extend(_default, option);
        art.dialog(_option);
    },
    error: function (msg) {
        ////错误弹出框
        var content = "错误";
        if (msg != undefined && msg != "") {
            content = msg;
        }
        art.dialog({
            title: "标题",
            icon: 'error',
            time: 2,
            content: content
        });
    },
    wait: function () {
        art.dialog({
            cancel: false,
            title: false,
            padding: "0px,25px,20px,25px",
            width: "200px",
            height: "120px",
            content: '<div style="text-align:center;height:100px;width:180px;padding-top:20px;"><img style="height:60px;" src="loader.gif"><br/><span style="color:red">数据提交中，请等待......</span></div>',
            fixed: true,
            drag: false,
            resize: false,
            lock: true,
            background: '#676363', // 背景色
            opacity: 0.87,	// 透明度
        });
    },
    close: function () {
        var list = art.dialog.list;
        for (var i in list) {
            list[i].close();
        };
    },
    confirm: function (content, okfunc, cancelfunc) {
        ////确认弹出框
        artDialog({
            title: "标题",
            icon: 'question',
            content: content,
            fixed: true,
            lock: true,
            opacity: .1,
            okVal: "确定",
            ok: function () {
                ////确认回调函数
                if (okfunc != undefined && typeof (okfunc) == "function") {
                    okfunc();
                } else {
                    this.close();
                }
            },
            cancelVal: "取消",
            cancel: function () {
                ////取消回调函数
                if (cancelfunc != undefined && typeof (cancelfunc) == "function") {
                    cancelfunc();
                } else {
                    this.close();
                }
            }
        });
    },
    dialog: function (option) {
        ////自定义弹出框，参数为artdialog参数
        art.dialog(option);
    }
};

function ChangeChkboxVal(obj) {
    ////改变选中复选框复制
    if (obj != undefined && obj.checked) {
        $(obj).val("true").attr("checked", "checked");
    } else {
        $(obj).val("false").removeAttr("checked");
    }
}


String.prototype.format = function (args) {
    var result = this;
    if (arguments.length > 0) {
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                if (args[key] != undefined) {
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    var reg = new RegExp("({)" + i + "(})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
}

//var template1 = "我是{0}，今年{1}了";
//var template2 = "我是{name}，今年{age}了";
//var result1 = template1.format("loogn", 22);
//var result2 = template2.format({ name: "loogn", age: 22 });