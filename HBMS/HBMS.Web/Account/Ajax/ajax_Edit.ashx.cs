﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：用户编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using HBMS.Model;
using Resources;

namespace HBMS.Web.Account.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }
            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            BLL.HY_BMS_Users bll = BLLFactory.CreateInstance<BLL.HY_BMS_Users>();
            HY_BMS_Users model = bll.GetModel(Context.Request.Params["UserCode"]);
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            
            bll.Update(model);
            DataSet ds = bll.GetList(string.Format(" UserCode!='{0}' AND UserName='{1}' ", model.UserCode, model.UserName));
            if (ds.Tables[0].Rows.Count == 0)
            {
                ////如果用户角色存在更新，否则新增
                Model.HY_BMS_UserRole userRole = new HY_BMS_UserRole();
                TryUpdateModel(userRole);
                userRole.UseCode = model.UserCode;
                BLL.HY_BMS_UserRole bllUserRole = BLLFactory.CreateInstance<BLL.HY_BMS_UserRole>();
                DataSet ds2 = bllUserRole.GetList(string.Format(" UseCode='{0}' ", userRole.UseCode));
                if (ds2.Tables[0].Rows.Count > 0)
                {
                    bllUserRole.Update(userRole);
                }
                else
                {
                    bllUserRole.Add(userRole);
                }
               
                
                
                return JsonSuccess();
            }
            return JsonError(Msg.errorCodeExist);
        }
    }
}