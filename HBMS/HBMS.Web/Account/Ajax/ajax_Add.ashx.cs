﻿// 
// 创建人：宋欣
// 创建时间：2015-02-02
// 功能：用户新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using HBMS.Model;

namespace HBMS.Web.Account.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_Users model = new Model.HY_BMS_Users();
            TryUpdateModel(model);
            BLL.HY_BMS_Users bll = BLLFactory.CreateInstance<BLL.HY_BMS_Users>();
            DataSet ds = bll.GetList(string.Format(" UserCode='{0}' OR UserName='{1}' ", model.UserCode, model.UserName));
            if (ds.Tables[0].Rows.Count == 0)
            {
                Model.HY_BMS_UserRole userRole=new HY_BMS_UserRole();
                TryUpdateModel(userRole);
                userRole.UseCode = model.UserCode;
                model.PassWord = MD5Encrypt.Encrypt(ConfigHelper.GetAppSettingString("originalpwd"));
                model.IsUse = true;
                bll.Add(model);
                BLLFactory.CreateInstance<BLL.HY_BMS_UserRole>().Add(userRole);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorCodeExist);
        }
    }
}