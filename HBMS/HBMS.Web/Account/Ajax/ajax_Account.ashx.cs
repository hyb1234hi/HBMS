﻿// 
// 创建人：宋欣
// 创建时间：2015-01-31
// 功能：用户管理请求后台
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.BLL;
using HBMS.Factory;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Account.Ajax
{
    /// <summary>
    ///     ajax_Account 的摘要说明
    /// </summary>
    public class ajax_Account : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult=this.QueryAccount();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryAccount()
        {
            ////查询条件
            StringBuilder where=new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ",this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["name"]))
            {
                where.AppendFormat(" AND ( UserCode LIKE '%{0}%' OR UserName LIKE '%{0}%' OR CName LIKE '%{0}%' )",
                                   Context.Request.Params["name"].Trim());
            }
            BLL.View_Users bll = BLLFactory.CreateInstance<View_Users>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " UserCode ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_Users> list = bll.DataTableToList(ds.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                        {
                            data = from l in list 
                                   select new
                                           {
                                              l.UserCode,
                                              l.UserName,
                                              l.CName,
                                              l.EName,
                                              l.Sex,
                                              SexDesc=l.Sex.ToSex(),
                                              l.PhoneNo,
                                              l.Email,
                                              l.DefaultAirPortID,
                                              l.AirPortName,
                                              l.RoleName,
                                              l.IsUse,
                                              IsUseDesc = l.IsUse.ToUse()
                                           },
                            totalCount=Pager.TotalCount,
                            totalPages = Pager.TotalPage,
                            visiblePages = Pager.VisiblePages,
                            startPage = Pager.PageIndex,
                        }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}