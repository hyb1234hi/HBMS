﻿// 
// 创建人：宋欣
// 创建时间：2015-02-13
// 功能：编辑用户
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using HBMS.Factory;

namespace HBMS.Web.Account
{
    public partial class Edit : BasePage
    {
        /// <summary>
        /// </summary>
        public HBMS.Model.HY_BMS_Users Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.Params["usercode"]))
            {
                this.BindData();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.ucrSex.ControlName = "Sex";
            this.ucrLang.ControlName = "defaultSpras";
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navAccount + " </a> >> " +
                                Resources.Nav.navAccountEdit);
        }

        private void BindData()
        {
            this.Model = BLLFactory.CreateInstance<BLL.HY_BMS_Users>().GetModel(Request.Params["usercode"]);
            this.uctrAirport.SelectedValue = this.Model.DefaultAirPortID;
            DataSet ds =
                BLLFactory.CreateInstance<BLL.HY_BMS_UserRole>().GetList(string.Format("UseCode='{0}'", Model.UserCode));
            if (ds.Tables[0].Rows.Count > 0)
            {
                this.uctrRole.SelectedVal = ds.Tables[0].Rows[0]["RoleID"].ToString();
            }
        }
    }
}