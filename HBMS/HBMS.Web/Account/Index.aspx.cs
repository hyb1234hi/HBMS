﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.Account
{
    public partial class Index : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation = RebuildLocation(Resources.Nav.navAccount);
        }
    }
}