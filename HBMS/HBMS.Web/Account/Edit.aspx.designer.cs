﻿//------------------------------------------------------------------------------
// <自动生成>
//     此代码由工具生成。
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。 
// </自动生成>
//------------------------------------------------------------------------------

namespace HBMS.Web.Account {
    
    
    public partial class Edit {
        
        /// <summary>
        /// ucrSex 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::HBMS.Web.CustomerControl.AccountCtr.SexCtr ucrSex;
        
        /// <summary>
        /// ucrLang 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::HBMS.Web.CustomerControl.LanguageCtr ucrLang;
        
        /// <summary>
        /// uctrRole 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::HBMS.Web.CustomerControl.AccountCtr.RoleCtr uctrRole;
        
        /// <summary>
        /// uctrAirport 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::HBMS.Web.CustomerControl.AirportCtr.AirportNameCtr uctrAirport;
        
        /// <summary>
        /// Master 属性。
        /// </summary>
        /// <remarks>
        /// 自动生成的属性。
        /// </remarks>
        public new HBMS.Web.Main Master {
            get {
                return ((HBMS.Web.Main)(base.Master));
            }
        }
    }
}
