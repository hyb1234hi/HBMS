﻿// 
// 创建人：宋欣
// 创建时间：2015-01-31
// 功能：用户账户新增页面
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Account
{
    public partial class Add : BasePage
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.ucrSex.ControlName = "Sex";
            this.ucrLang.ControlName = "defaultSpras";
            ////设置面包线
            this.Master.CurPageLocation =  RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navAccount + " </a> >> " +
                                          Resources.Nav.navAccountAdd); 
        }

        
    }
}