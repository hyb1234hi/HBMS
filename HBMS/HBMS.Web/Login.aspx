﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="HBMS.Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%=Resources.PageTitle.pageTitleLogin %></title>
    <link href="/css/login.css" rel="stylesheet" />
    <link href="/script/dialog/skins/idialog.css" rel="stylesheet" />
    <script src="/script/lib/jquery-1.10.2.js"></script>
    <script src="/script/dialog/artDialog.js"></script>
    <script src="/script/dialog/artDialog.source.js"></script>
    <script src="/script/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="login_top">
            <div id="welcome">
                <%=Resources.Msg.msgWeclomeHBMS %>
            </div>
        </div>
        <div id="login_center">
            <div id="login_area">
                <div id="login_form">
                    <div id="login_tip">
                        <%=Resources.Lbl.lblUserLoginInfo %>&nbsp;&nbsp;
                    </div>
                    <div class="login-input" style="margin-top: 10px;">
                        <%--   <asp:TextBox ID="txtUserName" CssClass="username" runat="server"></asp:TextBox>--%>
                        <span>用户名:</span><asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                    </div>
                    <div class="login-input">
                        <%--    <asp:TextBox ID="txtPwd" CssClass="pwd" runat="server" TextMode="Password"></asp:TextBox>--%>
                        <span>密码:</span><asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="login-input">
                        <%-- <asp:DropDownList CssClass="lang" ID="ddlLanguage" runat="server"></asp:DropDownList>--%>
                        <span>语言:</span>
                        <asp:DropDownList ID="ddlLanguage" runat="server"></asp:DropDownList>
                    </div>
                    <div id="btn_area">
                        <asp:Button ID="sub_btn" runat="server" Text="登&nbsp;&nbsp;录" OnClick="btnLogin_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div id="login_bottom">
            <%=Resources.Lbl.lblCopyrightTip %>
        </div>
    </form>
</body>
</html>
