﻿// 
// 创建人：宋欣
// 创建时间：2015-02-13
// 功能：基类
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Linq;
using System.Reflection;

namespace HBMS.Web
{
    public class BasePage : System.Web.UI.Page
    {
        /// <summary>
        ///     获取路径
        /// </summary>
        /// <param name="curlocation"></param>
        /// <returns></returns>
        public string RebuildLocation(string curlocation)
        {
            return "<a href=\"/Home/Index.aspx\">" + Resources.Nav.navHome + "</a> >> " + curlocation;
        }

        /// <summary>
        ///     拼接实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public void TryUpdateModel<T>(T t)
        {
            Type type = t.GetType();
            PropertyInfo[] pi = type.GetProperties();
            object val = null;
            foreach (
                var p in pi.Where(p => Request.Params[p.Name] != null && !string.IsNullOrEmpty(Request.Params[p.Name])))
            {
                ////给一个对象属性赋值可以通过PropertyInfo.SetValue()方式进行赋值，但要注意值的类型要与属性保持一致。   
                ////不确定目标类型，如何进行类型转换。
                val = Request.Params[p.Name];
                if (!p.PropertyType.IsGenericType)
                {
                    p.SetValue(t,
                               val != null && string.IsNullOrEmpty(val.ToString())
                                   ? null
                                   : Convert.ChangeType(Request.Params[p.Name], p.PropertyType), null);
                }
                else
                {
                    //泛型Nullable<>
                    Type genericTypeDefinition = p.PropertyType.GetGenericTypeDefinition();
                    if (genericTypeDefinition == typeof (Nullable<>))
                    {
                        p.SetValue(t,
                                   val != null && string.IsNullOrEmpty(val.ToString())
                                       ? null
                                       : Convert.ChangeType(val, Nullable.GetUnderlyingType(p.PropertyType)), null);
                    }
                }
            }
        }
    }
}