﻿// 
// 创建人：宋欣
// 创建时间：2015-02-12
// 功能：获取当前用户登录的
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HBMS.Model;

namespace HBMS.Web
{
    public class GoalUserInfoConfig
    {
        public static CurrentUser GetGoalUserInfo()
        {
            CurrentUser GoalUserInfo = null;

            try
            {
                if (HttpContext.Current.Session["GoalUserInfo"] == null)
                {
                    if (HttpContext.Current.Session["UserCode"] != null &&
                        HttpContext.Current.Session["LanguageID"] != null)
                    {
                        string userCode = HttpContext.Current.Session["UserCode"].ToString();
                        ////查找当前用户信息
                        Model.HY_BMS_Users users =
                            Factory.BLLFactory.CreateInstance<BLL.HY_BMS_Users>().GetModel(userCode);
                        if (users != null)
                        {
                            GoalUserInfo = new CurrentUser();
                            //GoalUserInfo.Spras = Convert.ToInt32(users.defaultSpras);
                            GoalUserInfo.Spras = Convert.ToInt32(HttpContext.Current.Session["LanguageID"].ToString());
                            GoalUserInfo.Users = users;
                            List<int> roles = new List<int>();
                            ////查找用户角色
                            List<Model.HY_BMS_UserRole> list = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_UserRole>()
                                                                      .GetModelList(string.Format(" UseCode='{0}' ",
                                                                                                  users.UserCode));
                            roles.AddRange(from r in list select r.RoleID);
                            GoalUserInfo.RoleCollection = roles;
                            HttpContext.Current.Session["GoalUserInfo"] = GoalUserInfo;
                        }
                        else
                        {
                            throw new ArgumentNullException();
                        }
                    }
                    else
                    {
                        throw new ArgumentNullException();
                    }
                }
                GoalUserInfo = (CurrentUser)HttpContext.Current.Session["GoalUserInfo"];
                return GoalUserInfo;
            }
            catch (Exception)
            {
                HttpContext.Current.Response.Redirect("/Login.aspx");
                throw;
            }

            //CurrentUser GoalUserInfo = new CurrentUser();
            //GoalUserInfo.Spras = 1;
            //Model.HY_BMS_Users users = new Model.HY_BMS_Users();
            //users.UserCode = "01620";
            //users.UserName = "test";
            //users.CName = "测试";
            //users.EName = "test";
            //GoalUserInfo.Users = users;
            //List<int> roles = new List<int>();
            //roles.Add(1);
            //GoalUserInfo.RoleCollection = roles;
            //return GoalUserInfo;
        }
    }
}