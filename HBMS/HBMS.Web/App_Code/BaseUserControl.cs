﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HBMS.Model;

namespace HBMS.Web
{
    public class BaseUserControl: System.Web.UI.UserControl
    {
        /// <summary>
        /// 登录用户的全局信息
        /// </summary>
        public CurrentUser GoalUserInfo { get { return GoalUserInfoConfig.GetGoalUserInfo(); } }
    }
}