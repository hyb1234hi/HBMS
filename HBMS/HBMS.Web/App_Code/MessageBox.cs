// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：后台弹出框
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Text;
using System.Web.UI;

namespace HBMS.Web
{
    /// <summary>
    ///     显示消息提示对话框。
    ///     Copyright (C) Maticsoft
    /// </summary>
    public class MessageBox
    {
        private MessageBox()
        {
        }

        /// <summary>
        ///     显示消息提示对话框
        /// </summary>
        /// <param name="page">当前页面指针，一般为this</param>
        /// <param name="msg">提示信息</param>
        public static void Show(Page page, string msg)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "message",
                                                    "<script language='javascript' defer>MessageBox.alert('" + msg +
                                                    "');</script>");
        }

        /// <summary>
        ///     显示成功消息提示对话框
        /// </summary>
        /// <param name="page">当前页面指针，一般为this</param>
        /// <param name="msg">提示信息</param>
        public static void Success(Page page, string msg = "")
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "message",
                                                    "<script language='javascript' defer>MessageBox.success('" + msg +
                                                    "');</script>");
        }

        /// <summary>
        ///     显示成功消息提示对话框
        /// </summary>
        /// <param name="page">当前页面指针，一般为this</param>
        /// <param name="msg">提示信息</param>
        public static void Error(Page page, string msg = "")
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "message",
                                                    "<script language='javascript' defer>MessageBox.error('" + msg +
                                                    "');</script>");
        }

        /// <summary>
        ///     显示消息提示对话框，并进行页面跳转
        /// </summary>
        /// <param name="page">当前页面指针，一般为this</param>
        /// <param name="msg">提示信息</param>
        /// <param name="url">跳转的目标URL</param>
        public static void ShowAndRedirect(Page page, string msg, string url)
        {
            //Response.Write("<script>alert('帐户审核通过！现在去为企业充值。');window.location=\"" + pageurl + "\"</script>");
            page.ClientScript.RegisterStartupScript(page.GetType(), "message",
                                                    "<script language='javascript' defer>MessageBox.alert('" + msg +
                                                    "');window.location=\"" + url + "\"</script>");
        }

        public static void ErrorAndRedirect(Page page, string msg, string url)
        {
            //Response.Write("<script>alert('帐户审核通过！现在去为企业充值。');window.location=\"" + pageurl + "\"</script>");
            page.ClientScript.RegisterStartupScript(page.GetType(), "message",
                                                    "<script language='javascript' defer>MessageBox.error('" + msg +
                                                    "');window.location=\"" + url + "\"</script>");
        }

        /// <summary>
        ///     显示消息提示对话框，并进行页面跳转
        /// </summary>
        /// <param name="page">当前页面指针，一般为this</param>
        /// <param name="msg">提示信息</param>
        /// <param name="url">跳转的目标URL</param>
        public static void ShowAndRedirects(Page page, string msg, string url)
        {
            StringBuilder Builder = new StringBuilder();
            Builder.Append("<script language='javascript' defer>");
            Builder.AppendFormat("MessageBox.alert('{0}');", msg);
            Builder.AppendFormat("top.location.href='{0}'", url);
            Builder.Append("</script>");
            page.ClientScript.RegisterStartupScript(page.GetType(), "message", Builder.ToString());
        }

        /// <summary>
        ///     输出自定义脚本信息
        /// </summary>
        /// <param name="page">当前页面指针，一般为this</param>
        /// <param name="script">输出脚本</param>
        public static void ResponseScript(Page page, string script)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "message",
                                                    "<script language='javascript' defer>" + script + "</script>");
        }
    }
}