﻿// 
// 创建人：宋欣
// 创建时间：2015-02-03
// 功能：基础资源文件
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using HBMS.Common;

namespace HBMS.Web
{
    public static class BaseResources
    {
        /// <summary>
        /// 日期格式转换
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string yyyy_MM_dd(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return Convert.ToDateTime(obj).ToString("yyyy-MM-dd");
            }
            return "";
        }

        /// <summary>
        ///     是/否
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string YesNo(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return Convert.ToBoolean(obj) ? Resources.Lbl.lblYes : Resources.Lbl.lblNo;
            }
            return "";
        }

        /// <summary>
        ///     启用 / 禁用 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToUse(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return Convert.ToBoolean(obj) ? Resources.Lbl.lblUse : Resources.Lbl.lblDisable;
            }
            return "";
        }

        /// <summary>
        /// 参数类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ParamType(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                int paramType = Convert.ToInt32(obj);
                if (paramType == (int)ParamTypeEnum.布尔)
                {
                    return Resources.Lbl.lblParamsBooleanType;
                }
                return Resources.Lbl.lblParamsNumberType;
            }
            return "";
        }

        /// <summary>
        /// 参数类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
       public static string ParamStateDesc(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                int state = Convert.ToInt32(obj);
                switch ((ParamStateEnum)state)
                {
                    case ParamStateEnum.过高:
                        return Resources.Lbl.lblStateGuoGao;
                    case ParamStateEnum.异常:
                        return Resources.Lbl.lblStateYiChang;
                    case ParamStateEnum.过低:
                        return Resources.Lbl.lblStateGuoDi;
                    default:
                        return Resources.Lbl.lblStateZhengChange;
                }
            }
            return "";
        }

        /// <summary>
        /// 参数类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string EquipmentState(this object obj)
        {
            ////警告 提醒 故障
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                int state = Convert.ToInt32(obj);
                switch ((AirePortStaetEnum)state)
                {
                    case AirePortStaetEnum.告急:
                        return Resources.Lbl.lblStateGaoJi;
                    case AirePortStaetEnum.提醒:
                        return Resources.Lbl.lblStateTiXing;
                    case AirePortStaetEnum.警示:
                        return Resources.Lbl.lblStateJingShi;
                    case AirePortStaetEnum.断网:
                        return Resources.Lbl.lblStateDuanWanag;
                    default:
                        return Resources.Lbl.lblStateZhengChange;
                }
            }
            return "";
        }

        /// <summary>
        /// 性别
        /// </summary>
        /// <param name="sex"></param>
        /// <returns></returns>
        public static string ToSex(this object sex)
        {
            if (sex != null)
            {
                return Convert.ToInt32(sex) == 1 ? Resources.Lbl.lblMale : Resources.Lbl.lblFemale;
            }
            return "";

        }
    }
}