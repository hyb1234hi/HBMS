﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.Equipment
{
    public partial class Edit : BasePage
    {
        /// <summary>
        /// 设备表ID
        /// </summary>
        public string EquipmentID { get; set; }
        /// <summary>
        /// 设备实体
        /// </summary>
        public Model.View_EquipmentDesc Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               if (!string.IsNullOrEmpty(Request.QueryString["id"]))
               {
                   this.EquipmentID = Request.QueryString["id"];
                   this.BindData();
               }
               else
               {
                   MessageBox.ErrorAndRedirect(this,Resources.Msg.errorParam,"Index.aspx");
               }
               
            }
        }

        private void BindData()
        {
            BLL.View_EquipmentDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentDesc>();
            DataSet ds =
                bll.GetList(string.Format(" EquipmentID='{0}' AND SPRAS={1} ", this.EquipmentID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_EquipmentDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
                this.uctrAirportName.SelectedValue = Model.AirPortID;
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.uctrAirportName.ControlName = "AirPortID";
            this.uctrAirportName.SelectedValue = Model.AirPortID;
            ////设置面包线
            this.Master.CurPageLocation = RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navEquipment + " </a> >> " +
                                          Resources.Nav.navAccountEdit);
        }
    }
}