﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.Equipment.Add" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register src="../CustomerControl/AirportCtr/AirportNameCtr.ascx" tagname="AirportNameCtr" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
             if ($form.validationEngine('validate')) {
                 $.post("Ajax/ajax_Add.ashx", $form.formSerialize(), function (result) {
                     if (result.result == 1) {
                         MessageBox.success();
                     } else {
                         MessageBox.error(result.error);
                     }
                 }, "json");
             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="add" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td style="width: 13%;">
                                <label class="require"><%=Resources.Lbl.lblEquipmentCode %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="EquipmentCode" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblEquipmentName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="EquipmentName" class="validate[required] input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirportName %>:</label></td>
                            <td class="wp23">
                                <uc1:AirportNameCtr ID="uctrAirportName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblChuChangRiQI %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ProductDate" class="validate[required] input-text Wdate" onclick="WdatePicker()" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblJiaoFuRiQi %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="UserdDate" class="validate[required] input-text Wdate" onclick="WdatePicker()" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblBaoXiuQi %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="Guarantee" class="validate[required,custom[integer]] input-text input-num-100" /><%=Resources.Lbl.lblDayUnit %>
                            </td>
                        </tr>
                        <!--机场提醒人-->
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentJCTSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="AirPortPromptUser" class="validate[required] input-text wp43" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="AirPortPromptPhone" class="validate[required] input-text wp43" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentJCJSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="AirPortWarningUser" class="validate[required] input-text wp43" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="AirPortWarningPhone" class="validate[required] input-text wp43" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentJCGJFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="AirPortErrorUser" class="validate[required] input-text wp43" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="AirPortErrorPhone" class="validate[required] input-text wp43" />
                            </td>
                            <td></td>
                        </tr>
                        <!--公司提醒人-->
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentGSTSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="CompanyPromptUser" class="validate[required] input-text wp43" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="CompanyPromptPhone" class="validate[required] input-text wp43" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentGSTSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="CompanyWarningUser" class="validate[required] input-text wp43" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="CompanyWarningPhone" class="validate[required] input-text wp43" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentGSGJFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="CompanyErrorUser" class="validate[required] input-text wp43" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="CompanyErrorPhone" class="validate[required] input-text wp43" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea name="Memo" class="wp100" rows="5"></textarea>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
