﻿// 
// 创建人：宋欣
// 创建时间：2015-02-02
// 功能：编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Equipment.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            Model.HY_BMS_Equipment model = new Model.HY_BMS_Equipment();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            Model.HY_BMS_EquipmentDesc desc = new Model.HY_BMS_EquipmentDesc();
            TryUpdateModel(desc);
            desc.SPRAS = GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_Equipment bll = BLLFactory.CreateInstance<BLL.HY_BMS_Equipment>();
            BLL.HY_BMS_EquipmentDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_EquipmentDesc>();
            DataSet ds = bll.GetList(string.Format(" EquipmentCode='{0}' AND EquipmentID<>{1}  ", model.EquipmentCode,model.EquipmentID));
            if (ds.Tables[0].Rows.Count == 0)
            {
                ////更新数据
                bll.Update(model);
                blldesc.Update(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorCodeExist);
        }
    }
}