﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Equipment.Ajax
{
    /// <summary>
    /// ajax_Index 的摘要说明
    /// </summary>
    public class ajax_Index : BaseHandler
    {

        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.QueryAccount();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryAccount()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ", this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["code"]))
            {
                ////设备编号
                where.AppendFormat(" AND EquipmentCode='{0}' ", Context.Request.Params["code"].FilterQueryCondition());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["airport"]))
            {
                ////机场编号
                where.AppendFormat(" AND  AirPortID={0}", Context.Request.Params["airport"].FilterQueryCondition());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["name"]))
            {
                ////设备名称
                where.AppendFormat(" AND  EquipmentName LIKE '%{0}%' ", Context.Request.Params["name"].FilterQueryCondition());
            }
            BLL.View_EquipmentDesc bll = BLLFactory.CreateInstance<BLL.View_EquipmentDesc>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " EquipmentID ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentDesc> list = bll.DataTableToList(ds.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                    {
                        data = from l in list
                               select new
                               {
                                   l.EquipmentID,
                                   l.EquipmentCode,
                                   l.SPRAS,
                                   l.EquipmentName,
                                   l.Guarantee,
                                   l.IsUse,
                                   l.AirPortID,
                                   l.AirPortCode,
                                   ProductDate=l.ProductDate.ToDateNoSFM(),
                                   UserdDate = l.UserdDate.ToDateNoSFM(),
                                   l.AirPortErrorUser,
                                   l.AirPortErrorPhone,
                                   l.AirPortPromptUser,
                                   l.AirPortPromptPhone,
                                   l.AirPortWarningUser,
                                   l.AirPortWarningPhone,
                                   l.CompanyPromptUser,
                                   l.CompanyPromptPhone,
                                   l.CompanyWarningUser,
                                   l.CompanyWarningPhone,
                                   l.CompanyErrorUser,
                                   l.CompanyErrorPhone,
                                   l.AirPortName,
                                   l.AirPortAddress
                               },
                        totalCount = Pager.TotalCount,
                        totalPages = Pager.TotalPage,
                        visiblePages = Pager.VisiblePages,
                        startPage = Pager.PageIndex
                    }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}