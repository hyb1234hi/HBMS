﻿// 
// 创建人：宋欣
// 创建时间：2015-02-02
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Equipment.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_Equipment model = new Model.HY_BMS_Equipment();
            TryUpdateModel(model);
            model.IsUse = true;
             Model.HY_BMS_EquipmentDesc desc = new Model.HY_BMS_EquipmentDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
           ////实例化
            BLL.HY_BMS_Equipment bll = BLLFactory.CreateInstance<BLL.HY_BMS_Equipment>();
            BLL.HY_BMS_EquipmentDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_EquipmentDesc>();
            DataSet ds = bll.GetList(string.Format(" EquipmentCode='{0}'  ", model.EquipmentCode));
            if (ds.Tables[0].Rows.Count == 0 )
            {
                model.EquipmentID=bll.Add(model);
                desc.EquipmentID = model.EquipmentID;
                blldesc.Add(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorCodeExist);
        }
    }
}