﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.Equipment.Edit" %>

<%@ Import Namespace="HBMS.Common" %>
<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../CustomerControl/AirportCtr/AirportNameCtr.ascx" TagName="AirportNameCtr" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td style="width: 13%;">
                                <label class="require"><%=Resources.Lbl.lblEquipmentCode %>:</label></td>
                            <td class="wp23">
                                <input type="hidden" name="EquipmentID" value="<%= Model.EquipmentID %>"/>
                                <input type="text" name="EquipmentCode" class="validate[required] input-text wp100" value="<%= Model.EquipmentCode %>" />
                                
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblEquipmentName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="EquipmentName" class="validate[required] input-text wp100" value="<%= Model.EquipmentName %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirportName %>:</label></td>
                            <td class="wp23">
                                <uc1:AirportNameCtr ID="uctrAirportName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblChuChangRiQI %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ProductDate" class="validate[required] input-text Wdate" onclick="WdatePicker()" value="<%= Model.ProductDate.ToDateNoSFM() %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblJiaoFuRiQi %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="UserdDate" class="validate[required] input-text Wdate" onclick="WdatePicker()" value="<%= Model.UserdDate.ToDateNoSFM() %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblBaoXiuQi %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="Guarantee" class="validate[required,custom[integer]] input-text input-num-100" value="<%= Model.Guarantee %>" /><%=Resources.Lbl.lblDayUnit %>
                            </td>
                        </tr>
                        <!--机场提醒人-->
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentJCTSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="AirPortPromptUser" class="validate[required] input-text wp43" value="<%= Model.AirPortPromptUser %>" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="AirPortPromptPhone" class="validate[required] input-text wp43" value="<%= Model.AirPortPromptPhone %>" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentJCJSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="AirPortWarningUser" class="validate[required] input-text wp43" value="<%= Model.AirPortWarningUser %>" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="AirPortWarningPhone" class="validate[required] input-text wp43" value="<%= Model.AirPortWarningPhone %>" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentJCGJFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="AirPortErrorUser" class="validate[required] input-text wp43" value="<%= Model.AirPortErrorUser %>" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="AirPortErrorPhone" class="validate[required] input-text wp43" value="<%= Model.AirPortErrorPhone %>" />
                            </td>
                            <td></td>
                        </tr>
                        <!--公司提醒人-->
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentGSTSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="CompanyPromptUser" class="validate[required] input-text wp43" value="<%= Model.CompanyPromptUser %>" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="CompanyPromptPhone" class="validate[required] input-text wp43" value="<%= Model.CompanyPromptPhone %>" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentGSTSFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="CompanyWarningUser" class="validate[required] input-text wp43" value="<%= Model.CompanyWarningUser %>" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="CompanyWarningPhone" class="validate[required] input-text wp43" value="<%= Model.CompanyWarningPhone %>" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentGSGJFZR %>:</label></td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblUserName %>:</label><input type="text" name="CompanyErrorUser" class="validate[required] input-text wp43" value="<%= Model.CompanyErrorUser %>" />
                            </td>
                            <td colspan="2">
                                <label class="require pr10"><%=Resources.Lbl.lblMobilePhone %>:</label><input type="text" name="CompanyErrorPhone" class="validate[required] input-text wp43" value="<%= Model.CompanyErrorPhone %>" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea name="Memo" class="wp100" rows="5">
                                <%=Model.Memo %>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <label><%=Resources.Lbl.lblUse %>:</label>
                                <% if (Model.IsUse)
                                   { %>

                                <input type="checkbox" value="true" name="IsUse" checked="checked" onclick="ChangeChkboxVal(this)"/>

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsUse" onclick="ChangeChkboxVal(this)"/>
                                <% } %>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
