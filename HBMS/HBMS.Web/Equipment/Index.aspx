﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HBMS.Web.Equipment.Index" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../CustomerControl/AirportCtr/AirportNameCtr.ascx" TagName="AirportNameCtr" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            query(1);
        });

        function GetOptionHtml(row) {
            var optionHtml = "";
            optionHtml += '<a target="_blank" href="Edit.aspx?id=' + row.EquipmentID + '" ><img src="/images/pencil.png" width="16" height="16"></a>';
            return optionHtml;
        }

        function query(page) {
            $("#tbResult").NewDataTable({
                initComplete: function (settings, json) {
                    //////表格加载完后出发事件
                    if (json.data != undefined && json.data != "" && json.data.length > 0) {
                        $('#pagination').twbsPagination({
                            totalPages: json.totalPages,        /////总页数
                            visiblePages: json.visiblePages,    /////最大可显示几页
                            startPage: json.startPage,           ////当前页
                            onPageClick: function (event, page) { ////分页按钮点击事件
                                query(page);
                            },
                            refresh: 'query(1)'         /////刷新按钮
                        });
                        $("#pagination-message").empty().append(showPaginationMsg(json.startPage, json.totalPages, json.totalCount));
                    } else {
                        $("#pagination").twbsPaginationEmpty();
                        $("#pagination-message").twbsPaginationMsgEmpty();
                    }
                },
                ajax: {
                    url: "Ajax/ajax_Index.ashx",   ////请求后台
                    type: "post",
                    data: function (d) { ////查询条件
                        d.action = "query";
                        d.code = $("#txtEquipmentCode").val(); ////编号
                        d.name = $("#txtEquipmentName").val(); ////设别名称
                        d.airport = $("#<%=this.uctrAiportName.ClientControlID%>").val(); ////机场
                        d.page = page; ////页索引
                    },
                },
                columns: [////绑定每列
                    { data: "EquipmentCode" },
                    { data: "EquipmentName" },
                    { data: "AirPortName" },
                    { data: "ProductDate" },
                    { data: "UserdDate" },
                    { data: "Guarantee" },
                    {
                        data: function (row, type, set, meta) {
                            return row.AirPortPromptUser + '(' + row.AirPortPromptPhone + ')';
                        }
                    },
                    {
                        data: function (row, type, set, meta) {
                            return row.AirPortWarningUser + '(' + row.AirPortWarningPhone + ')';
                        }
                    },
                    {
                        data: function (row, type, set, meta) {
                            return row.AirPortErrorUser + '(' + row.AirPortErrorPhone + ')';
                        }
                    },
                    {
                        data: function (row, type, set, meta) {
                            return row.CompanyPromptUser + '(' + row.CompanyPromptPhone + ')';
                        }
                    },
                    {
                        data: function (row, type, set, meta) {
                            return row.CompanyWarningUser + '(' + row.CompanyWarningPhone + ')';
                        }
                    },
                    {
                        data: function (row, type, set, meta) {
                            return row.CompanyErrorUser + '(' + row.CompanyErrorPhone + ')';
                        }
                    },
                    {
                        data: function (row, type, set, meta) {
                            return GetOptionHtml(row);
                        }
                    }
                ]
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div class="box_top"><b class="pl15"><%=Resources.Lbl.lblQueryCondition %></b></div>
            <div class="pt10 pb10">
                <table class="tb-form">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblEquipmentCode %>:</label></td>
                            <td class="wp23">
                                <input type="text" id="txtEquipmentCode" class="input-text" /></td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblEquipmentName %>:</label></td>
                            <td class="wp23">
                                <input type="text" id="txtEquipmentName" class="input-text" /></td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblAirport %>:</label></td>
                            <td class="wp23">
                                <uc1:AirportNameCtr ID="uctrAiportName" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-box ">
                <input type="button" class="btn btn82 btn_search" value="<%=Resources.Btn.btnQuery %>" onclick="query(1)" />
                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                <a class="btn btn82 btn_add" href="Add.aspx"><%=Resources.Btn.btnAdd %></a>
            </div>
        </div>
        <div class="box span10">
            <table id="tbResult" class="tb-data">
                <thead>
                    <tr>
                        <th rowspan="2"><%=Resources.Lbl.lblEquipmentCode %></th>
                        <th rowspan="2"><%=Resources.Lbl.lblEquipmentName %></th>
                        <th rowspan="2"><%=Resources.Lbl.lblAirport %></th>
                        <th rowspan="2"><%=Resources.Lbl.lblChuChangRiQI %></th>
                        <th rowspan="2"><%=Resources.Lbl.lblJiaoFuRiQi %></th>
                        <th rowspan="2"><%=Resources.Lbl.lblBaoXiuQi %></th>
                        <th colspan="3"><%=Resources.Lbl.lblAirport %><%=Resources.Lbl.lblEquipmentFZR %></th>
                        <th colspan="3"><%=Resources.Lbl.lblCompany %><%=Resources.Lbl.lblEquipmentFZR %></th>
                        <th rowspan="2"><%=Resources.Lbl.lblOperation %></th>
                    </tr>
                    <tr>
                        <th><%=Resources.Lbl.lblTiShi %></th>
                        <th><%=Resources.Lbl.lblJingShi %></th>
                        <th><%=Resources.Lbl.lblGaoJi %></th>
                        <th><%=Resources.Lbl.lblTiShi %></th>
                        <th><%=Resources.Lbl.lblJingShi %></th>
                        <th><%=Resources.Lbl.lblGaoJi %></th>
                    </tr>
                </thead>
            </table>
            <div class="block-pagination">
                <ul id="pagination-message" class="controls-message"></ul>
                <ul id="pagination" class="controls-buttons"></ul>
            </div>
        </div>
    </div>
</asp:Content>
