﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.Equipment
{
    public partial class Add : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            this.uctrAirportName.ControlName = "AirPortID";
            ////设置面包线
            this.Master.CurPageLocation = RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navEquipment + " </a> >> " +
                                          Resources.Nav.navEquipmentAdd);
        }
    }
}