﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：分配菜单
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.Right.Role
{
    public partial class MenuRight : BasePage
    {
        protected string  RoleID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                this.RoleID = Request.QueryString["id"];
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navRole + " </a> >> " +
                                Resources.Nav.navRoleMenu);
        }
    }
}