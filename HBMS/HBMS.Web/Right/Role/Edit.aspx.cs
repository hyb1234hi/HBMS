﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.Right.Role
{
    public partial class Edit : BasePage
    {
        /// <summary>
        ///     角色ID
        /// </summary>
        public string RoleID { get; set; }

        /// <summary>
        ///     角色实体
        /// </summary>
        public Model.View_RoleDesc Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    this.RoleID = Request.QueryString["id"];
                    this.BindData();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        private void BindData()
        {
            BLL.View_RoleDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_RoleDesc>();
            DataSet ds =
                bll.GetList(string.Format(" RoleID='{0}' AND spras={1} ", this.RoleID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_RoleDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navRole + " </a> >> " +
                                Resources.Nav.navRoleEdit);
        }
    }
}