﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="MenuRight.aspx.cs" Inherits="HBMS.Web.Right.Role.MenuRight" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/lib/zTreeStyle/zTreeStyle.css" rel="stylesheet" />
    <script src="/script/lib/json2.js"></script>
    <script src="/script/ztree/jquery.ztree.core-3.5.js"></script>
    <script src="/script/ztree/jquery.ztree.excheck-3.5.js"></script>
    <script>
        var setting = {
            view: {
                selectedMulti: true
            },
            check: {
                enable: true,
                chkStyle: "checkbox",
                chkboxType: { "Y": "p", "N": "ps" }
            },
            async: {
                enable: true,
                url: "Ajax/ajax_MenuRight.ashx",
                otherParam: ["roleid", "<%=this.RoleID%>"]
            },
        };



        $(document).ready(function () {
            $.fn.zTree.init($("#treeMenu"), setting);
        });

        function addmenu() {
            ////获取选中的菜单
            var treeObj = $.fn.zTree.getZTreeObj("treeMenu");
            var nodes = treeObj.getCheckedNodes(true);
            var menus = "";
            for (var i in nodes) {
                menus += ",'" + nodes[i].id+"'";
            }
            ////添加菜单
            $.post("Ajax/ajax_AddMenuRight.ashx", { roleid: "<%=this.RoleID%>", menus: menus.substring(1) }, function (result) {
                if (result.result == 1) {
                    MessageBox.success();
                } else {
                    MessageBox.error(result.error);
                }
            }, "json");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <ul id="treeMenu" class="ztree"></ul>
            </div>
            <div>
                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="addmenu();" />
                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
            </div>
        </div>
    </div>
</asp:Content>
