﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Model;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Right.Role.Ajax
{
    /// <summary>
    /// ajax_MenuRight 的摘要说明
    /// </summary>
    public class ajax_MenuRight : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);

            WriteResult = this.GetMenu();

            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string GetMenu()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["roleid"]))
            {
                int roleID = Convert.ToInt32(Context.Request.Params["roleid"]);
                BLL.View_MenuDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_MenuDesc>();
                ////查询菜单
                DataSet ds = bll.GetList(string.Format(" spras={0} AND IsUse=1 ORDER BY OrderIndex", this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////所有启用的菜单
                    List<Model.View_MenuDesc> menus = bll.DataTableToList(ds.Tables[0]);
                    ////所有角色关联的菜单
                    List<Model.View_RoleMenu> roleMenus = new List<View_RoleMenu>();
                    BLL.View_RoleMenu bllRM = Factory.BLLFactory.CreateInstance<BLL.View_RoleMenu>();
                    DataSet ds2 = bllRM.GetList(string.Format(" RoleID={0} AND spras={1} ", roleID, this.GoalUserInfo.Spras));
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        roleMenus = bllRM.DataTableToList(ds2.Tables[0]);
                    }

                    if (menus.Count > 0)
                    {
                        List<Ztree> result = new List<Ztree>();
                        result.AddRange(from m in menus
                                        where m.ParentID == "0"
                                        select new Ztree()
                                            {
                                                id = m.MenuID,
                                                name = m.MenuName,
                                                isParent = menus.Any(i => i.ParentID == m.MenuID),
                                                open = menus.Any(i => i.ParentID == m.MenuID),
                                                ////没有子菜单，且在角色菜单中有值得，则选中
                                                @checked =roleMenus.Any(i => i.MenuID == m.MenuID),
                                                children = menus.Any(i => i.ParentID == m.MenuID) ?
                                                GetChildMenu(m.MenuID, menus, roleMenus) :
                                                new List<Ztree>(),
                                            });
                        ////返回查询结果json
                        return JToken.FromObject(result).ToString();
                    }
                }
            }

            ////如果没有返回空字符
            return NoDataJson();
        }

        /// <summary>
        /// 递归获取子菜单
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="menus"></param>
        /// <param name="roleMenus"></param>
        /// <returns></returns>
        private List<Ztree> GetChildMenu(string pid, List<Model.View_MenuDesc> menus, List<Model.View_RoleMenu> roleMenus)
        {
            List<Ztree> result = new List<Ztree>();
            result.AddRange(from m in menus
                            where m.ParentID == pid
                            select new Ztree()
                            {
                                id = m.MenuID,
                                name = m.MenuName,
                                isParent = menus.Any(i => i.ParentID == m.MenuID),
                                open = menus.Any(i => i.ParentID == m.MenuID),
                                ////没有子菜单，且在角色菜单中有值得，则选中
                                @checked =roleMenus.Any(i => i.MenuID == m.MenuID),
                                children = menus.Any(i => i.ParentID == m.MenuID) ?
                                            GetChildMenu(m.MenuID, menus, roleMenus) :
                                            new List<Ztree>(),
                            });
            return result;
        }
    }
}