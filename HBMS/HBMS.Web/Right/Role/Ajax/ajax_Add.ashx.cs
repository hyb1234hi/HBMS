﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：角色新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Right.Role.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_Role model = new Model.HY_BMS_Role();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Form["IsAdmin"]))
            {
                model.IsAdmin = false;
            }
            model.Type = (int) RoleTypeEnum.RoleType1;
            model.IsUse = true;

            Model.HY_BMS_RoleDesc desc = new Model.HY_BMS_RoleDesc();
            TryUpdateModel(desc);
            desc.spras = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_RoleDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_RoleDesc>();
            ////判断是否有同名的存在
            DataSet ds = blldesc.GetList(string.Format(" RoleName='{0}' AND spras={1} ", desc.RoleName, desc.spras));
            if (ds.Tables[0].Rows.Count == 0)
            {
                BLL.HY_BMS_Role bll = BLLFactory.CreateInstance<BLL.HY_BMS_Role>();
                model.RoleID = bll.Add(model);
                desc.RoleID = model.RoleID;
                blldesc.Add(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorRoleNameExist);
        }
    }
}