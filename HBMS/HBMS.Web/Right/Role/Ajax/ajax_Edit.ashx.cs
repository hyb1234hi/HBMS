﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：角色编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Right.Role.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            Model.HY_BMS_Role model = new Model.HY_BMS_Role();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsAdmin"]))
            {
                model.IsAdmin = false;
            }
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            Model.HY_BMS_RoleDesc desc = new Model.HY_BMS_RoleDesc();
            TryUpdateModel(desc);
            desc.spras = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_Role bll = BLLFactory.CreateInstance<BLL.HY_BMS_Role>();
            BLL.HY_BMS_RoleDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_RoleDesc>();
            DataSet ds =
                blldesc.GetList(string.Format("  RoleName='{0}' and spras='{1}' and RoleID<>{2} ", desc.RoleName,
                                              desc.spras, desc.RoleID));
            if (ds.Tables[0].Rows.Count == 0)
            {
                ////更新数据
                bll.Update(model);
                blldesc.Update(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorRoleNameExist);
        }
    }
}