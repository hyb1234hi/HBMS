﻿// 
// 创建人：宋欣
// 创建时间：2015-03-06
// 功能：新增角色菜单
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Right.Role.Ajax
{
    /// <summary>
    ///     ajax_AddMenuRight 的摘要说明
    /// </summary>
    public class ajax_AddMenuRight : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                WriteResult = this.Add();
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["roleid"]))
            {
                int roleID = Convert.ToInt32(Context.Request.Params["roleid"]);
                string menus = Context.Request.Params["menus"];
                BLLFactory.CreateInstance<BLL.View_RoleMenu>().AddRoleMenu(roleID, menus);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorParam);
        }
    }
}