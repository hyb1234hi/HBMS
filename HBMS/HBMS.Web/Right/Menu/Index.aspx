﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HBMS.Web.Right.Menu.Index" %>
<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register src="../../CustomerControl/LanguageCtr.ascx" tagname="LanguageCtr" tagprefix="uc1" %>
<%@ Register src="../../CustomerControl/MenuCtr.ascx" tagname="MenuCtr" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            query(1);
        });
        function GetOptionHtml(row) {
            var optionHtml = "";
            optionHtml += '<a target="_blank" href="Edit.aspx?id=' + row.MenuID + '" ><img src="/images/pencil.png" width="16" height="16"></a>';
            return optionHtml;
        }

        function query(page) {
            $("#tbResult").NewDataTable({
                initComplete: function (settings, json) {
                    //////表格加载完后出发事件
                    if (json.data != undefined && json.data != "" && json.data.length > 0) {
                        $('#pagination').twbsPagination({
                            totalPages: json.totalPages,        /////总页数
                            visiblePages: json.visiblePages,    /////最大可显示几页
                            startPage: json.startPage,           ////当前页
                            onPageClick: function (event, page) { ////分页按钮点击事件
                                query(page);
                            },
                            refresh: 'query(1)'         /////刷新按钮
                        });
                        $("#pagination-message").empty().append(showPaginationMsg(json.startPage, json.totalPages, json.totalCount));
                    } else {
                        $("#pagination").twbsPaginationEmpty();
                        $("#pagination-message").twbsPaginationMsgEmpty();
                    }
                },
                ajax: {
                    url: "Ajax/ajax_Index.ashx",   ////请求后台
                    type: "post",
                    data: function (d) { ////查询条件
                        d.action = "query";
                        d.name = $("#txtName").val();
                        d.spras = $("#<%=this.uctrLanguage.ClientControlID%>").val();
                        d.pmenu = $("#<%=this.uctrPMenu.ClientControlID%>").val();
                        d.page = page; ////页索引
                    },
                },
                columns: [////绑定每列
                    { data: "MenuID" },
                    { data: "MenuName" },
                    { data: "PMenuName" },
                    { data: "URL" },
                    { data: "IsHideDesc" },
                    { data: "IsUseDesc" },
                    { data: "OrderIndex" },
                    {
                        data: function (row, type, set, meta) {
                            return GetOptionHtml(row);
                        }
                    }
                ]
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div class="box_top"><b class="pl15"><%=Resources.Lbl.lblQueryCondition %></b></div>
            <div class="pt10 pb10">
                <table class="tb-form">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblMenuName %>:</label></td>
                            <td class="wp23">
                                <input type="text" id="txtName" name="name" class="input-text" /></td>
                            <td class="wp10"><label><%=Resources.Lbl.lblSpars %>:</label></td>
                            <td class="wp23">
                                <uc1:LanguageCtr ID="uctrLanguage"  runat="server" />
                            </td>
                            <td class="wp10"><label><%=Resources.Lbl.lblParentMenu %>:</label></td>
                            <td class="wp23">
                                <uc2:MenuCtr ID="uctrPMenu" CSS="wp100 select" ShowAllOption="1" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-box ">
                <input type="button" class="btn btn82 btn_search" value="<%=Resources.Btn.btnQuery %>" onclick="query(1)" />
                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>"/>
                <a class="btn btn82 btn_add" href="Add.aspx"><%=Resources.Btn.btnAdd %></a>
            </div>
        </div>
        <div class="box span10">
            <table id="tbResult" class="tb-data">
                <thead>
                    <tr>
                        <th><%=Resources.Lbl.lblCode %></th>
                        <th><%=Resources.Lbl.lblMenuName %></th>
                        <th><%=Resources.Lbl.lblParentMenu %></th>
                        <th><%=Resources.Lbl.lblLink %></th>
                        <th><%=Resources.Lbl.lblHidden %></th>
                        <th><%=Resources.Lbl.lblUse %></th>
                        <th><%=Resources.Lbl.lblOrderIndex %></th>
                        <th><%=Resources.Lbl.lblOperation %></th>
                    </tr>
                </thead>
            </table>
            <div class="block-pagination">
                <ul id="pagination-message" class="controls-message"></ul>
                <ul id="pagination" class="controls-buttons"></ul>
            </div>
        </div>
    </div>
</asp:Content>
