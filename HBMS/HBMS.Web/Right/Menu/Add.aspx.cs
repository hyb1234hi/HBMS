﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：菜单新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.Right.Menu
{
    public partial class Add : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navMenu + " </a> >> " +
                                Resources.Nav.navMenuAdd);
        }
    }
}