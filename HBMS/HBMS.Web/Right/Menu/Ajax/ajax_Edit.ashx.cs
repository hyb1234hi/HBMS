﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：菜单编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Right.Menu.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            Model.HY_BMS_Menu model = new Model.HY_BMS_Menu();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            if (string.IsNullOrEmpty(Context.Request.Params["IsHide"]))
            {
                model.IsHide = false;
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["MURL"]))
            {
                model.URL = Context.Request.Form["MURL"];
            }
            Model.HY_BMS_MenuDesc desc = new Model.HY_BMS_MenuDesc();
            TryUpdateModel(desc);
            desc.spras = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_Menu bll = BLLFactory.CreateInstance<BLL.HY_BMS_Menu>();
            BLL.HY_BMS_MenuDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_MenuDesc>();
            ////更新数据
            bll.Update(model);
            blldesc.Update(desc);
            return JsonSuccess();
        }
    }
}