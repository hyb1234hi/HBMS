﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：菜单查询
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Factory;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Right.Menu.Ajax
{
    /// <summary>
    ///     ajax_Index 的摘要说明
    /// </summary>
    public class ajax_Index : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.QueryAccount();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryAccount()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.Append("1=1");
            if (!string.IsNullOrEmpty(Context.Request.Params["name"]))
            {
                where.AppendFormat(" AND MenuName LIKE '%{0}%'", Context.Request.Params["name"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["spras"]))
            {
                where.AppendFormat(" AND spras='{0}'", Context.Request.Params["spras"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["pmenu"]))
            {
                where.AppendFormat(" AND ParentID='{0}'", Context.Request.Params["pmenu"].Trim());
            }
            BLL.View_MenuDesc bll = BLLFactory.CreateInstance<BLL.View_MenuDesc>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " OrderIndex ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_MenuDesc> list = bll.DataTableToList(ds.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                        {
                            data = from l in list
                                       select  new
                                           {
                                               l.MenuID,
                                               l.ParentID,
                                               l.MenuName,
                                               l.spras,
                                               l.PMenuName,
                                               l.OrderIndex,
                                               l.URL,
                                               l.IsHide,
                                               IsHideDesc=l.IsHide.YesNo(),
                                               l.IsUse,
                                               IsUseDesc = l.IsUse.YesNo()
                                           },
                            totalCount = Pager.TotalCount,
                            totalPages = Pager.TotalPage,
                            visiblePages = Pager.VisiblePages,
                            startPage = Pager.PageIndex,
                        }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}