﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：新增菜单
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Right.Menu.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_Menu model = new Model.HY_BMS_Menu();
            TryUpdateModel(model);
            if (Context.Request.Form["IsHide"] == null)
            {
                model.IsHide = false;
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["MURL"]))
            {
                model.URL = Context.Request.Form["MURL"];
            }

            model.IsUse = true;

            Model.HY_BMS_MenuDesc desc = new Model.HY_BMS_MenuDesc();
            TryUpdateModel(desc);
            desc.spras = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_MenuDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_MenuDesc>();
            ////判断是否有同名的存在
            DataSet ds = blldesc.GetList(string.Format(" MenuName='{0}' AND spras={1} ", desc.MenuName, desc.spras));
            if (ds.Tables[0].Rows.Count == 0)
            {
                BLL.HY_BMS_Menu bll = BLLFactory.CreateInstance<BLL.HY_BMS_Menu>();
                bll.Add(model);
                blldesc.Add(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorSystemNameExist);
        }
    }
}