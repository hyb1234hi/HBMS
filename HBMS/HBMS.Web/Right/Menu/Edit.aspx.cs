﻿// 
// 创建人：宋欣
// 创建时间：2015-03-05
// 功能：菜单编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HBMS.Web.Right.Menu
{
    public partial class Edit : BasePage
    {
        /// <summary>
        ///     菜单ID
        /// </summary>
        public string MenuID { get; set; }

        /// <summary>
        ///     设备实体
        /// </summary>
        public Model.View_MenuDesc Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    this.MenuID = Request.QueryString["id"];
                    this.BindData();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        private void BindData()
        {
            BLL.View_MenuDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_MenuDesc>();
            DataSet ds =
                bll.GetList(string.Format(" MenuID='{0}' AND SPRAS={1} ", this.MenuID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_MenuDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
                if (!string.IsNullOrEmpty(Model.PMenuName))
                {
                    this.uctrPMenu.SelectedVal = Model.ParentID;
                }
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navMenu + " </a> >> " +
                                Resources.Nav.navMenuEdit);
        }
    }
}