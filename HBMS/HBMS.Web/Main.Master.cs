﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HBMS.Model;

namespace HBMS.Web
{
    public partial class Main : System.Web.UI.MasterPage
    {
        /// <summary>
        /// 当前页面路径
        /// </summary>
        public string CurPageLocation { get; set; }

        /// <summary>
        /// 登录用户的全局信息
        /// </summary>
        public CurrentUser GoalUserInfo { get { return GoalUserInfoConfig.GetGoalUserInfo(); } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              
            }
        }


        
    }
}