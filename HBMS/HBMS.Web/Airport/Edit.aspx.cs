﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：编辑机场
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using HBMS.Factory;
using HBMS.Model;
using Resources;

namespace HBMS.Web.Airport
{
    public partial class Edit : BasePage
    {
        public View_AirPortDesc Model;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.Params["id"]))
                {
                    this.BindData();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =  RebuildLocation("<a href=\"Index.aspx\" >" + Nav.navAirport + " </a> >> " +
                                          Nav.navAirportEdit);
        }

        private void BindData()
        {
            ////机场ID
            string airPortID = Request.Params["id"].Trim();
            BLL.View_AirPortDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_AirPortDesc>();
            DataSet ds =
                bll.GetList(string.Format(" AirPortID='{0}' AND SPRAS={1} ", airPortID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_AirPortDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
                this.uctrCounty.ShowAllOption = "1";
                this.uctrProvice.ShowAllOption = "1";
                this.uctrCity.ShowAllOption = "1";
                this.uctrCounty.SelectedVal = Model.CountryID;
                this.uctrProvice.CountryID = Model.CountryID;
                this.uctrProvice.SelectedVal = Model.ProvinceID;
                this.uctrCity.ProvinceID = Model.ProvinceID;
                this.uctrCity.SelectedVal = Model.CityID;
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }
    }
}