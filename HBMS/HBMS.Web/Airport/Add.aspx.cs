﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Airport
{
    public partial class Add : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                this.uctrCounty.ShowAllOption = "1";
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =  RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navAirport + " </a> >> " +
                                          Resources.Nav.navAirportAdd);
        }
    }
}