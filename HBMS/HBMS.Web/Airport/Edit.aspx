﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.Airport.Edit" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../CustomerControl/GeographicCtr/CountryCtr.ascx" TagName="CountryCtr" TagPrefix="uc1" %>
<%@ Register Src="../CustomerControl/GeographicCtr/ProvinceCtr.ascx" TagName="ProvinceCtr" TagPrefix="uc2" %>
<%@ Register Src="../CustomerControl/GeographicCtr/CityCtr.ascx" TagName="CityCtr" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $("#<%=this.uctrCounty.ClientControlID%>").change(function () {
                ////绑定国家下拉change事件
                Geographic.ChangeCountryGetProvince(this, "<%=this.uctrProvice.ClientControlID%>");
            });
            $("#<%=this.uctrProvice.ClientControlID%>").change(function () {
                ////绑定国家下拉change事件
                Geographic.ChangeProvinceGetCity(this, "<%=this.uctrCity.ClientControlID%>");
            });
        });
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
                if ($form.validationEngine('validate')) {
                    $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                        if (result.result == 1) {
                            MessageBox.success();
                        } else {
                            MessageBox.error(result.error);
                        }
                    }, "json");
                }
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table class="tb-form wp100">
                    <tbody>
                        <tr>
                            <td class="wp10 ">
                                <label class="require">机场编号:</label></td>
                            <td class="wp23">
                                <input type="hidden" name="AirPortID" value="<%=this.Model.AirPortID %>" />
                                <input type="text" name="AirPortCode" class="validate[required] input-text wp100" value="<%=this.Model.AirPortCode %>" />
                                
                            </td>
                            <td class="wp10 ">
                                <label class="require">机场名称:</label></td>
                            <td class="wp23">
                                <input type="text" name="AirPortName" class="validate[required] input-text wp100" value="<%=this.Model.AirPortName %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require">机场地址:</label></td>
                            <td class="wp23">
                                <input type="text" name="AirPortAddress" class="validate[required] input-text wp100" value="<%=this.Model.AirPortAddress %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCountry %>:</label></td>
                            <td class="wp23">
                                <uc1:CountryCtr ControlName="CountryID" ID="uctrCounty" runat="server" CSS="validate[required] select wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblProvince %>:</label></td>
                            <td class="wp23">
                                <uc2:ProvinceCtr ControlName="ProvinceID" ID="uctrProvice" runat="server" CSS="validate[required] select wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCity %>:</label></td>
                            <td class="wp23">
                                <uc3:CityCtr ControlName="CityID" ID="uctrCity" runat="server" CSS="validate[required] select wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblDimension %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="Dimension" class="validate[required]  input-text wp100" value="<%=this.Model.Dimension %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblLongitude %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="Longitude" class="validate[required] input-text wp100" value="<%=this.Model.Longitude %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require">负责人:</label></td>
                            <td class="wp23">
                                <input type="text" name="Head" class="validate[required] input-text wp100" value="<%=this.Model.Head %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require">负责人电话:</label></td>
                            <td class="wp23">
                                <input type="text" name="HeadPhone" class="validate[required] input-text wp100" value="<%=this.Model.HeadPhone %>" />
                                <td class="wp10 ">
                                    <label>负责人邮箱:</label></td>
                                <td class="wp23">
                                    <input type="text" name="HeadEmail" class="validate[custom[email]] input-text wp100" value="<%=this.Model.HeadEmail %>" />
                                </td>
                                <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="6" class="td_left">
                                <input type="button" class="btn btn82 btn_res" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
