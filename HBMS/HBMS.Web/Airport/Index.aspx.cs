﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.Airport
{
    public partial class Index : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =  RebuildLocation(Resources.Nav.navAirport);
        }
    }
}