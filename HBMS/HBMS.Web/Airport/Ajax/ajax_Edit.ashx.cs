﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.Airport.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////更新实体
            Model.HY_BMS_AirPorts model = new Model.HY_BMS_AirPorts();
            TryUpdateModel(model);
            Model.HY_BMS_AirPortDesc desc = new Model.HY_BMS_AirPortDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
            BLL.HY_BMS_AirPorts bll = BLLFactory.CreateInstance<BLL.HY_BMS_AirPorts>();
            BLL.HY_BMS_AirPortDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_AirPortDesc>();
            ////编辑验证其它机场名字是否有重复的
            DataSet dsDesc = blldesc.GetList(string.Format(" AirPortID<>'{0}' AND AirPortName='{1}' AND SPRAS={2}",
                                                           desc.AirPortID,
                                                           desc.AirPortName,
                                                           desc.SPRAS));
            if (dsDesc.Tables[0].Rows.Count == 0)
            {
                bll.Update(model);
                blldesc.Update(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorCodeExist);
        }
    }
}