﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：机场新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using HBMS.Model;
using Resources;

namespace HBMS.Web.Airport.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }
            
            ResponseWrite();
        }

        private string Add()
        {
            ////更新实体
            HY_BMS_AirPorts model = new HY_BMS_AirPorts();
            TryUpdateModel(model);
            HY_BMS_AirPortDesc desc = new HY_BMS_AirPortDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
            BLL.HY_BMS_AirPorts bll = BLLFactory.CreateInstance<BLL.HY_BMS_AirPorts>();
            BLL.HY_BMS_AirPortDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_AirPortDesc>();
            ////验证编号是否存在
            DataSet ds = bll.GetList(string.Format(" AirPortCode='{0}' ", model.AirPortCode));
            DataSet dsDesc = blldesc.GetList(string.Format(" AirPortName='{0}' AND SPRAS={1}",
                                                           desc.AirPortName,
                                                           desc.SPRAS));
            if (ds.Tables[0].Rows.Count == 0)
            {
                if (dsDesc.Tables[0].Rows.Count == 0)
                {
                    model.AirPortID=bll.Add(model);
                    desc.AirPortID = model.AirPortID;
                    blldesc.Add(desc);
                    return JsonSuccess();
                }
            }
            return JsonError(Msg.errorCodeExist);
        }
    }
}