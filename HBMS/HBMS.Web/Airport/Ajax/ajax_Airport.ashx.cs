﻿// 
// 创建人：宋欣
// 创建时间：2015-02-13
// 功能：机场查询
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.Airport.Ajax
{
    /// <summary>
    ///     ajax_Airport 的摘要说明
    /// </summary>
    public class ajax_Airport : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.QueryAccount();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string QueryAccount()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS='{0}'",this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["name"]))
            {
                ////机场编号或机场名称
                where.AppendFormat(" AND AirPortName LIKE '%{0}%'",
                                   Context.Request.Params["name"].FilterQueryCondition());
            }
            BLL.View_AirPortDesc bll = BLLFactory.CreateInstance<BLL.View_AirPortDesc>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " AirPortID ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_AirPortDesc> list = bll.DataTableToList(ds.Tables[0]);

                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                        {
                            data = from l in list
                                   select new
                                       {
                                           l.AirPortID,
                                           l.AirPortCode,
                                           l.SPRAS,
                                           l.AirPortName,
                                           l.AirPortAddress,
                                           l.CityID,
                                           l.CityName,
                                           l.CountryID,
                                           l.CountryName,
                                           l.ProvinceID,
                                           l.ProvinceName,
                                           l.Longitude,
                                           l.Dimension,
                                           l.Head,
                                           l.HeadEmail,
                                           l.HeadPhone
                                       },
                            totalCount = Pager.TotalCount,
                            totalPages = Pager.TotalPage,
                            visiblePages = Pager.VisiblePages,
                            startPage = Pager.PageIndex
                        }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}