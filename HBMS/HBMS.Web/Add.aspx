﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.Account.Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>

                <table class="tb-form wp89">
                    <tbody>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">用户编号:</label></td>
                            <td class="wp33">
                                <input type="text" name="name" class="input-text wp100" />
                            </td>
                            <td class="wp10 td_right">
                                <label class="require">用户名:</label></td>
                            <td class="wp33">
                                <input type="text" name="name" class="input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">姓名:</label></td>
                            <td class="wp33">
                                <input type="text" name="name" class="input-text wp100" />
                            </td>
                            <td class="wp10 td_right">
                                <label class="require">性别:</label></td>
                            <td class="wp33">
 
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">手机:</label></td>
                            <td class="wp33">
                                <input type="text" name="name" class="input-text wp100" />
                            </td>
                            <td class="wp10 td_right">
                                <label class="require">邮箱:</label></td>
                            <td class="wp33">
 <input type="text" name="name" class="input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">语言:</label></td>
                            <td class="wp33">
                                <input type="text" name="name" class="input-text wp100" />
                            </td>
                            <td class="wp10 td_right">
                                <label class="require">默认机场:</label></td>
                            <td class="wp33">
 <input type="text" name="name" class="input-text wp100" />
                            </td>
                        </tr>
                        
                        用户编号：*		用户名：*		姓名：*	
性别：*		手机：*		邮箱	
语言：*	中文	默认语言：*	中文	默认机场	
是否启用：*					

                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">下拉框:</label></td>
                            <td class="wp89">
                                <select name="" class="select wp43">
                                    <option>北京</option>
                                    <option>天津</option>
                                    <option>上海</option>
                                    <option>重庆</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">文本框:</label></td>
                            <td class="wp89">
                                <textarea name="" id="" cols="30" rows="10" class="textarea"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">单选:</label></td>
                            <td class="wp89">
                                <input type="radio" name="status" />
                                可用
                    <input type="radio" name="status" />
                                不可用
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">多选:</label></td>
                            <td class="wp89">
                                <input type="checkbox" name="check" />
                                1
                    <input type="checkbox" name="check" />
                                2
                    <input type="checkbox" name="check" />
                                3
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 td_right">
                                <label class="require">文件:</label></td>
                            <td class="wp89">
                                <input type="file" name="file" class="input-text wp43" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" name="button" class="btn btn82 btn_save2" value="保存" />
                                <input type="button" name="button" class="btn btn82 btn_res" value="重置" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
