﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountryCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.GeographicCtr.CountryCtr" %>
<asp:DropDownList ID="ddlCountry" runat="server"></asp:DropDownList>
<script >
    if ('<%=this.ControlName%>' != "") {
        $("#<%=this.ddlCountry.ClientID%>").attr("name", "<%=this.ControlName%>");
    }
</script>