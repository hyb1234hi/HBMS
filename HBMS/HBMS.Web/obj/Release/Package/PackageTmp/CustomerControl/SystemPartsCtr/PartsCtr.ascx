﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartsCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.SystemPartsCtr.PartsCtr" %>
<asp:DropDownList ID="ddlParts" runat="server"></asp:DropDownList>
<script >
    if ('<%=this.ControlName%>' != "") {
        $("#<%=this.ddlParts.ClientID%>").attr("name", "<%=this.ControlName%>");
    }
</script>