﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParamsMsgCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.SystemPartsCtr.ParamsMsgCtr" %>
<%@ Import Namespace="HBMS.Common" %>
<select id="<%=this.ControlID%>" class="<%=CSS %>" name="<%=ControlName %>" onchange="<%=this.OnClientChange %>" >
    <option value="" selected="selected"></option>
    <option value="<%=(int)ParamMsgFWEnum.全部 %>" ><%=Resources.Lbl.lblQuanBu %></option>
    <option value="<%=(int)ParamMsgFWEnum.本设备 %>"><%=Resources.Lbl.lblParamsMsgFWSB %></option>
</select>
<script>
    var selectedVal = '<%=this.SelectedVal%>';
    if (selectedVal != null) {
        $("#<%=this.ControlID%>").find("option[value='" + selectedVal + "']").attr("selected", "selected");
    }
</script>