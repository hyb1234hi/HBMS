﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParamType.ascx.cs" Inherits="HBMS.Web.CustomerControl.SystemPartsCtr.ParamType" %>
<%@ Import Namespace="HBMS.Common" %>
<select id="<%=this.ControlID %>" class="select" name="<%=ControlName %>" onchange="<%=this.OnClientChange %>">
    <option value="<%=(int)ParamTypeEnum.数值 %>" selected="selected"><%=Resources.Lbl.lblParamsNumberType %></option>
    <option value="<%=(int)ParamTypeEnum.布尔 %>"><%=Resources.Lbl.lblParamsBooleanType %></option>
</select>
<script>
    var selectedVal = '<%=this.SelectedVal%>';
    if (selectedVal != null) {
        $("#<%=this.ControlID%>").find("option[value='" + selectedVal + "']").attr("selected", "selected");
    }
</script>