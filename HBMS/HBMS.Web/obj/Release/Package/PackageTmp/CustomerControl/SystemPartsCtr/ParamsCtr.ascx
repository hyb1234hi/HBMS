﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParamsCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.SystemPartsCtr.ParamsCtr" %>
<asp:DropDownList ID="ddlParams" runat="server"></asp:DropDownList>
<script >
    if ('<%=this.ControlName%>' != "") {
        $("#<%=this.ddlParams.ClientID%>").attr("name", "<%=this.ControlName%>");
    }
</script>