﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Params.aspx.cs" Inherits="HBMS.Web.Map.Bai.Params" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function() {
            query(1);
        });
       
        function query(page) {
            $("#tbResult").NewDataTable({
                initComplete: function (settings, json) {
                    //////表格加载完后出发事件
                    if (json.data != undefined && json.data != "" && json.data.length > 0) {
                        $('#pagination').twbsPagination({
                            totalPages: json.totalPages,        /////总页数
                            visiblePages: json.visiblePages,    /////最大可显示几页
                            startPage: json.startPage,           ////当前页
                            onPageClick: function (event, page) { ////分页按钮点击事件
                                query(page);
                            },
                            refresh: 'query(1)'         /////刷新按钮
                        });
                        $("#pagination-message").empty().append(showPaginationMsg(json.startPage, json.totalPages, json.totalCount));
                    } else {
                        $("#pagination").twbsPaginationEmpty();
                        $("#pagination-message").twbsPaginationMsgEmpty();
                    }
                },
                ajax: {
                    url: "Ajax/ajax_Params.ashx",   ////请求后台
                    type: "post",
                    data: function (d) { ////查询条件
                        d.action = "query";
                        d.aid = "<%=this.AID%>";
                        d.eid = "<%=this.EID%>";
                        d.page = page; ////页索引
                    },
                },
                columns: [////绑定每列
                    { data: "TagName" },
                    { data: "ParamName" },
                    { data: "ParamValue" },
                    { data: "DisplayParamValueDesc" },
                    { data: "ParamStateDesc" },
                ]
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box span10">
        <table id="tbResult" class="tb-data">
            <thead>
                <tr>
                    <th><%=Resources.Lbl.lblParamsCode %></th>
                    <th><%=Resources.Lbl.lblParamsName %></th>
                    <th><%=Resources.Lbl.lblParamsValueSJ %></th>
                    <th><%=Resources.Lbl.lblParamsValue %></th>
                    <th><%=Resources.Lbl.lblState %></th>
                </tr>
            </thead>
        </table>
        <div class="block-pagination">
            <ul id="pagination-message" class="controls-message"></ul>
            <ul id="pagination" class="controls-buttons"></ul>
        </div>
    </div>
</asp:Content>
