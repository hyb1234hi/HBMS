﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HBMS.Web.Map.Bai.Index" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../../CustomerControl/MapCtr.ascx" TagName="MapCtr" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="/script/lib/jquery.scroll.js"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=1.5&ak=4bd565aa6eefd9af9af1f27bba521e32"></script>
    <link href="/css/map.css" rel="stylesheet" />
    <script>
        function displayfull() {
           // window.open('Full.aspx', '全屏显示', 'width=' + screen.width + ',height=' + screen.height + ',alwaysRaised=yes,toolbar=yes,menubar=no,location=no,channelmode=yes,fullscreen=yes');
            window.open("Full.aspx", "_blank", 'width=' + screen.width + ',height=' + screen.height + ',alwaysRaised=yes,toolbar=yes,menubar=no,location=no,channelmode=yes,fullscreen=yes,titlebar=no')
            //window.open("Full.aspx", "_blank", "fullscreen=yes,location=no,menubar=no,resizable=no,scrollbars=no,status=no,toolbar=no,titlebar=no");
        }
        
        //var intervalalter = setInterval(alertWarn, 30000);

        //function stopget() {
        //    clearInterval(intervalalter);
        //}
        
        //function alertWarn() {
        //    $.post("Ajax/ajax_AlterWarn.ashx", function (result) {
        //        if (result != undefined && result != "") {
        //            var html = '';
        //            html = '<ul class="alterwarn">';
        //            for (var i in result) {
        //                html += '<li>';
        //                html += result[i].AirPortName;
        //                html += result[i].ParamName + '<span class="paramval">(' + result[i].ParamValue + ')</span>';
        //                html += result[i].ParamStateDesc;
        //                html += '</li>';
        //            }
        //            html += '</ul>';
        //            MessageBox.dialog({
        //                id: 'alterWarn',
        //                padding: 0,
        //                title: '警告',
        //                content: html,
        //                left: '100%',
        //                top: '100%',
        //                fixed: true,
        //                drag: false,
        //                resize: false,
        //                time:10
        //            });
        //        }
                
        //    }, "json");

        //}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <a href="javascript:void(0)" onclick="displayfull();"><%=Resources.Btn.btnFullScreen %></a>
    <div style="height: 600px;width: 100%">
        <uc1:MapCtr ID="MapCtr1" runat="server" />
    </div>
    
</asp:Content>
