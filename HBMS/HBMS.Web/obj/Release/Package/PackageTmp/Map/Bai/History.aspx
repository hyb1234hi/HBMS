﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="HBMS.Web.Map.Bai.History" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="~/CustomerControl/AirportCtr/AirportNameCtr.ascx" TagName="AirportNameCtr" TagPrefix="uc1" %>
<%@ Register Src="~/CustomerControl/EquipmentCtr/EquipementNameCtr.ascx" TagName="EquipementNameCtr" TagPrefix="uc2" %>
<%@ Register Src="~/CustomerControl/SystemPartsCtr/SystemCtr.ascx" TagName="SystemCtr" TagPrefix="uc3" %>
<%@ Register Src="~/CustomerControl/SystemPartsCtr/PartsCtr.ascx" TagName="PartsCtr" TagPrefix="uc4" %>
<%@ Register src="~/CustomerControl/SystemPartsCtr/ParamsCtr.ascx" tagname="ParamsCtr" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $("#<%=this.uctrAirport.ClientControlID%>").change(function () {
                 AirPort.ChangeAirportGetEquipment(this, "<%=this.uctrEquipment.ClientControlID%>");
             });
             $("#<%=this.uctrEquipment.ClientControlID%>").change(function () {
                 AirPort.ChangeEquipmentGetSystem(this, "<%=this.uctrSystem.ClientControlID%>");
            });
             $("#<%=this.uctrSystem.ClientControlID%>").change(function () {
                 SystemParts.ChangeSystemGetParts(this, "<%=this.uctrParts.ClientControlID%>");
             });
            $("#<%=this.uctrParts.ClientControlID%>").change(function () {
                ////绑定部件下拉change事件
                SystemParts.ChangePartsGetParams(this, "<%=this.uctrParams.ClientControlID%>");
            });
             query(1);
         });
        function GetOptionHtml(row) {
            var optionHtml = "";
            optionHtml += '<a target="_blank" href="Info.aspx?id=' + row.ID + '" ><img src="/images/information-blue.png" width="16" height="16"></a>';
            return optionHtml;
        }

        function query(page) {
            $("#tbResult").NewDataTable({
                initComplete: function (settings, json) {
                    //////表格加载完后出发事件
                    if (json.data != undefined && json.data != "" && json.data.length > 0) {
                        $('#pagination').twbsPagination({
                            totalPages: json.totalPages,        /////总页数
                            visiblePages: json.visiblePages,    /////最大可显示几页
                            startPage: json.startPage,           ////当前页
                            onPageClick: function (event, page) { ////分页按钮点击事件
                                query(page);
                            },
                            refresh: 'query(1)'         /////刷新按钮
                        });
                        $("#pagination-message").empty().append(showPaginationMsg(json.startPage, json.totalPages, json.totalCount));
                    } else {
                        $("#pagination").twbsPaginationEmpty();
                        $("#pagination-message").twbsPaginationMsgEmpty();
                    }
                },
                ajax: {
                    url: "Ajax/ajax_History.ashx",   ////请求后台
                    type: "post",
                    data: function (d) { ////查询条件
                        d.action = "query";
                        d.airport = $("#<%=this.uctrAirport.ClientControlID%>").val();////机场名称
                        d.equipment = $("#<%=this.uctrEquipment.ClientControlID%>").val();////设别名称
                        d.system = $("#<%=this.uctrSystem.ClientControlID%>").val();////系统名称
                        d.parts = $("#<%=this.uctrParts.ClientControlID%>").val();////部件名称
                        d.param = $("#<%=this.uctrParams.ClientControlID%>").val(); ////参数名称
                        d.page = page; ////页索引
                    },
                },
                columns: [////绑定每列
                    { data: "ID" },
                    { data: "AirPortName" },
                    { data: "EquipmentName" },
                    { data: "SystemName" },
                    { data: "ParamName" },
                    { data: "ParamValue" },
                    { data: "SubDatetime" },
                    { data: "ParamStateDesc" }
                ]
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div class="box_top"><b class="pl15"><%=Resources.Lbl.lblQueryCondition %></b></div>
            <div class="pt10 pb10">
                <table class="tb-form">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblAirportName %>:</label>:</td>
                            <td class="wp23">

                                <uc1:AirportNameCtr ID="uctrAirport" ControlName="AirPortID" CSS="select " runat="server" />

                            </td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblEquipmentName %></label>
                            </td>
                            <td class="wp23">

                                <uc2:EquipementNameCtr ID="uctrEquipment" ControlName="EquipmentID" CSS="select " ShowAllOption="0" runat="server" />

                            </td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">

                                <uc3:SystemCtr ID="uctrSystem" ControlName="SystemID" CSS="select" ShowAllOption="0" runat="server" />

                            </td>
                        </tr>
                        <tr>

                            <td class="wp10">
                                <label><%=Resources.Lbl.lblPartsName %>:</label>:</td>
                            <td class="wp23">
                                <uc4:PartsCtr ID="uctrParts" ControlName="PartsID" CSS="select " ShowAllOption="0" runat="server" />

                            </td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblParamsName %>:</label></td>
                            <td class="wp23">
                                <uc1:ParamsCtr ID="uctrParams" ShowAllOption="0" runat="server" />
                            </td>
                            <td class="wp10"></td>
                            <td class="wp23"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-box ">
                <input type="button" class="btn btn82 btn_search" value="<%=Resources.Btn.btnQuery %>" onclick="query(1)" />
                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
            </div>
        </div>
        <div class="box span10">
            <table id="tbResult" class="tb-data">
                <thead>
                    <tr>
                        <th><%=Resources.Lbl.lblCode %></th>
                        <th><%=Resources.Lbl.lblAirportName %></th>
                        <th><%=Resources.Lbl.lblEquipmentName %></th>
                        <th><%=Resources.Lbl.lblSystemName %></th>
                        <th><%=Resources.Lbl.lblParamsName %></th>
                        <th><%=Resources.Lbl.lblParamsValue %></th>
                        <th><%=Resources.Lbl.lblDate %></th>
                        <th><%=Resources.Lbl.lblState %></th>
                    </tr>
                </thead>
            </table>
            <div class="block-pagination">
                <ul id="pagination-message" class="controls-message"></ul>
                <ul id="pagination" class="controls-buttons"></ul>
            </div>
        </div>
    </div>
</asp:Content>
