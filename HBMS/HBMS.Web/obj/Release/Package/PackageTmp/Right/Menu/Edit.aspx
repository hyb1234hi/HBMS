﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.Right.Menu.Edit" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../../CustomerControl/MenuCtr.ascx" TagName="MenuCtr" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
                if ($form.validationEngine('validate')) {
                    $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                        if (result.result == 1) {
                            MessageBox.success();
                        } else {
                            MessageBox.error(result.error);
                        }
                    }, "json");
                }
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCode %>:</label></td>
                            <td class="wp23">
                                <input type="hidden" name="MenuID" value="<%=Model.MenuID %>" />
                                <%=Model.MenuID %>
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblMenuName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="MenuName" value="<%=Model.MenuName %>" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblParentMenu %>:</label></td>
                            <td class="wp23">
                                <uc1:MenuCtr ID="uctrPMenu" ShowAllOption="1" ControlName="ParentID" CSS="select wp100" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblLink %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="MURL" value="<%=Model.URL %>" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" value="<%=Model.OrderIndex %>" class="validate[required] input-text wp100" />
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label><%=Resources.Lbl.lblHidden %>:</label>
                                <% if (Model.IsHide)
                                   { %>

                                <input type="checkbox" value="true" name="IsHide" checked="checked" onclick="ChangeChkboxVal(this)" />

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsHide" onclick="ChangeChkboxVal(this)" />
                                <% } %>
                               
                            </td>
                            <td colspan="2">
                                <label><%=Resources.Lbl.lblUse %>:</label>
                                <% if (Model.IsUse)
                                   { %>

                                <input type="checkbox" value="true" name="IsUse" checked="checked" onclick="ChangeChkboxVal(this)" />

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsUse" onclick="ChangeChkboxVal(this)" />
                                <% } %>
                               
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea rows="5" class="input-text wp100" name="Memo">
                                <%=Model.Memo.Trim() %>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
