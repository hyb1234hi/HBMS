﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.Right.Menu.Add" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register src="../../CustomerControl/MenuCtr.ascx" tagname="MenuCtr" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Add.ashx", $form.formSerialize(), function(result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="add" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCode %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="MenuID" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblMenuName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="MenuName" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblParentMenu %>:</label></td>
                            <td class="wp23">
                                <uc1:MenuCtr ID="uctrPMenu" ShowAllOption="1" ControlName="ParentID" CSS="select wp100" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblLink %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="MURL" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" class="validate[required] input-text wp100" />
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label><%=Resources.Lbl.lblHidden %>:</label>
                                <input type="checkbox" value="true" name="IsHide" onclick="ChangeChkboxVal(this)" />
                            </td>
                            <td colspan="4"></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea rows="5" class="input-text wp100" name="Memo"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
