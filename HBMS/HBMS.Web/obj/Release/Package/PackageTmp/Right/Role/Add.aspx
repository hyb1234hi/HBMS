﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.Right.Role.Add" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Add.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="add" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td style="width: 13%;">
                                <label class="require"><%=Resources.Lbl.lblRole %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="RoleName" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp33 " colspan="2">
                                <label><%=Resources.Lbl.lblIsAdmin %>:</label>
                                <input type="checkbox" value="true" name="IsAdmin" onclick="ChangeChkboxVal(this)" />
                            </td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
