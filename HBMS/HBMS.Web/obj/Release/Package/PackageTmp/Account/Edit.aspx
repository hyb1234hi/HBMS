﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.Account.Edit" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../CustomerControl/AccountCtr/SexCtr.ascx" TagName="SexCtr" TagPrefix="uc1" %>
<%@ Register Src="../CustomerControl/LanguageCtr.ascx" TagName="LanguageCtr" TagPrefix="uc2" %>
<%@ Register Src="../CustomerControl/AirportCtr/AirportNameCtr.ascx" TagName="AirportNameCtr" TagPrefix="uc3" %>

<%@ Register Src="../CustomerControl/AccountCtr/RoleCtr.ascx" TagName="RoleCtr" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>
                            <td class="wp10 ">
                                <label class="require">用户编号:</label></td>
                            <td class="wp23">
                                <input type="text" name="UserCode" class="validate[required]  input-text wp100" value="<%=this.Model.UserCode %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require">用户名:</label></td>
                            <td class="wp23">
                                <input type="text" name="UserName" class="validate[required] input-text wp100" value="<%=this.Model.UserName %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require">姓名:</label></td>
                            <td class="wp23">
                                <input type="text" name="CName" class="validate[required] input-text wp100" value="<%=this.Model.CName %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require">性别:</label></td>
                            <td class="wp23">
                                <uc1:SexCtr ID="ucrSex" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require">手机:</label></td>
                            <td class="wp23">
                                <input type="text" name="PhoneNo" class="validate[required] input-text wp100" value="<%=this.Model.PhoneNo %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require">邮箱:</label></td>
                            <td class="wp23">
                                <input type="text" name="Email" class="validate[required,custom[email]] input-text wp100" value="<%=this.Model.Email %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require">语言:</label></td>
                            <td class="wp23">
                                <uc2:LanguageCtr ID="ucrLang" runat="server" />
                            </td>
                           
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblRole %>:</label></td>
                            <td class="wp23">

                                <uc4:RoleCtr ID="uctrRole" runat="server" ControlName="RoleID" CSS="select validate[required]" />

                            </td>
                             <td class="wp10 ">
                                <label><%=Resources.Lbl.lblDefaultAirport %>:</label></td>
                            <td class="wp23">
                                <uc3:AirportNameCtr ID="uctrAirport" ControlName="DefaultAirPortID" CSS="select" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <label><%=Resources.Lbl.lblUse %>:</label>
                                <% if (Model.IsUse)
                                   { %>

                                <input type="checkbox" value="true" name="IsUse" checked="checked" onclick="ChangeChkboxVal(this)" />

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsUse" onclick="ChangeChkboxVal(this)" />
                                <% } %>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
