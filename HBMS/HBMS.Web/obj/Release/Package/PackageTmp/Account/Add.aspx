﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.Account.Add" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../CustomerControl/AccountCtr/SexCtr.ascx" TagName="SexCtr" TagPrefix="uc1" %>
<%@ Register Src="../CustomerControl/LanguageCtr.ascx" TagName="LanguageCtr" TagPrefix="uc2" %>

<%@ Register Src="../CustomerControl/AirportCtr/AirportNameCtr.ascx" TagName="AirportNameCtr" TagPrefix="uc3" %>

<%@ Register Src="../CustomerControl/AccountCtr/RoleCtr.ascx" TagName="RoleCtr" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Add.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="add" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblUserCode %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="UserCode" class="validate[required]  input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblUserName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="UserName" class="validate[required] input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblUserXingMing %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="CName" class="validate[required] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblSex%>:</label></td>
                            <td class="wp23">
                                <uc1:SexCtr ID="ucrSex" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblMobilePhone %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="PhoneNo" class="validate[required] input-text wp100" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblEmail %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="Email" class="validate[required,custom[email]] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblDefaultSpars %>:</label></td>
                            <td class="wp23">
                                <uc2:LanguageCtr ID="ucrLang" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblRole %>:</label></td>
                            <td class="wp23">

                                <uc4:RoleCtr ID="uctrRole" runat="server" ControlName="RoleID" CSS="select validate[required]" />

                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblDefaultAirport %>:</label></td>
                            <td class="wp23">
                                <uc3:AirportNameCtr ID="uctrAirport" ControlName="DefaultAirPortID" CSS="select" runat="server" />
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
