﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HBMS.Web.SystemPart.Params.Index" %>
<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register src="../../CustomerControl/SystemPartsCtr/SystemCtr.ascx" tagname="SystemCtr" tagprefix="uc2" %>
<%@ Register src="../../CustomerControl/UseStateCtr.ascx" tagname="UseStateCtr" tagprefix="uc1" %>
<%@ Register src="../../CustomerControl/SystemPartsCtr/PartsCtr.ascx" tagname="PartsCtr" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $("#<%=this.uctrSystem.ClientControlID%>").change(function() {
                ////绑定系统下拉change事件
                SystemParts.ChangeSystemGetParts(this, "<%=this.uctrParts.ClientControlID%>");
            });
            query(1);
        });
        function GetOptionHtml(row) {
            var optionHtml = "";
            optionHtml += '<a target="_blank" href="Edit.aspx?id=' + row.ParamID + '" ><img src="/images/pencil.png" width="16" height="16"></a>';
            return optionHtml;
        }

        function query(page) {
            $("#tbResult").NewDataTable({
                initComplete: function (settings, json) {
                    //////表格加载完后出发事件
                    if (json.data != undefined && json.data != "" && json.data.length > 0) {
                        $('#pagination').twbsPagination({
                            totalPages: json.totalPages,        /////总页数
                            visiblePages: json.visiblePages,    /////最大可显示几页
                            startPage: json.startPage,           ////当前页
                            onPageClick: function (event, page) { ////分页按钮点击事件
                                query(page);
                            },
                            refresh: 'query(1)'         /////刷新按钮
                        });
                        $("#pagination-message").empty().append(showPaginationMsg(json.startPage, json.totalPages, json.totalCount));
                    } else {
                        $("#pagination").twbsPaginationEmpty();
                        $("#pagination-message").twbsPaginationMsgEmpty();
                    }
                },
                ajax: {
                    url: "Ajax/ajax_Index.ashx",   ////请求后台
                    type: "post",
                    data: function (d) { ////查询条件
                        d.action = "query";
                        d.system = $("#<%=this.uctrSystem.ClientControlID%>").val(),
                        d.parts = $("#<%=this.uctrParts.ClientControlID%>").val(),
                        d.name = $("#txtParamName").val();
                        d.use = $("#ddlUseState").val();
                        d.page = page; ////页索引
                    },
                },
                columns: [////绑定每列
                    { data: "ParamCode" },
                    { data: "SystemName" },
                    { data: "PartsName" },
                    { data: "ParamName" },
                    { data: "ParamTypeDesc" },
                    { data: "OrderIndex" },
                    { data: "IsUseDesc" },
                    {
                        data: function (row, type, set, meta) {
                            return GetOptionHtml(row);
                        }
                    }
                ]
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div class="box_top"><b class="pl15"><%=Resources.Lbl.lblQueryCondition %></b></div>
            <div class="pt10 pb10">
                <table class="tb-form">
                    <tbody>
                        <tr>
                             
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <uc2:SystemCtr ID="uctrSystem" CSS="select" runat="server"  ShowAllOption="1" />
                           </td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <uc3:PartsCtr ID="uctrParts" runat="server" />
                            </td>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblParamsName %>:</label></td>
                            <td class="wp23">
                                <input type="text" id="txtParamName" class="input-text" />
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblUseState %>:</label>
                            </td>
                            <td class="wp23">
                                <uc1:UseStateCtr ID="uctrUseState" ControlID="ddlUseState" runat="server" />
                            </td>
                            <td colspan="4"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-box ">
                <input type="button" class="btn btn82 btn_search" value="<%=Resources.Btn.btnQuery %>" onclick="query(1)" />
                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                <a class="btn btn82 btn_add" href="Add.aspx"><%=Resources.Btn.btnAdd %></a>
            </div>
        </div>
        <div class="box span10">
            <table id="tbResult" class="tb-data">
                <thead>
                    <tr>
                        <th><%=Resources.Lbl.lblCode %></th>
                        <th><%=Resources.Lbl.lblSystemName %></th>
                        <th><%=Resources.Lbl.lblPartsName %></th>
                        <th><%=Resources.Lbl.lblParamsName %></th>
                        <th><%=Resources.Lbl.lblParamsType %></th>
                        <th><%=Resources.Lbl.lblOrderIndex %></th>
                        <th><%=Resources.Lbl.lblUseState %></th>
                        <th><%=Resources.Lbl.lblOperation %></th>
                    </tr>
                </thead>
            </table>
            <div class="block-pagination">
                <ul id="pagination-message" class="controls-message"></ul>
                <ul id="pagination" class="controls-buttons"></ul>
            </div>
        </div>
    </div>
</asp:Content>
