﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.SystemPart.Equipment.Edit" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/SystemCtr.ascx" TagName="SystemCtr" TagPrefix="uc2" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/PartsCtr.ascx" TagName="PartsCtr" TagPrefix="uc3" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/ParamsCtr.ascx" TagName="ParamsCtr" TagPrefix="uc1" %>
<%@ Register Src="../../CustomerControl/EquipmentCtr/EquipementNameCtr.ascx" TagName="EquipementNameCtr" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $("#<%=this.uctrSystem.ClientControlID%>").change(function () {
                ////绑定系统下拉change事件
                SystemParts.ChangeSystemGetParts(this, "<%=this.uctrParts.ClientControlID%>");
            });
            $("#<%=this.uctrParts.ClientControlID%>").change(function () {
                ////绑定部件下拉change事件
                SystemParts.ChangePartsGetParams(this, "<%=this.uctrParams.ClientControlID%>");
            });
        });
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label><%=Resources.Lbl.lblCode %>:</label></td>
                            <td class="wp23">
                                <%= Model.ID %>
                                <input type="hidden" name="ID" value="<%= Model.ID %>" />
                            </td>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <uc2:SystemCtr ID="uctrSystem" runat="server" ShowAllOption="1" />
                            </td>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <uc3:PartsCtr ID="uctrParts" ShowAllOption="0" runat="server" />
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblParamsName %>:</label></td>
                            <td class="wp23">
                                <uc1:ParamsCtr ID="uctrParams" ShowAllOption="0" runat="server" />
                            </td>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblEquipmentName %>:</label>
                            </td>
                            <td class="wp23">
                                <uc4:EquipementNameCtr ID="uctrEquipment" runat="server" ShowAllOption="1" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" value="<%=Model.OrderIndex %>" class="validate[required] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
