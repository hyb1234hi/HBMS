﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.SystemPart.SystemA.Edit" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
             if ($form.validationEngine('validate')) {
                 $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                     if (result.result == 1) {
                         MessageBox.success();
                     } else {
                         MessageBox.error(result.error);
                     }
                 }, "json");
             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td class="wp10">
                                <label><%=Resources.Lbl.lblCode %>:</label></td>
                            <td class="wp23">
                                <%= Model.SystemID %>
                                <input type="hidden" name="SystemID" value="<%= Model.SystemID %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="SystemName" class="validate[required] input-text wp100" value="<%= Model.SystemName %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" class="validate[required] input-text wp100" value="<%= Model.OrderIndex %>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <label><%=Resources.Lbl.lblUse %>:</label>
                                <% if (Model.IsUse)
                                   { %>

                                <input type="checkbox" value="true" name="IsUse" checked="checked" onclick="ChangeChkboxVal(this)" />

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsUse" onclick="ChangeChkboxVal(this)" />
                                <% } %>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
