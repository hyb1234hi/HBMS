﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.SystemPart.Parts.Edit" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register src="../../CustomerControl/SystemPartsCtr/SystemCtr.ascx" tagname="SystemCtr" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script>
            function add() {
                $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td class="wp10">
                                <label><%=Resources.Lbl.lblCode %>:</label></td>
                            <td class="wp23">
                                <%= Model.PartsID %>
                                <input type="hidden" name="PartsID" value="<%= Model.PartsID %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="PartsName" class="validate[required] input-text wp100" value="<%= Model.PartsName %>" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <uc1:SystemCtr ID="uctrSystemID" ShowAllOption="1"  ControlName="SystemID" runat="server" CSS="validate[required] select wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" class="validate[required] input-text wp100" value="<%= Model.OrderIndex %>" />
                            </td>
                            <td colspan="4"></td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <label><%=Resources.Lbl.lblUse %>:</label>
                                <% if (Model.IsUse)
                                   { %>

                                <input type="checkbox" value="true" name="IsUse" checked="checked" onclick="ChangeChkboxVal(this)" />

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsUse" onclick="ChangeChkboxVal(this)" />
                                <% } %>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
