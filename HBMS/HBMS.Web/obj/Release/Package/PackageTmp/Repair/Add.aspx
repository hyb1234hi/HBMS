﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.Repair.Add" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../CustomerControl/AirportCtr/AirportNameCtr.ascx" TagName="AirportNameCtr" TagPrefix="uc1" %>
<%@ Register Src="../CustomerControl/EquipmentCtr/EquipementNameCtr.ascx" TagName="EquipementNameCtr" TagPrefix="uc2" %>
<%@ Register Src="../CustomerControl/SystemPartsCtr/SystemCtr.ascx" TagName="SystemCtr" TagPrefix="uc3" %>
<%@ Register Src="../CustomerControl/SystemPartsCtr/PartsCtr.ascx" TagName="PartsCtr" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #ulfile {
            
        }

        #ulfile li {
            line-height: 22px;
        }

        #ulfile li.file {
            padding: 3px;
        } 
    </style>
    <script src="/script/ajaxfileupload.js"></script>
    <script>
        $(function () {
            $("#<%=this.uctrAirport.ClientControlID%>").change(function () {
                AirPort.ChangeAirportGetEquipment(this, "<%=this.uctrEquipment.ClientControlID%>");
            });
            $("#<%=this.uctrEquipment.ClientControlID%>").change(function () {
                AirPort.ChangeEquipmentGetSystem(this, "<%=this.uctrSystem.ClientControlID%>");
            });
            $("#<%=this.uctrSystem.ClientControlID%>").change(function () {
                SystemParts.ChangeSystemGetParts(this, "<%=this.uctrParts.ClientControlID%>");
            });
        });

        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                var data = $form.formSerialize();
                var rfiles = new Array();
                if ($("#ulfile .file").length > 0) {
                    $("#ulfile .file").each(function() {
                        rfiles.push({ name: $(this).attr("data-attaname"), path: $(this).attr("data-attapath") });
                    });
                    data = data + "&rfiles=" + JSON.stringify(rfiles);
                }
                
                $.post("Ajax/ajax_Add.ashx", data, function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }

        function delfile(obj) {
            $(obj).parent().remove();
        }

        function uploadfile() {
            $.ajaxFileUpload(
                {
                    url: '/CommonAjax/ajax_File.ashx',
                    secureuri: false,
                    fileElementId: 'file1',
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.result == "1") {
                            var AttaName = data.filename; //附件名称
                            var AttaPath = data.path; //存放路径
                            var html = '<li class="file" data-attaname="' + AttaName + '" data-attapath="' + AttaPath + '">' +
                                '<a target="_blank" href="' + AttaPath + '">' + AttaName + '</a>' +
                                '<img style="margin-left:10px;" src="/images/del.png" onclick="delfile(this)"  />' +
                                '</li>';
                            $first_li = $("#ulfile").find("li").eq(0);
                            $first_li.nextAll().remove();
                            $first_li.after(html);
                        } else {
                            MessageBox.error(data.error);
                        }
                    },
                    error: function (data, status, e) {
                        MessageBox.error(e);
                    }
                }
            );
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="add" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td style="width: 13%;">
                                <label class="require"><%=Resources.Lbl.lblRepairCode %>:</label></td>
                            <td class="wp23">
                                <input type="hidden" name="ID" value="<%=this.RepairCode %>" />
                                <%=this.RepairCode %>
                            </td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblAirportName %>:</label></td>
                            <td class="wp23">
                                <uc1:AirportNameCtr ID="uctrAirport" ControlName="AirPortID" CSS="select validate[required] " runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblEquipmentName %>:</label></td>
                            <td class="wp23">
                                <uc2:EquipementNameCtr ID="uctrEquipment" ControlName="EquipmentID" CSS="select validate[required] " ShowAllOption="0" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <uc3:SystemCtr ID="uctrSystem" ControlName="SystemID" CSS="select validate[required] " ShowAllOption="0" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <uc4:PartsCtr ID="uctrParts" ControlName="PartsID" CSS="select validate[required] " ShowAllOption="0" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblRepairFZR %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="RepairUse" class="validate[required] input-text " />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblCheckFZR %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="CheckUser" class="validate[required] input-text " />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblRepairDate %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="RepairDate" class="validate[required] input-text Wdate" onclick="WdatePicker()" />
                            </td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRepairDescription %>:</label></td>
                            <td colspan="5">
                                <textarea name="Contect" class="wp100" rows="5"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea name="memo" class="wp100" rows="5"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <h5><%=Resources.Lbl.lbFiles %>:</h5>
                                <ul id="ulfile">
                                    <li>
                                        <input id="file1" name="file1" class="input-file wp43" type="file" />
                                        <img  onclick="uploadfile();" src="/images/add.png" />
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
