﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Info.aspx.cs" Inherits="HBMS.Web.Repair.Info" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>

                            <td style="width: 13%;">
                                <label><%=Resources.Lbl.lblRepairCode %>:</label></td>
                            <td class="wp23">
                                <%=Model.ID%>
                            </td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                            <td class="wp10 "></td>
                            <td class="wp23"></td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblAirportName %>:</label></td>
                            <td class="wp23">
                                <%=Model.AirPortName%>
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblEquipmentName %>:</label></td>
                            <td class="wp23">
                                <%=Model.EquipmentName%>
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <%=Model.SystemName%>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <%=Model.PartsName%>
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRepairFZR %>:</label></td>
                            <td class="wp23">
                                <%=Model.RepairUse%>
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblCheckFZR %>:</label></td>
                            <td class="wp23">
                                <%=Model.CheckUser%>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRepairDate %>:</label></td>
                            <td class="wp23">
                                <%=DateFormat(Model.RepairDate)%>
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblSubUser %>:</label>
                            </td>
                            <td class="wp23">
                                <%=Model.CName%>
                            </td>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblSubDate %>:</label></td>
                            <td class="wp23">
                                <%=DateFormat(Model.SubDate)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRepairDescription %>:</label></td>
                            <td colspan="5">
                                <%=Model.Contect%>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <%=Model.memo%>
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label><%=Resources.Lbl.lbFiles %>:</label></td>
                            <td colspan="5">
                                <ul>
                                  <asp:Repeater ID="repeaterFile" runat="server">
                                      <ItemTemplate>
                                          <li><a href='<%# Eval("FilePath") %>'><%# Eval("FileName") %></a></li>
                                      </ItemTemplate>
                                  </asp:Repeater>  
                                </ul>
                               
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
