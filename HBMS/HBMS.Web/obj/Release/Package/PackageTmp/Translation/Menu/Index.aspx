﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HBMS.Web.Translation.Menu.Index" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/script/lib/json2.js"></script>
    <script>
        $(function () {
            query(1);
        });
        function query() {
            $.post("Ajax/ajax_Index.ashx", { action: "query" }, function (result) {
                $("#divResult").append(result);
            });
        }

        function save() {
            ////保存
            var data = new Array();
            $("#tbResult tbody").find("tr").each(function () {
                $tr = $(this);
                data.push({
                    MenuID: $tr.find(":hidden").eq(0).val(),
                    SPRAS: $tr.find(":hidden").eq(1).val(),
                    MenuName: $tr.find(":text").eq(0).val(),
                });
            });
            $.post("Ajax/ajax_Add.ashx", { data: JSON.stringify(data) }, function (result) {
                if (result.result == 1) {
                    MessageBox.success();
                } else {
                    MessageBox.error(result.error);
                }
            }, "json");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="box span10">
        <div id="divResult"></div>
        <div class="mt10">
            <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnSave %>" onclick="save();" />
            <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
        </div>
    </div>
</asp:Content>
