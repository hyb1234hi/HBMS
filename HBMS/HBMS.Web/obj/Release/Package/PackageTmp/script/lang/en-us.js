﻿var Lang = {
    Pagination: {
        first: 'First',
        prev: 'Previous',
        next: 'Next',
        last: 'Last',
    }
}