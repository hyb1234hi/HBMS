﻿/*
*
* 显示页数信息
*
*
*/
(function($) {
    ////rcms 扩展jquery插件
    $.fn.twbsPaginationEmpty = function() {
        ////分页控件无数据显示
        $(this).twbsPagination({
            totalPages: 1,        /////总页数
            visiblePages: 1,    /////最大可显示几页
            startPage: 1,           ////当前页
            first: Lang.Pagination.first,
            prev: Lang.Pagination.prev,
            next: Lang.Pagination.next,
            last: Lang.Pagination.last,
        });
    };

    $.fn.twbsPaginationMsgEmpty = function() {
        ////分页信息无数据显示
        $(this).empty().append(showPaginationMsg(1, 1, 0));
    };
})(jQuery);


function showPaginationMsg(startPage, totalPages, totalCount) {
    ////分页显示行数消息
    if (startPage == undefined || startPage == "") {
        ////当前页
        startPage = 0;
    }
    if (totalPages == undefined || totalPages == "") {
        ////总页数
        totalPages = 0;
    }
    if (totalCount == undefined || totalCount == "") {
        ////总行数
        totalCount = 0;
    }
    return "<li>第<span style='color:red'>" + startPage + "</span>页,共<span style='color:red'>" + totalPages + "</span>页&nbsp;&nbsp;总共 <span style='color:red'>" + totalCount + "</span> 条记录</li>";
}