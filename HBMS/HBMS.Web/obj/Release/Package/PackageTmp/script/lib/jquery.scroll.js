﻿(function ($) {
    $.fn.MapParamsScroll = function (option) {
        var defaults = {
            speed: 50,
        };

        var _option = $.extend({}, defaults, option), _int = [];

        function marquee(obj) {
            obj.find("ul").animate({
                marginTop: '-=1'
            }, 10, function () {
                var s = Math.abs(parseInt($(this).css("margin-top")));
                var perh = $(this).find("li").first().height();
                if (s >= perh) {
                    $(this).find("li").slice(0, 1).appendTo($(this));
                    $(this).css("margin-top", 0);
                }
            });
        }

        this.each(function (i) {
            var speed = _option.speed, _this = $(this);
            _int[i] = setInterval(function () {
                if (_this.find("ul").height() <= _this.height()) {
                    clearInterval(_int[i]);
                } else {
                    marquee(_this);
                }
            }, speed);
            _this.hover(function () {
                clearInterval(_int[i]);
            }, function () {
                _int[i] = setInterval(function () {
                    if (_this.find("ul").height() <= _this.height()) {
                        clearInterval(_int[i]);
                    } else {
                        marquee(_this);
                    }
                }, speed);
            });
        });
    };
})(jQuery);
