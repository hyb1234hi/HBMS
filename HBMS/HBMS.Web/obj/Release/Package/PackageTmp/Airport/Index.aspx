﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HBMS.Web.Airport.Index" %>
<%@ MasterType VirtualPath="~/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            query(1);
        });

        function GetOptionHtml(row) {
            var optionHtml = "";
            optionHtml += '<a target="_blank" href="Edit.aspx?id=' + row.AirPortID + '" ><img src="/images/pencil.png" width="16" height="16"></a>';
            return optionHtml;
        }

        function query(page) {
            $("#tbResult").NewDataTable({
                initComplete: function (settings, json) {
                    //////表格加载完后出发事件
                    if (json.data != undefined && json.data != "" && json.data.length > 0) {
                        $('#pagination').twbsPagination({
                            totalPages: json.totalPages,        /////总页数
                            visiblePages: json.visiblePages,    /////最大可显示几页
                            startPage: json.startPage,           ////当前页
                            onPageClick: function (event, page) { ////分页按钮点击事件
                                query(page);
                            },
                            refresh: 'query(1)'         /////刷新按钮
                        });
                        $("#pagination-message").empty().append(showPaginationMsg(json.startPage, json.totalPages, json.totalCount));
                    } else {
                        $("#pagination").twbsPaginationEmpty();
                        $("#pagination-message").twbsPaginationMsgEmpty();
                    }
                },
                ajax: {
                    url: "Ajax/ajax_Airport.ashx",   ////请求后台
                    type: "post",
                    data: function (d) { ////查询条件
                        d.action = "query";
                        d.name = $("#txtName").val();
                        d.page = page; ////页索引
                    },
                },
                columns: [////绑定每列
                    { data: "AirPortCode" },
                    { data: "AirPortName" },
                    { data: "CountryName" },
                    { data: "ProvinceName" },
                    { data: "CityName" },
                    { data: "AirPortAddress" },
                    { data: "Longitude" },
                    { data: "Dimension" },
                    { data: "Head" },
                    { data: "HeadPhone" },
                    {
                        data: function (row, type, set, meta) {
                            return GetOptionHtml(row);
                        }
                    }
                ]
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div class="box_top"><b class="pl15"><%=Resources.Lbl.lblQueryCondition %></b></div>
            <div class="pt10 pb10">
                <table class="tb-form">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label>机场名称:</label></td>
                            <td class="wp23">
                                <input type="text" id="txtName" name="name" class="input-text" /></td>
                            <td class="wp10"></td>
                            <td class="wp23"></td>
                            <td class="wp10"></td>
                            <td class="wp23"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-box ">
                <input type="button" class="btn btn82 btn_search" value="<%=Resources.Btn.btnQuery %>" onclick="query(1)" />
                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>"/>
                <a class="btn btn82 btn_add" href="Add.aspx"><%=Resources.Btn.btnAdd %></a>
            </div>
        </div>
        <div class="box span10">
            <table id="tbResult" class="tb-data">
                <thead>
                    <tr>
                        <th>机场编号</th>
                        <th>机场名称</th>
                        <th>国家</th>
                        <th>省份</th>
                        <th>城市</th>
                        <th>地址</th>
                        <th><%=Resources.Lbl.lblDimension %></th>
                        <th><%=Resources.Lbl.lblLongitude %></th>
                        <th>负责人</th>
                        <th>负责人电话</th>
                        <th><%=Resources.Lbl.lblOperation %></th>
                    </tr>
                </thead>
            </table>
            <div class="block-pagination">
                <ul id="pagination-message" class="controls-message"></ul>
                <ul id="pagination" class="controls-buttons"></ul>
            </div>
        </div>
    </div>
</asp:Content>
