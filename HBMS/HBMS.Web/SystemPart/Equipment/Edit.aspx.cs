﻿// 
// 创建人：宋欣
// 创建时间：2015-02-13
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HBMS.Web.SystemPart.Equipment
{
    public partial class Edit : BasePage
    {
        /// <summary>
        ///     设备参数
        /// </summary>
        public Model.View_EquipmentParams Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    this.ID = Request.QueryString["id"];
                    this.BindData();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        private void BindData()
        {
            BLL.View_EquipmentParams bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentParams>();
            DataSet ds =
                bll.GetList(string.Format(" ID='{0}'  ", this.ID));
            List<Model.View_EquipmentParams> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
                ////设置系统默认只
                this.uctrSystem.SelectedVal = Model.SystemID.ToString();
                ////设置部件下拉框初始值
                this.uctrParts.ShowAllOption = "1";
                this.uctrParts.SelectedVal = Model.PartsID.ToString();
                this.uctrParts.SystemID = Model.SystemID;
                ////设置下拉框初始值
                this.uctrParams.PartsID = Model.PartsID;
                this.uctrParams.ShowAllOption = "1";
                this.uctrParams.SelectedVal = Model.ParamID.ToString();
                ////设置设别下拉框默认值
                this.uctrEquipment.SelectedVal = Model.EquipmentID;
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navEquipmentParams + " </a> >> " +
                                Resources.Nav.navEquipmentParamsEdit);
        }
    }
}