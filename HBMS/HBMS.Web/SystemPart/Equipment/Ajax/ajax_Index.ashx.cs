﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.SystemPart.Equipment.Ajax
{
    /// <summary>
    /// ajax_Index 的摘要说明
    /// </summary>
    public class ajax_Index : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.Query();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string Query()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS={0} ", this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["equipment"]))
            {
                ////名称
                where.AppendFormat(" AND EquipmentName LIKE '%{0}%'", Context.Request.Params["equipment"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["system"]))
            {
                ////系统
                where.AppendFormat(" AND SystemID={0}", Context.Request.Params["system"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["parts"]))
            {
                ////部件
                where.AppendFormat(" AND PartsID={0}", Context.Request.Params["parts"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["params"]))
            {
                ////参数
                where.AppendFormat(" AND ParamID='{0}'", Context.Request.Params["params"].Trim());
            }
            BLL.View_EquipmentParams bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentParams>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " OrderIndex ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentParams> list = bll.DataTableToList(ds.Tables[0]);

                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                    {
                        data = from l in list
                               select new
                               {
                                   l.ID,
                                   l.SubDatetime,
                                   l.OrderIndex,
                                   l.EquipmentID,
                                   l.EquipmentName,
                                   l.ParamID,
                                   l.ParamName,
                                   l.ParamTypeID,
                                   ParamTypeDesc = l.ParamTypeID.ParamType(),
                                   l.boolValue,
                                   l.PromptMin,
                                   l.PromptMax,
                                   l.WarningMin,
                                   l.WarningMax,
                                   l.ErrorMin,
                                   l.ErrorMax,
                                   l.PartsID,
                                   l.PartsName,
                                   l.SystemID,
                                   l.SPRAS,
                                   l.SystemName,
                               },
                        totalCount = Pager.TotalCount,
                        totalPages = Pager.TotalPage,
                        visiblePages = Pager.VisiblePages,
                        startPage = Pager.PageIndex
                    }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}