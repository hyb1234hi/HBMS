﻿// 
// 创建人：宋欣
// 创建时间：2015-02-08
// 功能：新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.Equipment.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_EquipmentParams model = new Model.HY_BMS_EquipmentParams();
            TryUpdateModel(model);
            model.SubDatetime = DateTime.Now;
            ////实例化
            BLL.HY_BMS_EquipmentParams bll = BLLFactory.CreateInstance<BLL.HY_BMS_EquipmentParams>();
            ////判断是否有相同数据的存在
            DataSet ds =
                bll.GetList(string.Format(" EquipmentID='{0}' AND ParamID={1} ", model.EquipmentID, model.ParamID));
            if (ds.Tables[0].Rows.Count == 0)
            {
                bll.Add(model);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorSystemNameExist);
        }
    }
}