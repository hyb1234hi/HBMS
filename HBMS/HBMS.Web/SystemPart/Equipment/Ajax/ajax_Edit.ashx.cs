﻿// 
// 创建人：宋欣
// 创建时间：2015-02-08
// 功能：编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.Equipment.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["ID"]))
            {
                int id = Convert.ToInt32(Context.Request.Params["ID"]);
                ////实例化
                BLL.HY_BMS_EquipmentParams bll = BLLFactory.CreateInstance<BLL.HY_BMS_EquipmentParams>();
                ////获取实体
                Model.HY_BMS_EquipmentParams model = bll.GetModel(id);
                TryUpdateModel(model);
                ////更新数据
                bll.Update(model);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorParam);
        }
    }
}