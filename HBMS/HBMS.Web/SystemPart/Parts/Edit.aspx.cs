﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：编辑部件
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HBMS.Web.SystemPart.Parts
{
    public partial class Edit : BasePage
    {
        /// <summary>
        ///     部件ID
        /// </summary>
        public string PartsID { get; set; }

        /// <summary>
        ///     部件实体
        /// </summary>
        public Model.View_PartsDesc Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    this.PartsID = Request.QueryString["id"];
                    this.BindData();
                    this.uctrSystemID.SelectedVal = Model.SystemID.ToString();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        private void BindData()
        {
            BLL.View_PartsDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_PartsDesc>();
            DataSet ds =
                bll.GetList(string.Format(" PartsID='{0}' AND SPRAS={1} ", this.PartsID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_PartsDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navParts + " </a> >> " +
                                Resources.Nav.navPartsEdit);
        }
    }
}