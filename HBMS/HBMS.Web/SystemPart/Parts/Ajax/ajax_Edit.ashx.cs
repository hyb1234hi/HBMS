﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：部件编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.Parts.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            Model.HY_BMS_Parts model = new Model.HY_BMS_Parts();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            Model.HY_BMS_PartsDesc desc = new Model.HY_BMS_PartsDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_Parts bll = BLLFactory.CreateInstance<BLL.HY_BMS_Parts>();
            BLL.HY_BMS_PartsDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_PartsDesc>();
            DataSet ds =BLLFactory.CreateInstance<BLL.View_PartsDesc>()
                .GetList(string.Format(" PartsName='{0}' AND SPRAS={1} AND PartsID<>{2} AND SystemID={3}", desc.PartsName,
                                              desc.SPRAS, desc.PartsID,model.SystemID));
            if (ds.Tables[0].Rows.Count == 0)
            {
                ////更新数据
                bll.Update(model);
                blldesc.Update(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorPartsNameExist);
        }
    }
}