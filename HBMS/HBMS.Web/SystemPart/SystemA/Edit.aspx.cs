﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HBMS.Web.SystemPart.SystemA
{
    public partial class Edit : BasePage
    {
        /// <summary>
        ///     系统ID
        /// </summary>
        public string SystemID { get; set; }

        /// <summary>
        ///     设备实体
        /// </summary>
        public Model.View_SystemDesc Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    this.SystemID = Request.QueryString["id"];
                    this.BindData();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        private void BindData()
        {
            BLL.View_SystemDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_SystemDesc>();
            DataSet ds =
                bll.GetList(string.Format(" SystemID='{0}' AND SPRAS={1} ", this.SystemID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_SystemDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navSystem + " </a> >> " +
                                Resources.Nav.navSystemEdit);
        }
    }
}