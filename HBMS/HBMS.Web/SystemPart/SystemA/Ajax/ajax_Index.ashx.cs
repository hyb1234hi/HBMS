﻿// 
// 创建人：宋欣
// 创建时间：2015-02-03
// 功能：系统管理查询后台
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.SystemPart.SystemA.Ajax
{
    /// <summary>
    ///     ajax_Index 的摘要说明
    /// </summary>
    public class ajax_Index : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("query"))
            {
                WriteResult = this.Query();
            }
            ResponseWrite();
        }

        /// <summary>
        ///     查询用户
        /// </summary>
        public string Query()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS={0} ",this.GoalUserInfo.Spras);
            if (!string.IsNullOrEmpty(Context.Request.Params["id"]))
            {
                ////编号
                where.AppendFormat(" AND SystemID={0}", Context.Request.Params["id"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["name"]))
            {
                ////编号
                where.AppendFormat(" AND SystemName LIKE '%{0}%'", Context.Request.Params["name"].Trim());
            }
            if (!string.IsNullOrEmpty(Context.Request.Params["use"]))
            {
                ////编号
                where.AppendFormat(" AND IsUse={0}", Context.Request.Params["use"]);
            }
            BLL.View_SystemDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_SystemDesc>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where.ToString());
            ////分页查询
            DataSet ds = bll.GetListByPage(where.ToString(), " OrderIndex ", Pager.StartIndex, Pager.EndIndex);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_SystemDesc> list = bll.DataTableToList(ds.Tables[0]);

                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                        {
                            data = from l in list
                                   select new
                                       {
                                           l.SystemID,
                                           l.SPRAS,
                                           l.OrderIndex,
                                           l.SystemName,
                                           l.IsUse,
                                           IsUseDesc = l.IsUse.YesNo()
                                       },
                            totalCount = Pager.TotalCount,
                            totalPages = Pager.TotalPage,
                            visiblePages = Pager.VisiblePages,
                            startPage = Pager.PageIndex
                        }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }
    }
}