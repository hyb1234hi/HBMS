﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：设备新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.SystemA.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_System model = new Model.HY_BMS_System();
            TryUpdateModel(model);
            model.IsUse = true;

            Model.HY_BMS_SystemDesc desc = new Model.HY_BMS_SystemDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_SystemDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_SystemDesc>();
            ////判断是否有同名的存在
            DataSet ds = blldesc.GetList(string.Format(" SystemName='{0}' AND SPRAS={1} ",desc.SystemName, desc.SPRAS));
            if (ds.Tables[0].Rows.Count == 0)
            {
                BLL.HY_BMS_System bll = BLLFactory.CreateInstance<BLL.HY_BMS_System>();
                model.SystemID=bll.Add(model);
                desc.SystemID = model.SystemID;
                blldesc.Add(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorSystemNameExist);
        }
    }
}