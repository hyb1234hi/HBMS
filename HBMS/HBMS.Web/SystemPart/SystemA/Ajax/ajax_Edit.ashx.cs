﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：编辑
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.SystemA.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            Model.HY_BMS_System model = new Model.HY_BMS_System();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            Model.HY_BMS_SystemDesc desc = new Model.HY_BMS_SystemDesc();
            TryUpdateModel(desc);
            ////实例化
            BLL.HY_BMS_System bll = BLLFactory.CreateInstance<BLL.HY_BMS_System>();
            BLL.HY_BMS_SystemDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_SystemDesc>();
            ////更新数据
            bll.Update(model);
            blldesc.Update(desc);
            return JsonSuccess();
        }
    }
}