﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HBMS.Web.SystemPart.Params.Edit" %>

<%@ Import Namespace="HBMS.Common" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/SystemCtr.ascx" TagName="SystemCtr" TagPrefix="uc2" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/PartsCtr.ascx" TagName="PartsCtr" TagPrefix="uc3" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/ParamType.ascx" TagName="ParamType" TagPrefix="uc1" %>
<%@ Register src="../../CustomerControl/SystemPartsCtr/ParamsMsgCtr.ascx" tagname="ParamsMsgCtr" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
       
        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Edit.ashx", $form.formSerialize(), function (result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }

        function changeParamType(obj) {
            if ($(obj).val() == "<%=(int)ParamTypeEnum.布尔%>") {
                $(".ParamType-Boolean").css("display", "table-row");
                $(".ParamType-Number").each(function () {
                    $(this).css("display", "none");
                });
            } else {
                $(".ParamType-Number").each(function () {
                    $(this).css("display", "table-row");
                });
                $(".ParamType-Boolean").css("display", "none");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="edit" />
                <input type="hidden" name="ParamID" value="<%=Model.ParamID %>"/>
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <uc2:SystemCtr ID="uctrSystem" CSS="select" runat="server" ShowAllOption="1" />
                            </td>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <uc3:PartsCtr ID="uctrParts" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblParamsName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ParamName" value="<%=Model.ParamName.Trim() %>" class="validate[required] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblParamsCode %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ParamCode" value="<%=Model.ParamCode %>" class="validate[required] input-text wp100" />
                            </td>
                            
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblParamsType %>:</label></td>
                            <td class="wp23">
                                <uc1:ParamType ID="uctParamType" ControlName="ParamTypeID" OnClientChange="changeParamType(this)" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" value="<%=Model.OrderIndex %>" class="validate[required] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea rows="5" class="input-text wp100" name="Memo"><%=Model.Memo %></textarea>
                            </td>
                        </tr>
                        <tr class="ParamType-Boolean" style="<%=this.StrBooleanDisplay%>">
                            <td>
                                <label><%=Resources.Lbl.lblCanBoolean %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="boolValue" value="<%=Model.boolValue %>" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblInfoVal %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="InfoLevel" value="<%=Model.InfoLevel %>" class="input-text wp100" />
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr class="ParamType-Number" style="<%=this.StrNumberDisplay%>">
                            <td>
                                <label><%=Resources.Lbl.lblPromptMin %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="PromptMin" value="<%=Model.PromptMin %>" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblPromptMax %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="PromptMax" value="<%=Model.PromptMax %>" class="input-text wp100" />
                            </td>
                           <td>
                                <label><%=Resources.Lbl.lblParamsMsg %>:</label></td>
                            <td class="wp23">
                                <uc4:ParamsMsgCtr ID="uctrParamsMsgPrompt" ControlID="ddlPromptMsgFW" ControlName="PromptMsgFW"  runat="server" />
                            </td>
                        </tr>
                        <tr class="ParamType-Number" style="<%=this.StrNumberDisplay%>">
                            <td>
                                <label><%=Resources.Lbl.lblWarningMin %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="WarningMin" value="<%=Model.WarningMin %>" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblWarningMax %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="WarningMax" value="<%=Model.WarningMax %>" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblParamsMsg %>:</label></td>
                            <td class="wp23">
                                <uc4:ParamsMsgCtr ID="uctrParamsMsgWarning" ControlID="ddlWarningMsgFW" ControlName="WarningMsgFW"  runat="server" />
                            </td>
                            
                        </tr>
                        <tr class="ParamType-Number" style="<%=this.StrNumberDisplay%>">
                            <td>
                                <label><%=Resources.Lbl.lblErrorMin %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ErrorMin" value="<%=Model.ErrorMin %>" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblErrorMax %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ErrorMax" value="<%=Model.ErrorMax %>" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblParamsMsg %>:</label></td>
                            <td class="wp23">
                                <uc4:ParamsMsgCtr ID="uctrParamsMsgError"  ControlID="ddlErrorMsgFW" ControlName="ErrorMsgFW"  runat="server" />
                            </td>
                        </tr>
                         <tr>
                            <td colspan="6">
                                <label><%=Resources.Lbl.lblUse %>:</label>
                                <% if (Model.IsUse)
                                   { %>

                                <input type="checkbox" value="true" name="IsUse" checked="checked" onclick="ChangeChkboxVal(this)" />

                                <% }
                                   else
                                   { %>
                                <input type="checkbox" value="false" name="IsUse" onclick="ChangeChkboxVal(this)" />
                                <% } %>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnEdit %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
