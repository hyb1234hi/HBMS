﻿// 
// 创建人：宋欣
// 创建时间：2015-02-07
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using HBMS.Common;

namespace HBMS.Web.SystemPart.Params
{
    public partial class Edit : BasePage
    {
        /// <summary>
        ///     参数ID
        /// </summary>
        public string ParamID { get; set; }

        /// <summary>
        ///     参数实体
        /// </summary>
        public Model.View_ParamDesc Model { get; set; }

        /// <summary>
        ///     布尔行显示或隐藏
        /// </summary>
        public string StrBooleanDisplay = "";

        /// <summary>
        ///     数值行隐藏或显示
        /// </summary>
        public string StrNumberDisplay = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    this.ParamID = Request.QueryString["id"];
                    this.BindData();
                }
                else
                {
                    MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
                }
            }
        }

        private void BindData()
        {
            BLL.View_ParamDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_ParamDesc>();
            DataSet ds =
                bll.GetList(string.Format(" ParamID='{0}' AND SPRAS={1} ", this.ParamID,
                                          this.Master.GoalUserInfo.Spras));
            List<Model.View_ParamDesc> list = bll.DataTableToList(ds.Tables[0]);
            if (list.Count > 0)
            {
                Model = list.First();
                this.uctrSystem.SelectedVal = Model.SystemID.ToString();
                ////绑定部件。且显示项
                this.uctrParts.SystemID = Model.SystemID;
                this.uctrParts.SelectedVal = Model.PartsID.ToString();
                this.uctrParts.ShowAllOption = "1";
                if (Model.ParamTypeID == (int) ParamTypeEnum.布尔)
                {
                    this.StrNumberDisplay = "display:none";
                }
                else
                {
                    this.StrBooleanDisplay = "display:none";
                    this.uctrParamsMsgPrompt.SelectedVal = Model.PromptMsgFW.ToString();
                    this.uctrParamsMsgWarning.SelectedVal = Model.WarningMsgFW.ToString();
                    this.uctrParamsMsgError.SelectedVal = Model.ErrorMsgFW.ToString();

                }
            }
            else
            {
                MessageBox.ErrorAndRedirect(this, Resources.Msg.errorParam, "Index.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navParams + " </a> >> " +
                                Resources.Nav.navParamsEdit);
        }
    }
}