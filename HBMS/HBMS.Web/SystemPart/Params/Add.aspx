﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HBMS.Web.SystemPart.Params.Add" %>
<%@ Import Namespace="HBMS.Common" %>

<%@ MasterType VirtualPath="~/Main.master" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/SystemCtr.ascx" TagName="SystemCtr" TagPrefix="uc2" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/PartsCtr.ascx" TagName="PartsCtr" TagPrefix="uc3" %>
<%@ Register Src="../../CustomerControl/SystemPartsCtr/ParamType.ascx" TagName="ParamType" TagPrefix="uc1" %>
<%@ Register src="../../CustomerControl/SystemPartsCtr/ParamsMsgCtr.ascx" tagname="ParamsMsgCtr" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $("#<%=this.uctrSystem.ClientControlID%>").change(function () {
                ////绑定系统下拉change事件
                SystemParts.ChangeSystemGetParts(this, "<%=this.uctrParts.ClientControlID%>");
            });
        });

        function add() {
            $form = $("#<%=this.Master.form1.ClientID%>");
            if ($form.validationEngine('validate')) {
                $.post("Ajax/ajax_Add.ashx", $form.formSerialize(), function(result) {
                    if (result.result == 1) {
                        MessageBox.success();
                    } else {
                        MessageBox.error(result.error);
                    }
                }, "json");
            }
        }
        
        function changeParamType(obj) {
            if ($(obj).val() == "<%=(int)ParamTypeEnum.布尔%>") {
                $(".ParamType-Boolean").css("display", "table-row");
                $(".ParamType-Number").each(function () {
                    $(this).css("display", "none");
                });
            } else {
                $(".ParamType-Number").each(function() {
                    $(this).css("display", "table-row");
                });
                $(".ParamType-Boolean").css("display", "none");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box_border">
            <div>
                <input type="hidden" name="action" value="add" />
                <table id="tbfrom" class="tb-form wp100">
                    <tbody>
                        <tr>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblSystemName %>:</label></td>
                            <td class="wp23">
                                <uc2:SystemCtr ID="uctrSystem" CSS="select" runat="server" ShowAllOption="1" />
                            </td>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblPartsName %>:</label></td>
                            <td class="wp23">
                                <uc3:PartsCtr ID="uctrParts" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblParamsName %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ParamName" class="validate[required] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblParamsCode %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ParamCode" class="validate[required] input-text wp100" />
                            </td>
                            <td class="wp10">
                                <label class="require"><%=Resources.Lbl.lblParamsType %>:</label></td>
                            <td class="wp23">
                                <uc1:ParamType ID="uctParamType" ControlName="ParamTypeID" OnClientChange="changeParamType(this)" runat="server" />
                            </td>
                            <td class="wp10 ">
                                <label class="require"><%=Resources.Lbl.lblOrderIndex %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="OrderIndex" class="validate[required] input-text wp100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%=Resources.Lbl.lblRemark %>:</label></td>
                            <td colspan="5">
                                <textarea rows="5" class="input-text wp100" name="Memo"></textarea>
                            </td>
                        </tr>
                        <tr class="ParamType-Boolean" style="display: none;">
                            <td>
                                <label><%=Resources.Lbl.lblCanBoolean %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="boolValue" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblInfoVal %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="InfoLevel" value="1" class="input-text wp100" />
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr  class="ParamType-Number">
                            <td>
                                <label><%=Resources.Lbl.lblPromptMin %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="PromptMin" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblPromptMax %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="PromptMax" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblParamsMsg %>:</label></td>
                            <td class="wp23">
                                <uc4:ParamsMsgCtr ID="uctrParamsMsgPrompt" ControlName="PromptMsgFW"  runat="server" />
                            </td>
                        </tr>
                        <tr class="ParamType-Number">
                            <td>
                                <label><%=Resources.Lbl.lblWarningMin %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="WarningMin" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblWarningMax %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="WarningMax" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblParamsMsg %>:</label></td>
                            <td class="wp23">
                                <uc4:ParamsMsgCtr ID="uctrParamsMsgWarning" ControlName="WarningMsgFW"  runat="server" />
                            </td>
                        </tr>
                        <tr class="ParamType-Number">
                            <td>
                                <label><%=Resources.Lbl.lblErrorMin %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ErrorMin" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblErrorMax %>:</label></td>
                            <td class="wp23">
                                <input type="text" name="ErrorMax" class="input-text wp100" />
                            </td>
                            <td>
                                <label><%=Resources.Lbl.lblParamsMsg %>:</label></td>
                            <td class="wp23">
                                <uc4:ParamsMsgCtr ID="uctrParamsMsgError" ControlName="ErrorMsgFW"  runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="td_left">
                                <input type="button" class="btn btn82 btn_save2" value="<%=Resources.Btn.btnAdd %>" onclick="add();" />
                                <input type="reset" class="btn btn82 btn_res" value="<%=Resources.Btn.btnReset %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
