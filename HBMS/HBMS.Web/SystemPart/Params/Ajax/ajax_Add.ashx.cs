﻿// 
// 创建人：宋欣
// 创建时间：2015-02-06
// 功能：新增
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.Params.Ajax
{
    /// <summary>
    ///     ajax_Add 的摘要说明
    /// </summary>
    public class ajax_Add : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("add"))
                {
                    WriteResult = this.Add();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Add()
        {
            ////获取实体
            Model.HY_BMS_Param model = new Model.HY_BMS_Param();
            TryUpdateModel(model);
            model.IsUse = true;

            Model.HY_BMS_ParamDesc desc = new Model.HY_BMS_ParamDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_ParamDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_ParamDesc>();
            ////判断是否有同名的存在
            DataSet ds = blldesc.GetList(string.Format(" ParamName='{0}' AND SPRAS={1} ", desc.ParamName, desc.SPRAS));
            if (ds.Tables[0].Rows.Count == 0)
            {
                BLL.HY_BMS_Param bll = BLLFactory.CreateInstance<BLL.HY_BMS_Param>();
                model.ParamID = bll.Add(model);
                desc.ParamID = model.ParamID;
                blldesc.Add(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorPartsNameExist);
        }
    }
}