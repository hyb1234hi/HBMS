﻿// 
// 创建人：宋欣
// 创建时间：2015-02-07
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web;
using HBMS.Common;
using HBMS.Factory;

namespace HBMS.Web.SystemPart.Params.Ajax
{
    /// <summary>
    ///     ajax_Edit 的摘要说明
    /// </summary>
    public class ajax_Edit : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (Action.Equals("edit"))
                {
                    WriteResult = this.Edit();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        private string Edit()
        {
            ////获取实体
            Model.HY_BMS_Param model = new Model.HY_BMS_Param();
            TryUpdateModel(model);
            if (string.IsNullOrEmpty(Context.Request.Params["IsUse"]))
            {
                model.IsUse = false;
            }
            Model.HY_BMS_ParamDesc desc = new Model.HY_BMS_ParamDesc();
            TryUpdateModel(desc);
            desc.SPRAS = this.GoalUserInfo.Spras;
            ////实例化
            BLL.HY_BMS_Param bll = BLLFactory.CreateInstance<BLL.HY_BMS_Param>();
            BLL.HY_BMS_ParamDesc blldesc = BLLFactory.CreateInstance<BLL.HY_BMS_ParamDesc>();
            DataSet ds = BLLFactory.CreateInstance<BLL.View_ParamDesc>()
                                   .GetList(
                                       string.Format(" ParamName='{0}' AND SPRAS={1} AND ParamID<>{2} AND PartsID={3}",
                                                     desc.ParamName,
                                                     desc.SPRAS, desc.ParamID, model.PartsID));
            if (ds.Tables[0].Rows.Count == 0)
            {
                ////更新数据
                bll.Update(model);
                blldesc.Update(desc);
                return JsonSuccess();
            }
            return JsonError(Resources.Msg.errorPartsNameExist);
        }
    }
}