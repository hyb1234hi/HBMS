﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.SystemPart.Params
{
    public partial class Add : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            ////设置面包线
            this.Master.CurPageLocation =
                RebuildLocation("<a href=\"Index.aspx\" >" + Resources.Nav.navParams + " </a> >> " +
                                Resources.Nav.navParamsAdd);
        }
    }
}