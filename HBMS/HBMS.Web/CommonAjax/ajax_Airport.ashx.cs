﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using HBMS.Common;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.CommonAjax
{
    /// <summary>
    /// ajax_Airport 的摘要说明
    /// </summary>
    public class ajax_Airport : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (this.Action.Equals("changeairportgetequipment"))
                {
                    ////改变机场下拉框获取设备
                    WriteResult = this.ChangeAirportGetEquipment();
                }
                else if (this.Action.Equals("changeequipmentgetsystem"))
                {
                    ////改变设备下拉框获取系统
                    WriteResult = this.ChangeEquipmentGetSystem();
                }
               
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        /// <summary>
        /// 根据机场获取设备
        /// </summary>
        /// <returns></returns>
        private string ChangeAirportGetEquipment()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["airportid"]))
            {
                int airportID = Convert.ToInt32(Context.Request.Params["airportid"]);
                BLL.View_EquipmentDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentDesc>();
                DataSet ds = bll.GetList(string.Format(" AirPortID='{0}' AND SPRAS={1} ", airportID,
                                                           this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////获取部件实体
                    List<Model.View_EquipmentDesc> list = bll.DataTableToList(ds.Tables[0]);
                    return JToken.FromObject(
                        from l in list
                        select new
                            {
                                l.EquipmentID,
                                EquipmentName = l.IsUse ? l.EquipmentName : (l.EquipmentName + "(" + Resources.Lbl.lblDisable + ")"),
                                l.IsUse
                            }).ToString();
                }
            }
            return "";
        }

        private string ChangeEquipmentGetSystem()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["equipmentid"]))
            {
                int equipmentID = Convert.ToInt32(Context.Request.Params["equipmentid"]);
                BLL.View_EquipmentParams bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentParams>();
                DataSet ds = bll.GetList(string.Format(" EquipmentID='{0}' AND SPRAS={1} ", equipmentID,
                                                           this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////获取部件实体
                    List<Model.View_EquipmentParams> list = bll.DataTableToList(ds.Tables[0]);
                    return JToken.FromObject(
                        (from l in list
                        select new
                            {
                                l.SystemID,
                                l.SystemName
                            }).Distinct()).ToString();
                }
            }
            return "";
        }

    }
}