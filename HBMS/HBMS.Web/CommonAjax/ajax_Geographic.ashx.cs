﻿// 
// 创建人：宋欣
// 创建时间：2015-02-13
// 功能：地理位置
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using HBMS.Common;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.CommonAjax
{
    /// <summary>
    ///     ajax_Geographic 的摘要说明
    /// </summary>
    public class ajax_Geographic : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (this.Action.Equals("changecountrygetprovince"))
                {
                    ////改变系统下拉框获取部件
                    WriteResult = this.ChangeCountryGetProvince();
                }
                else if (this.Action.Equals("changeprovincegetcity"))
                {
                    WriteResult = this.ChangeProvinceGetCity();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        /// <summary>
        ///     改变国家下拉框获取省份
        /// </summary>
        /// <returns></returns>
        public string ChangeCountryGetProvince()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["countryid"]))
            {
                ////系统ID
                string countryID = Context.Request.Params["countryid"];
                BLL.View_ProvinceDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_ProvinceDesc>();
                DataSet ds = Factory.BLLFactory.CreateInstance<BLL.View_ProvinceDesc>()
                                    .GetList(string.Format(" CountryID='{0}' AND SPRAS={1} ", countryID,
                                                           this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////获取部件实体
                    List<Model.View_ProvinceDesc> list = bll.DataTableToList(ds.Tables[0]);
                    return JToken.FromObject(
                        from l in list
                        select new
                            {
                                l.ProvinceID,
                                ProvinceName =
                            l.IsUse ? l.ProvinceName : (l.ProvinceName + "(" + Resources.Lbl.lblDisable + ")"),
                                l.IsUse
                            }).ToString();
                }
            }
            return "";
        }

        /// <summary>
        ///     改变省份下拉框获取城市
        /// </summary>
        /// <returns></returns>
        public string ChangeProvinceGetCity()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["provinceid"]))
            {
                ////省份ID
                string provinceID = Context.Request.Params["provinceid"];
                BLL.View_CityDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_CityDesc>();
                DataSet ds = Factory.BLLFactory.CreateInstance<BLL.View_CityDesc>()
                                    .GetList(string.Format(" ProvinceID='{0}' AND SPRAS={1} ", provinceID,
                                                           this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////获取部件实体
                    List<Model.View_CityDesc> list = bll.DataTableToList(ds.Tables[0]);
                    return JToken.FromObject(
                        from l in list
                        select new
                            {
                                l.CityID,
                                CityName = l.IsUse ? l.CityName : (l.CityName + "(" + Resources.Lbl.lblDisable + ")"),
                                l.IsUse
                            }).ToString();
                }
            }
            return "";
        }
    }
}