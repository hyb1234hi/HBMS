﻿// 
// 创建人：宋欣
// 创建时间：2015-02-05
// 功能：系统部件通用
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using HBMS.Common;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.CommonAjax
{
    /// <summary>
    ///     ajax_SystemPart 的摘要说明
    /// </summary>
    public class ajax_SystemPart : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            try
            {
                if (this.Action.Equals("changesystemgetparts"))
                {
                    ////改变系统下拉框获取部件
                    WriteResult = this.ChangeSystemGetParts();
                }
                else if (this.Action.Equals("changepartgetparams"))
                {
                    WriteResult = this.ChangePartGetParams();
                }
            }
            catch (Exception e)
            {
                WriteResult = JsonError(e.Message.ReplaceExceptionMessage());
            }

            ResponseWrite();
        }

        /// <summary>
        ///     改变系统下拉框获取部件
        /// </summary>
        /// <returns></returns>
        public string ChangeSystemGetParts()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["systemid"]))
            {
                ////系统ID
                int systemID = Convert.ToInt32(Context.Request.Params["systemid"]);
                BLL.View_PartsDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_PartsDesc>();
                DataSet ds = Factory.BLLFactory.CreateInstance<BLL.View_PartsDesc>()
                                    .GetList(string.Format(" SystemID={0} AND SPRAS={1} ", systemID,
                                                           this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////获取部件实体
                    List<Model.View_PartsDesc> list = bll.DataTableToList(ds.Tables[0]);
                    return JToken.FromObject(
                        from l in list
                        select new
                            {
                                l.PartsID,
                                PartsName = l.IsUse ? l.PartsName : (l.PartsName + "(" + Resources.Lbl.lblDisable + ")"),
                                l.IsUse
                            }).ToString();
                }
            }
            return "";
        }

        /// <summary>
        ///     改变部件下拉框获取参数
        /// </summary>
        /// <returns></returns>
        public string ChangePartGetParams()
        {
            if (!string.IsNullOrEmpty(Context.Request.Params["partid"]))
            {
                ////部件ID
                int partID = Convert.ToInt32(Context.Request.Params["partid"]);
                BLL.View_ParamDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_ParamDesc>();
                DataSet ds = Factory.BLLFactory.CreateInstance<BLL.View_ParamDesc>()
                                    .GetList(string.Format(" PartsID={0} AND SPRAS={1} ", partID,
                                                           this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ////获取部件实体
                    List<Model.View_ParamDesc> list = bll.DataTableToList(ds.Tables[0]);
                    return JToken.FromObject(
                        from l in list
                        select new
                        {
                            l.ParamID,
                            ParamName = l.IsUse ? l.ParamName : (l.ParamName + "(" + Resources.Lbl.lblDisable + ")"),
                            l.IsUse
                        }).ToString();
                }
            }
            return "";
        }
    }
}