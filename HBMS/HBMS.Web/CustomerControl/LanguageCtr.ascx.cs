﻿// 
// 创建人：宋欣
// 创建时间：2015-01-30
// 功能：语言控件
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web.UI;
using HBMS.BLL;
using HBMS.Factory;

namespace HBMS.Web.CustomerControl
{
    public partial class LanguageCtr : BaseUserControl
    {
        /// <summary>
        ///     控件名字
        /// </summary>
        private string _controlName = "spras";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        /// <summary>
        /// 客户端控件ID
        /// </summary>
        public string ClientControlID
        {
            get { return this.dllCtrSpra.ClientID; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.dllCtrSpra.Attributes.Add("name", this.ControlName);
            if (!IsPostBack)
            {
                ////查询所有启用的语言
                DataSet ds = BLLFactory.CreateInstance<HY_BMS_SPRAS>().GetList(" IsUse=1 ");
                this.dllCtrSpra.DataSource = ds;
                this.dllCtrSpra.DataTextField = "LangName";
                this.dllCtrSpra.DataValueField = "ID";
                this.dllCtrSpra.DataBind();
            }
        }
    }
}