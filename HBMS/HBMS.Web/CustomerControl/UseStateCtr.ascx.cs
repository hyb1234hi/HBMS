﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：启用状态控件
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.CustomerControl
{
    public partial class UseStateCtr : BaseUserControl
    {
        private string _controlID = "ddlUseState";

        private string _controlName = "use";

        public string ControlID
        {
            get { return _controlID; }
            set { _controlID = value; }
        }

        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}