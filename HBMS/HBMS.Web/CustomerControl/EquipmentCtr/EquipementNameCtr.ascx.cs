﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl.EquipmentCtr
{
    public partial class EquipementNameCtr : BaseUserControl
    {
        private string _controlName = "EquipmentID";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        private string _selectedVal = "";

        /// <summary>
        ///     选中的值
        /// </summary>
        public string SelectedVal
        {
            get { return _selectedVal; }
            set { _selectedVal = value; }
        }

        /// <summary>
        ///     样式
        /// </summary>
        public string CSS
        {
            get { return _css; }
            set { _css = value; }
        }

        private string _css = "select";

        private string _onClientChange = "";

        /// <summary>
        ///     是否显示全部选项，默认为0，不显示
        /// </summary>
        private string _showAllOption = "0";

        /// <summary>
        ///     是否显示全部选项，默认为0，不显示
        /// </summary>
        public string ShowAllOption
        {
            get { return _showAllOption; }
            set { _showAllOption = value; }
        }

        /// <summary>
        /// 控件的前台ID
        /// </summary>
        public string ClientControlID
        {
            get { return this.ddlEquipment.ClientID; }
        }

        /// <summary>
        /// 前台change事件
        /// </summary>
        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        ///     绑定数据
        /// </summary>
        private void BindData()
        {
            this.ddlEquipment.CssClass = this.CSS;
            if (ShowAllOption == "0")
            {
                ////不显示全部项
                this.ddlEquipment.Items.Add("");
            }
            else
            {
                BLL.View_EquipmentDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_EquipmentDesc>();
                DataSet ds = bll.GetList(string.Format(" SPRAS={0} ", this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<Model.View_EquipmentDesc> list = bll.DataTableToList(ds.Tables[0]);
                    this.ddlEquipment.Items.Add("");
                    ListItem option = new ListItem();
                    foreach (var item in list)
                    {
                        option = new ListItem(
                            item.IsUse
                                ? item.EquipmentName
                                : (item.EquipmentName + "(" + Resources.Lbl.lblDisable + ")"),
                            item.EquipmentID.ToString());
                        if (!item.IsUse)
                        {
                            option.Attributes.Add("class", "jinyong");
                        }
                        this.ddlEquipment.Items.Add(option);
                    }
                }

                ////默认选中值
                if (!string.IsNullOrEmpty(SelectedVal))
                {
                    ListItem selectIteme = this.ddlEquipment.Items.FindByValue(this.SelectedVal);
                    if (selectIteme != null)
                    {
                        selectIteme.Selected = true;
                    }
                }
            }
            //// 增加onchange事件
            if (!string.IsNullOrEmpty(OnClientChange))
            {
                this.ddlEquipment.Attributes.Add("onchange", this.OnClientChange);
            }
        }
    }
}