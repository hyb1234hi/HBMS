﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CityCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.GeographicCtr.CityCtr" %>
<asp:DropDownList ID="ddlCity" runat="server"></asp:DropDownList>
<script >
    if ('<%=this.ControlName%>' != "") {
        $("#<%=this.ddlCity.ClientID%>").attr("name", "<%=this.ControlName%>");
    }
</script>