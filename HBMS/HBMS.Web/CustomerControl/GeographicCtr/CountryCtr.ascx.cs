﻿// 
// 创建人：宋欣
// 创建时间：2015-02-13
// 功能：国家下拉框
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl.GeographicCtr
{
    public partial class CountryCtr : BaseUserControl
    {
        private string _controlName = "CountryID";
        private string _selectedVal = "";
        private string _css = "select";
        private string _onClientChange = "";

        /// <summary>
        ///     是否显示全部选项，默认为0，不显示
        /// </summary>
        private string _showAllOption = "0";

        /// <summary>
        ///     国家ID
        /// </summary>
        public int CountryID { get; set; }

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        /// <summary>
        ///     选中的值
        /// </summary>
        public string SelectedVal
        {
            get { return _selectedVal; }
            set { _selectedVal = value; }
        }

        /// <summary>
        ///     样式
        /// </summary>
        public string CSS
        {
            get { return _css; }
            set { _css = value; }
        }

        /// <summary>
        ///     控件的前台ID
        /// </summary>
        public string ClientControlID
        {
            get { return this.ddlCountry.ClientID; }
        }

        /// <summary>
        ///     前台change事件
        /// </summary>
        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }

        /// <summary>
        ///     是否显示全部选项，默认为0，不显示
        /// </summary>
        public string ShowAllOption
        {
            get { return _showAllOption; }
            set { _showAllOption = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            this.ddlCountry.CssClass = CSS;
            if (ShowAllOption == "0")
            {
                ////不显示全部项
                this.ddlCountry.Items.Add("");
            }
            else
            {
                string strWhere = string.Format(" SPRAS={0} ", this.GoalUserInfo.Spras);

                BLL.HY_BMS_CountryDesc bll = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_CountryDesc>();
                DataSet ds = bll.GetList(strWhere);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<Model.HY_BMS_CountryDesc> list = bll.DataTableToList(ds.Tables[0]);
                    this.ddlCountry.Items.Add("");
                    ListItem option = new ListItem();
                    foreach (var item in list)
                    {
                        option = new ListItem(item.CountryName, item.CountryID);
                        this.ddlCountry.Items.Add(option);
                    }
                }
                ////默认选中值
                if (!string.IsNullOrEmpty(SelectedVal))
                {
                    ListItem selectIteme = this.ddlCountry.Items.FindByValue(this.SelectedVal);
                    if (selectIteme != null)
                    {
                        selectIteme.Selected = true;
                    }
                }

                //// 增加onchange事件
                if (!string.IsNullOrEmpty(OnClientChange))
                {
                    this.ddlCountry.Attributes.Add("onchange", this.OnClientChange);
                }
            }
        }
    }
}