﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvinceCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.GeographicCtr.ProvinceCtr" %>
<asp:DropDownList ID="ddlProvice" runat="server"></asp:DropDownList>
<script >
    if ('<%=this.ControlName%>' != "") {
        $("#<%=this.ddlProvice.ClientID%>").attr("name", "<%=this.ControlName%>");
    }
</script>