﻿// 
// 创建人：宋欣
// 创建时间：2015-02-08
// 功能：左边菜单
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl
{
    public partial class LeftSide : BaseUserControl
    {
        public IList<Model.HY_BMS_Menu> Menus;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindMenu();
            }
        }

        private void BindMenu()
        {
            if (HttpContext.Current.Session["UserMenu"] == null)
            {
                BLL.UserMenu bll = Factory.BLLFactory.CreateInstance<BLL.UserMenu>();
                DataSet ds = bll.GetUserMenu(this.GoalUserInfo.Users.UserCode, this.GoalUserInfo.Spras);
                HttpContext.Current.Session["UserMenu"] = bll.DataTableToList(ds.Tables[0]);
            }
            Menus = (IList<Model.HY_BMS_Menu>) HttpContext.Current.Session["UserMenu"];
            ////绑定一级菜单
            this.repeaterMenu.DataSource =
                Menus.Where(item => item.ParentID == "0").OrderBy(item => item.OrderIndex).ToList();
            this.repeaterMenu.DataBind();
        }

        /// <summary>
        ///     绑定子菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repeaterMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hidParentID = e.Item.FindControl("hidParentID") as HiddenField;
                if (!string.IsNullOrEmpty(hidParentID.Value))
                {
                    Repeater repeaterChildMenu = e.Item.FindControl("repeaterChildMenu") as Repeater;
                    repeaterChildMenu.DataSource =
                        Menus.Where(item => item.ParentID.Equals(hidParentID.Value) && !item.IsHide)
                             .OrderBy(item => item.OrderIndex);
                    repeaterChildMenu.DataBind();
                }
            }
        }
    }
}