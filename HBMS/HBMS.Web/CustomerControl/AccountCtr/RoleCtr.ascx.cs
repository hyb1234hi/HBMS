﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl.AccountCtr
{
    public partial class RoleCtr : BaseUserControl
    {
        private string _controlName = "RoleID";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        private string _selectedVal = "";

        /// <summary>
        ///     选中的值
        /// </summary>
        public string SelectedVal
        {
            get { return _selectedVal; }
            set { _selectedVal = value; }
        }

        /// <summary>
        ///     样式
        /// </summary>
        public string CSS
        {
            get { return _css; }
            set { _css = value; }
        }

        private string _css = "select";

        private string _onClientChange = "";



        /// <summary>
        /// 控件的前台ID
        /// </summary>
        public string ClientControlID
        {
            get { return this.ddlRole.ClientID; }
        }

        /// <summary>
        /// 前台change事件
        /// </summary>
        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        ///     绑定数据
        /// </summary>
        private void BindData()
        {
            this.ddlRole.CssClass = this.CSS;

            BLL.View_RoleDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_RoleDesc>();
            DataSet ds = bll.GetList(string.Format(" SPRAS={0} ", this.GoalUserInfo.Spras));
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_RoleDesc> list = bll.DataTableToList(ds.Tables[0]);
                this.ddlRole.Items.Add("");
                ListItem option = new ListItem();
                foreach (var item in list)
                {
                    option = new ListItem(
                                          item.IsUse
                                              ? item.RoleName
                                              : (item.RoleName + "(" + Resources.Lbl.lblDisable + ")"), item.RoleID.ToString());
                    if (!item.IsUse)
                    {
                        option.Attributes.Add("class", "jinyong");
                    }
                    this.ddlRole.Items.Add(option);
                }
            }


            ////默认选中值
            if (!string.IsNullOrEmpty(SelectedVal))
            {
                ListItem selectIteme = this.ddlRole.Items.FindByValue(this.SelectedVal);
                if (selectIteme != null)
                {
                    selectIteme.Selected = true;
                }
            }

            //// 增加onchange事件
            if (!string.IsNullOrEmpty(OnClientChange))
            {
                this.ddlRole.Attributes.Add("onchange", this.OnClientChange);
            }
        }
    }
}