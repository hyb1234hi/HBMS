﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HBMS.Common;

namespace HBMS.Web.CustomerControl.AccountCtr
{
    public partial class SexCtr : BaseUserControl
    {
        private string _controlName = "Sex";

        /// <summary>
        /// 控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.ddlSexCtr.Items.Add(new ListItem(Resources.Lbl.lblMale, ((int)AccountSex.男).ToString()));
                this.ddlSexCtr.Items.Add(new ListItem(Resources.Lbl.lblFemale, ((int)AccountSex.女).ToString()));
            }
            
        }

    }
}