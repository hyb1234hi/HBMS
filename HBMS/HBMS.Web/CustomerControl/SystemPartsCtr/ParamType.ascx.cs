﻿// 
// 创建人：宋欣
// 创建时间：2015-02-06
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl.SystemPartsCtr
{
    public partial class ParamType : BaseUserControl
    {
        private string _controlName = "ParamTypeID";
        private string _controlID = "ddlParamTypeID";
        private string _onClientChange = "";
        private string _selectedVal = "";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        /// <summary>
        ///     控件ID
        /// </summary>
        public string ControlID
        {
            get { return _controlID; }
            set { _controlID = value; }
        }

        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }

        /// <summary>
        /// 选中值
        /// </summary>
        public string SelectedVal
        {
            get { return _selectedVal; }
            set { _selectedVal = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }
    }
}