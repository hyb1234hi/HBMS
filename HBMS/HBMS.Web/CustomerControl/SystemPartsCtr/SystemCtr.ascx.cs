﻿// 
// 创建人：宋欣
// 创建时间：2015-02-04
// 功能：系统控件
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl.SystemPartsCtr
{
    public partial class SystemCtr : BaseUserControl
    {
        private string _controlName = "SystemID";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        private string _selectedVal = "";

        /// <summary>
        ///     选中的值
        /// </summary>
        public string SelectedVal
        {
            get { return _selectedVal; }
            set { _selectedVal = value; }
        }

        /// <summary>
        ///     样式
        /// </summary>
        public string CSS
        {
            get { return _css; }
            set { _css = value; }
        }

        private string _css = "select";

        private string _onClientChange = "";



        /// <summary>
        /// 控件的前台ID
        /// </summary>
        public string ClientControlID
        {
            get { return this.ddlSystem.ClientID; }
        }

        /// <summary>
        /// 前台change事件
        /// </summary>
        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }

        /// <summary>
        ///     是否显示全部选项，默认为0，不显示
        /// </summary>
        private string _showAllOption = "0";
        /// <summary>
        ///     是否显示全部选项，默认为0，不显示
        /// </summary>
        public string ShowAllOption
        {
            get { return _showAllOption; }
            set { _showAllOption = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        ///     绑定数据
        /// </summary>
        private void BindData()
        {
            this.ddlSystem.CssClass = this.CSS;
            if (ShowAllOption == "0")
            {
                ////不显示全部项
                this.ddlSystem.Items.Add("");
            }
            else
            {


                BLL.View_SystemDesc bll = Factory.BLLFactory.CreateInstance<BLL.View_SystemDesc>();
                DataSet ds = bll.GetList(string.Format(" SPRAS={0} ", this.GoalUserInfo.Spras));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<Model.View_SystemDesc> list = bll.DataTableToList(ds.Tables[0]);
                    this.ddlSystem.Items.Add("");
                    ListItem option = new ListItem();
                    foreach (var item in list)
                    {
                        option = new ListItem(
                            item.IsUse
                                ? item.SystemName
                                : (item.SystemName + "(" + Resources.Lbl.lblDisable + ")"), item.SystemID.ToString());
                        if (!item.IsUse)
                        {
                            option.Attributes.Add("class", "jinyong");
                        }
                        this.ddlSystem.Items.Add(option);
                    }
                }


                ////默认选中值
                if (!string.IsNullOrEmpty(SelectedVal))
                {
                    ListItem selectIteme = this.ddlSystem.Items.FindByValue(this.SelectedVal);
                    if (selectIteme != null)
                    {
                        selectIteme.Selected = true;
                    }
                }
            }

            //// 增加onchange事件
            if (!string.IsNullOrEmpty(OnClientChange))
            {
                this.ddlSystem.Attributes.Add("onchange", this.OnClientChange);
            }
        }
    }
}