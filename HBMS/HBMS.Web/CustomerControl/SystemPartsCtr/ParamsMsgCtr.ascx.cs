﻿// 
// 创建人：宋欣
// 创建时间：2015-03-03
// 功能：参数消息发送范围
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.CustomerControl.SystemPartsCtr
{
    public partial class ParamsMsgCtr : BaseUserControl
    {
        private string _controlName = "ParamMsg";
        private string _controlID = "ddlParamMsg";
        private string _onClientChange = "";
        private string _selectedVal = "";
        private string _css = "select wp100";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        /// <summary>
        ///     控件ID
        /// </summary>
        public string ControlID
        {
            get { return _controlID; }
            set { _controlID = value; }
        }

        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }

        /// <summary>
        ///     选中值
        /// </summary>
        public string SelectedVal
        {
            get { return _selectedVal; }
            set { _selectedVal = value; }
        }

        /// <summary>
        ///     样式
        /// </summary>
        public string CSS
        {
            get { return _css; }
            set { _css = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }
    }
}