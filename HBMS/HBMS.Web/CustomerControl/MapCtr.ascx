﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MapCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.MapCtr" %>
<%@ Import Namespace="HBMS.Common" %>
<div id="map_container"></div>
<div id="map_display_params">
    <div class="dtable dtitle">
        <ul>
            <li class="drow">
                <span class="map_airport">机场</span>
                <span class="map_airport">设备名</span>
                <span class="map_airport">部件名</span>
                <span class="map_params">参数</span>
                <span class="map_val">值</span>
                <span class="map_time">记录时间</span>
            </li>
        </ul>
    </div>
    <div class="dtable jingshi">
        <ul id="display_jingshi">
            <%--<li class="drow">
                <span class="map_airport">北京首都机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">武汉天河国际机场</span>
                <span class="map_params">缓慢左转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">上海虹桥国际机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">武汉天河国际机场</span>
                <span class="map_params">缓慢左转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">上海虹桥国际机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>--%>
        </ul>
    </div>
    <div class="dtable gaoji">
        <ul id="display_gaoji">
            <%--<li class="drow">
                <span class="map_airport">北京首都机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">武汉天河国际机场</span>
                <span class="map_params">缓慢左转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">上海虹桥国际机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">武汉天河国际机场</span>
                <span class="map_params">缓慢左转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">上海虹桥国际机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>--%>
        </ul>
    </div>
    <div class="dtable tixing">
        <ul id="display_tixing">
            <%--<li class="drow">
                <span class="map_airport">北京首都机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">武汉天河国际机场</span>
                <span class="map_params">缓慢左转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">上海虹桥国际机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">武汉天河国际机场</span>
                <span class="map_params">缓慢左转速度</span>
                <span class="map_val">0</span>
            </li>
            <li class="drow">
                <span class="map_airport">上海虹桥国际机场</span>
                <span class="map_params">缓慢右转速度</span>
                <span class="map_val">0</span>
            </li>--%>
        </ul>
    </div>
</div>
<script type="text/javascript">
    function getDisplayParamsHtml(result) {
        ////获取显示的html
        var html = "";
        if (result != undefined && result != "") {
            if (result.data != undefined && result.data != "" && result.data.length > 0) {
                for (var i in result.data) {
                    html += '<li class="drow">';
                    html += '<span class="map_airport">' + result.data[i].AirPortName + '</span>';
                    html += '  <span class="map_params">' + result.data[i].EquipmentName + '</span>';
                    html += '  <span class="map_params">' + result.data[i].PartsName + '</span>';
                    html += '  <span class="map_params">' + result.data[i].ParamName + '</span>';
                    html += '  <span class="map_val">' + result.data[i].ParamStateDesc + '</span>';
                    html += '  <span class="map_time">' + result.data[i].SubDatetime + '</span>';
                    html += '</li>';
                }
            }
        }
        return html;
    }
    
    function getDisplayParamsData() {
        $.post("/CustomerControl/Ajax/ajax_Map.ashx", { action: "gaoji" }, function (result) {
            var html = getDisplayParamsHtml(result);
            $("#display_gaoji").html(html);
                $(".gaoji").MapParamsScroll();
        }, "json");

        $.post("/CustomerControl/Ajax/ajax_Map.ashx", { action: "tixing" }, function (result) {
            var html = getDisplayParamsHtml(result);
            $("#display_tixing").html(html);
            $(".tixing").MapParamsScroll();
        }, "json");

        $.post("/CustomerControl/Ajax/ajax_Map.ashx", { action: "jingshi" }, function (result) {
            var html = getDisplayParamsHtml(result);
            $("#display_jingshi").html(html);
            $(".jingshi").MapParamsScroll();
        }, "json");
    }

    getDisplayParamsData();
    ////每隔1分钟获取最新数据
    //setInterval(getDisplayParamsData, 60000);
    
</script>
<script type="text/javascript">
    // 百度地图API功能
    var mp = new BMap.Map("map_container");  // 创建地图实例  
    var point = new BMap.Point('<%=this.CurDimension%>', '<%=this.CurLongitude%>'); ////经度，维度
    mp.centerAndZoom(point, 5);
    mp.enableScrollWheelZoom();

    function AirportOverlay(point, airport, state, id, gjs) {
        // 机场覆盖物对象
        this._point = point;
        this._airport = airport;
        this._state = state;
        this._id = id;
        this._gjs = gjs;
    }

    AirportOverlay.prototype = new BMap.Overlay();

    AirportOverlay.prototype.initialize = function (map) {
        this._map = map;
        var ariportid = this._id;
        var ariportname = this._airport;
        var gjs = this._gjs;
        ////创建div
        var div = this._div = document.createElement("div");
        div.style.zIndex = new BMap.Overlay.getZIndex(this._point.lat);
        div.className = "airport-overlay";
        //////填充内容
        var html = "";
        if (this._state == "<%=(int)AirePortStaetEnum.正常%>") {
            html += '<img src="/images/circle-green.png">';
        } else if (this._state == "<%=(int)AirePortStaetEnum.提醒%>") {
            html += '<img src="/images/circle-yellow.png">';
        } else if (this._state == "<%=(int)AirePortStaetEnum.警示%>") {
            html += '<img src="/images/circle-orange.png">';
        } else if (this._state == "<%=(int)AirePortStaetEnum.告急%>") {
            html += '<img src="/images/circle-red.png">';
        }
        else {
            html += '<img src="/images/circle-gray.png">';
        }
        html += "<span>" + this._airport + "</span>";
        //if (gjs != undefined && gjs != "" && gjs.length > 0) {
        //    html += '<ul>';
        //    for (var i in gjs) {
        //        html += '<li>' + gjs[i].ParamName + '(' + gjs[i].ParamValue + ')' + '</li>';
        //    }
        //    html += "</ul>";
        //}
        div.innerHTML = html;

        //////创建箭头
        var arrow = this._arrow = document.createElement("div");
        arrow.style.background = 'url("/images/label.png") no-repeat  bottom';
        arrow.style.position = "absolute";
        arrow.style.width = "11px";
        arrow.style.height = "10px";
        arrow.style.top = "34px";
        arrow.style.left = "10px";
        arrow.style.overflow = "hidden";
        div.appendChild(arrow);

        div.onclick = function () {
            $.post("Ajax/ajax_Equipment.ashx", { airport: ariportid }, function (result) {
                var arthtml = '';
                arthtml += '<h4>' + ariportname + '</h4>';
                arthtml += '<table style="width:400px;" class="tb-data">';
                arthtml += '<thead>';
                arthtml += '<tr>';
                arthtml += '<th><%=Resources.Lbl.lblEquipmentCode%></th>';
                arthtml += '<th><%=Resources.Lbl.lblEquipmentName%></th>';
                arthtml += '<th><%=Resources.Lbl.lblState%></th>';
                arthtml += '<th><%=Resources.Lbl.lblOperation%></th>';
                arthtml += '</tr>';
                arthtml += '</thead>';
                var rowspan = 1; ////合并行
                if (result != undefined && result != "") {
                    for (var i in result) {
                        //rowspan = result[i].Params.length;
                        arthtml += '<tr>';
                        arthtml += '<td rowspan="' + rowspan + '">' + result[i].EquipmentCode + '</td>';
                        arthtml += '<td rowspan="' + rowspan + '">' + result[i].EquipmentName + '</td>';
                        arthtml += '<td rowspan="' + rowspan + '">' + result[i].EquipmentStateDesc + '</td>';
                        arthtml += '<td rowspan="' + rowspan + '"><a href="/Map/Bai/Params.aspx?aid=' + result[i].AirPortID + '&eid=' + result[i].EquipmentID + '" target="_blank"><img src="/images/information-blue.png"/></a></td>';
                        //arthtml += '<td>' + result[i].Params[0].ParamCode + '</td>';
                        //arthtml += '<td>' + result[i].Params[0].ParamName + '</td>';
                        //arthtml += '<td>' + result[i].Params[0].ParamValue + '</td>';
                        arthtml += '</tr>';
                        //for (var j = 1; j < result[i].Params.length; j++) {
                        //    arthtml += '<tr>';
                        //    arthtml += '<td>' + result[i].Params[0].ParamCode + '</td>';
                        //    arthtml += '<td>' + result[i].Params[0].ParamName + '</td>';
                        //    arthtml += '<td>' + result[i].Params[0].ParamValue + '</td>';
                        //    arthtml += '</tr>';
                        //}
                    }
                }
                arthtml += '</table>';
                MessageBox.close();
                MessageBox.dialog({
                    content: arthtml,
                    lock: true,
                    background: '#ddd', // 背景色
                    opacity: 0.87,	// 透明度
                    //  follow: div,
                    padding: 0,
                });
            }, "json");

        };

        div.onmouseout = function () {

        }

        mp.getPanes().labelPane.appendChild(div);

        return div;
    };

    AirportOverlay.prototype.draw = function () {
        ////地图画覆盖物
        var map = this._map;
        var pixel = map.pointToOverlayPixel(this._point);
        this._div.style.left = pixel.x - parseInt(this._arrow.style.left) + "px";
        this._div.style.top = pixel.y - 30 + "px";
    };


    var interval = setInterval(getpoint, 10000);
    getpoint();
    function stopget() {
        clearInterval(interval);
    }

    function getpoint() {
        ////获取数据点

        function drawmap(result) {
            ////画图
            for (var i in result) {
                ////机场名字
                var airport = result[i].AirPortName;
                ////状态
                var state = result[i].EquipmentState;
                ////坐标点
                var point = new BMap.Point(result[i].Dimension, result[i].Longitude);
                //// 机场ID
                var id = result[i].AirPortID;
                ////告急参数
                var gjs = result[i].GJ;
                ////创建覆盖物
                var myCompOverlay = new AirportOverlay(point, airport, state, id, gjs);
                mp.addOverlay(myCompOverlay);
            }
        }
        mp.clearOverlays();
        $.post("Ajax/ajax_Map.ashx", function (result) {
            if (result != undefined && result != "") {
                drawmap(result);
            }
        }, "json");
    }


    getpoint();

</script>
