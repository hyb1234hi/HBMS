﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UseStateCtr.ascx.cs" Inherits="HBMS.Web.CustomerControl.UseStateCtr" %>
<select id="<%=this.ControlID %>" name="<%=this.ControlName %>" class="select">
    <option value=""></option>
    <option value="1" selected="selected"><%=Resources.Lbl.lblYes %></option>
    <option value="0"><%=Resources.Lbl.lblNo %></option>
</select>