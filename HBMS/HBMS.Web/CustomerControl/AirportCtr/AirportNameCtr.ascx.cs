﻿// 
// 创建人：宋欣
// 创建时间：2015-02-02
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using System.Data;
using System.Web.UI.WebControls;

namespace HBMS.Web.CustomerControl.AirportCtr
{
    public partial class AirportNameCtr : BaseUserControl
    {
        private string _controlName = "AirportID";
        private string _selectedValue = "";
        private string _css = "select wp100";
        private string _onClientChange = "";

        /// <summary>
        ///     控件名字
        /// </summary>
        public string ControlName
        {
            get { return _controlName; }
            set { _controlName = value; }
        }

        /// <summary>
        ///     选中的值
        /// </summary>
        public string SelectedValue
        {
            get { return _selectedValue; }
            set { _selectedValue = value; }
        }

        /// <summary>
        ///     样式
        /// </summary>
        public string CSS
        {
            get { return _css; }
            set { _css = value; }
        }

        /// <summary>
        /// 获取控件的客户端ID
        /// </summary>
        public string ClientControlID
        {
            get { return this.ddlAiportName.ClientID; }
        }

        /// <summary>
        /// 前台change事件
        /// </summary>
        public string OnClientChange
        {
            get { return _onClientChange; }
            set { _onClientChange = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.ddlAiportName.CssClass = CSS;
                this.BindAirport();
            }
        }

        private void BindAirport()
        {
            DataSet ds = Factory.BLLFactory.CreateInstance<BLL.HY_BMS_AirPortDesc>()
                                .GetList(string.Format(" SPRAS={0} ", GoalUserInfo.Spras));
            this.ddlAiportName.DataSource = ds;
            this.ddlAiportName.DataTextField = "AirPortName";
            this.ddlAiportName.DataValueField = "AirPortID";
            this.ddlAiportName.DataBind();
            this.ddlAiportName.Items.Insert(0, "");
            if (!string.IsNullOrEmpty(SelectedValue))
            {
                ////如果有选中值，默认选上
                ListItem selected = this.ddlAiportName.Items.FindByValue(SelectedValue);
                if (selected != null)
                {
                    selected.Selected = true;
                }
            }

            //// 增加onchange事件
            if (!string.IsNullOrEmpty(OnClientChange))
            {
                this.ddlAiportName.Attributes.Add("onchange", this.OnClientChange);
            }
        }
    }
}