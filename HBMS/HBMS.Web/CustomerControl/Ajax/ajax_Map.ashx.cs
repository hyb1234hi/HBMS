﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using HBMS.Common;
using HBMS.Factory;
using Newtonsoft.Json.Linq;

namespace HBMS.Web.CustomerControl.Ajax
{
    /// <summary>
    /// ajax_Map 的摘要说明
    /// </summary>
    public class ajax_Map : BaseHandler
    {
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            if (Action.Equals("gaoji"))
            {
                WriteResult = this.GaoJi();
            }
            else if (Action.Equals("tixing"))
            {
                WriteResult = this.TingXing();
            }
            else if (Action.Equals("jingshi"))
            {
                WriteResult = this.JingShi();
            }
            ResponseWrite();
        }

        /// <summary>
        /// 告急
        /// </summary>
        /// <returns></returns>
        public string GaoJi()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ", this.GoalUserInfo.Spras);
            where.AppendFormat(" AND ParamState={0}", (int) AirePortStaetEnum.告急);
            return GetParamValues(where.ToString());
        }

        /// <summary>
        /// 提醒
        /// </summary>
        /// <returns></returns>
        public string TingXing()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ", this.GoalUserInfo.Spras);
            where.AppendFormat(" AND ParamState={0}", (int)AirePortStaetEnum.提醒);
            return GetParamValues(where.ToString());
        }

        /// <summary>
        /// 警示
        /// </summary>
        /// <returns></returns>
        public string JingShi()
        {
            ////查询条件
            StringBuilder where = new StringBuilder();
            where.AppendFormat(" SPRAS ={0} ", this.GoalUserInfo.Spras);
            where.AppendFormat(" AND ParamState={0}", (int)AirePortStaetEnum.警示);
            return GetParamValues(where.ToString());

        }

        /// <summary>
        /// 获取参数值
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public string GetParamValues(string where)
        {
            BLL.View_EquipmentParamValues bll = BLLFactory.CreateInstance<BLL.View_EquipmentParamValues>();
            ////总行数
            Pager.TotalCount = bll.GetRecordCount(where);
            ////分页查询
            DataSet ds = bll.GetListByPage(where, " SubDatetime desc  ", 0, 10);
            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Model.View_EquipmentParamValues> list = bll.DataTableToList(ds.Tables[0]);
                if (list.Count > 0)
                {
                    ////返回查询结果json
                    return JToken.FromObject(new
                    {
                        data = from l in list
                               select new
                               {
                                   l.ID,
                                   l.AirPortID,
                                   l.AirPortName,
                                   l.EquipmentID,
                                   l.EquipmentName,
                                   l.SystemID,
                                   l.SystemName,
                                   l.PartsID,
                                   l.PartsName,
                                   l.ParamID,
                                   l.ParamName,
                                   l.ParamValue,
                                   SubDatetime = l.SubDatetime.ToDateHaveSFM(),
                                   l.ParamState,
                                   ParamStateDesc = l.ParamState.ParamStateDesc(),
                                   l.SPRAS,
                               },
                        totalCount = Pager.TotalCount,
                        totalPages = Pager.TotalPage,
                        visiblePages = Pager.VisiblePages,
                        startPage = Pager.PageIndex
                    }).ToString();
                }
            }
            ////如果没有返回空字符
            return NoDataJson();
        }

    }
}