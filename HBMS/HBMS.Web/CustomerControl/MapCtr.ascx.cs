﻿// 
// 创建人：宋欣
// 创建时间：2015-03-04
// 功能：地图公用控件
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;

namespace HBMS.Web.CustomerControl
{
    public partial class MapCtr : BaseUserControl
    {
        /// <summary>
        /// 维度
        /// </summary>
        protected string CurDimension = "117.3290140000";

        /// <summary>
        /// 经度
        /// </summary>
        protected string CurLongitude = "38.2236740000";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindLocation();
            }
        }

        /// <summary>
        ///     绑定经纬度
        /// </summary>
        private void BindLocation()
        {
            ////查找当前用户的默认机场
            Model.HY_BMS_Users curUser =
                Factory.BLLFactory.CreateInstance<BLL.HY_BMS_Users>().GetModel(this.GoalUserInfo.Users.UserCode);
            if (curUser != null && !string.IsNullOrEmpty(curUser.DefaultAirPortID))
            {
                ////查找机场的经纬度信息
                int airPortID = Convert.ToInt32(curUser.DefaultAirPortID);
                Model.HY_BMS_AirPorts airPorts =
                    Factory.BLLFactory.CreateInstance<BLL.HY_BMS_AirPorts>().GetModel(airPortID);
                this.CurDimension = airPorts.Dimension;
                this.CurLongitude = airPorts.Longitude;
            }
        }
    }
}