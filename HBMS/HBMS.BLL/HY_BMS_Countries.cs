﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Countries
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:13   N/A    初版
// 
using System;
using System.Data;
using System.Collections.Generic;
using HBMS.Common;
using HBMS.Model;
namespace HBMS.BLL
{
	/// <summary>
	/// HY_BMS_Countries
	/// </summary>
	public partial class HY_BMS_Countries
	{
		private readonly HBMS.DAL.HY_BMS_Countries dal=new HBMS.DAL.HY_BMS_Countries();
		public HY_BMS_Countries()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string ID,int ContinentID)
		{
			return dal.Exists(ID,ContinentID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.HY_BMS_Countries model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_Countries model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string ID,int ContinentID)
		{
			
			return dal.Delete(ID,ContinentID);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_Countries GetModel(string ID,int ContinentID)
		{
			
			return dal.GetModel(ID,ContinentID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public HBMS.Model.HY_BMS_Countries GetModelByCache(string ID,int ContinentID)
		{
			
			string CacheKey = "HY_BMS_CountriesModel-" + ID+ContinentID;
			object objModel = HBMS.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID,ContinentID);
					if (objModel != null)
					{
						int ModelCache = HBMS.Common.ConfigHelper.GetConfigInt("ModelCache");
						HBMS.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (HBMS.Model.HY_BMS_Countries)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<HBMS.Model.HY_BMS_Countries> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<HBMS.Model.HY_BMS_Countries> DataTableToList(DataTable dt)
		{
			List<HBMS.Model.HY_BMS_Countries> modelList = new List<HBMS.Model.HY_BMS_Countries>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				HBMS.Model.HY_BMS_Countries model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

