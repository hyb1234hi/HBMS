﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_ParamDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/3 23:29:18   N/A    初版
// 
using System;
using System.Data;
using System.Collections.Generic;
namespace HBMS.BLL
{
	/// <summary>
	/// HY_BMS_ParamDesc
	/// </summary>
	public partial class HY_BMS_ParamDesc
	{
		private readonly HBMS.DAL.HY_BMS_ParamDesc dal=new HBMS.DAL.HY_BMS_ParamDesc();
		public HY_BMS_ParamDesc()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ParamID,int SPRAS)
		{
			return dal.Exists(ParamID,SPRAS);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(HBMS.Model.HY_BMS_ParamDesc model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(HBMS.Model.HY_BMS_ParamDesc model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ParamID,int SPRAS)
		{
			
			return dal.Delete(ParamID,SPRAS);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public HBMS.Model.HY_BMS_ParamDesc GetModel(int ParamID,int SPRAS)
		{
			
			return dal.GetModel(ParamID,SPRAS);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public HBMS.Model.HY_BMS_ParamDesc GetModelByCache(int ParamID,int SPRAS)
		{
			
			string CacheKey = "HY_BMS_ParamDescModel-" + ParamID+SPRAS;
			object objModel = Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ParamID,SPRAS);
					if (objModel != null)
					{
						int ModelCache = Common.ConfigHelper.GetConfigInt("ModelCache");
						Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (HBMS.Model.HY_BMS_ParamDesc)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<HBMS.Model.HY_BMS_ParamDesc> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<HBMS.Model.HY_BMS_ParamDesc> DataTableToList(DataTable dt)
		{
			List<HBMS.Model.HY_BMS_ParamDesc> modelList = new List<HBMS.Model.HY_BMS_ParamDesc>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				HBMS.Model.HY_BMS_ParamDesc model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

