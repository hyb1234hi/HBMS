﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HBMS.BLL
{
    public partial class HY_BMS_MenuDesc
    { /// <summary>
        ///     保存机场多语言
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool SaveLanguage(List<Model.HY_BMS_MenuDesc> list)
        {
            if (list.Count > 0)
            {
                List<string> sqllist = new List<string>();
                sqllist.Add("delete from HY_BMS_MenuDesc");
                string sql = "insert into HY_BMS_MenuDesc values({0},{1},'{2}')";
                sqllist.AddRange(
                    list.Select(
                        item => string.Format(sql, item.MenuID, item.spras, item.MenuName)));
                return dal.SaveLanguage(sqllist);
            }
            return false;
        }
    }
}
