﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HBMS.BLL
{
    /// <summary>
    /// View_RoleMenu
    /// </summary>
    public partial class View_RoleMenu
    {
        /// <summary>
        /// 新增角色菜单
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="menus"></param>
        /// <returns></returns>
        public bool AddRoleMenu(int roleID, string menus)
        {
            return dal.AddRoleMenu(roleID, menus);
        }
    }
}
