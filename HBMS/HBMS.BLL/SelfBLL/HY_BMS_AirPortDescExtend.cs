﻿// 
// 创建人：宋欣
// 创建时间：2015-03-07
// 功能：机场多语言
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Linq;

namespace HBMS.BLL
{
    public partial class HY_BMS_AirPortDesc
    {
        /// <summary>
        ///     保存机场多语言
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool SaveLanguage(List<Model.HY_BMS_AirPortDesc> list)
        {
            if (list.Count > 0)
            {
                List<string> sqllist = new List<string>();
                sqllist.Add("delete from HY_BMS_AirPortDesc");
                string sql = "insert into HY_BMS_AirPortDesc values({0},{1},'{2}','{3}')";
                sqllist.AddRange(
                    list.Select(
                        item => string.Format(sql, item.AirPortID, item.SPRAS, item.AirPortName, item.AirPortAddress)));
                return dal.SaveLanguage(sqllist);
            }
            return false;
        }
    }
}