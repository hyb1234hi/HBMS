﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;
using System.Data;

namespace HBMS.BLL
{
    public class UserMenu
    {
        private readonly DAL.UserMenu dal = new DAL.UserMenu();

        /// <summary>
        ///     获取用户菜单
        /// </summary>
        /// <param name="userCode">用户编号</param>
        /// <param name="spras">当前语言</param>
        /// <returns></returns>
        public DataSet GetUserMenu(string userCode,int spras)
        {
            return dal.GetUserMenu(userCode,spras);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<HBMS.Model.HY_BMS_Menu> DataTableToList(DataTable dt)
        {
            List<HBMS.Model.HY_BMS_Menu> modelList = new List<HBMS.Model.HY_BMS_Menu>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                HBMS.Model.HY_BMS_Menu model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }
    }
}