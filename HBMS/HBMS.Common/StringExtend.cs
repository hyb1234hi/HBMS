﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HBMS.Common
{
    public static  class StringExtend
    {
        /// <summary>
        /// 异常信息过滤
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ReplaceExceptionMessage(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return obj.ToString().Trim().Replace('\'', ' ');
            }
            return "";
        }

        /// <summary>
        /// 查询条件过滤
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string FilterQueryCondition(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return obj.ToString().Trim(); 
            }
            return "";
        }

        /// <summary>
        /// 转换时间为yyyy-MM-dd
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateNoSFM(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return Convert.ToDateTime(obj).ToString("yyyy-MM-dd");
            }
            return "";
        }

        /// <summary>
        /// 转换时间为yyyy-MM-dd HH:mm
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateHaveSF(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return Convert.ToDateTime(obj).ToString("yyyy-MM-dd HH:mm");
            }
            return "";
        }

        /// <summary>
        /// 转换时间为yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateHaveSFM(this object obj)
        {
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return Convert.ToDateTime(obj).ToString("yyyy-MM-dd HH:mm:ss");
            }
            return "";
        }
    }
}
