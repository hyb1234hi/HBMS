﻿// 
// 创建人：宋欣
// 创建时间：2015-02-12
// 功能：所有状态枚举
// Copyright (c) 2014 宋欣. All rights reserved.
// 

namespace HBMS.Common
{
    /// <summary>
    /// 机场枚举
    /// </summary>
    public enum AirePortStaetEnum
    {
        断网=0,

        正常 = 1,

        提醒 = 2,

        警示 = 3,

        告急 = 4
    }
}