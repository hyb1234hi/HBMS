﻿// 
// 创建人：宋欣
// 创建时间：2015-02-06
// 功能：参数枚举
// Copyright (c) 2014 宋欣. All rights reserved.
// 

namespace HBMS.Common
{
    public enum ParamTypeEnum
    {
        布尔 = 2,

        数值 = 1
    }

    public enum  ParamMsgFWEnum
    {
        全部=0,
        本设备=1,
    }

    /// <summary>
    /// 参数状态
    /// </summary>
    public enum ParamStateEnum
    {
        正常 = 1,
        异常 = 2,
        过高 = 3,
        过低 = 4
    }
}