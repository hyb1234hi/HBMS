﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentParamValues
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/8/15 1:00:01   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_EquipmentParamValues:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_EquipmentParamValues
	{
		public View_EquipmentParamValues()
		{}
		#region Model
		private int _id;
		private DateTime? _subdatetime;
		private int? _paramvalue;
		private int? _paramstate;
		private DateTime? _recordtime;
		private int? _datapack;
		private string _airportid;
		private string _airportcode;
		private string _airportname;
		private string _dimension;
		private string _longitude;
		private int _equipmentid;
		private string _equipmentcode;
		private string _equipmentname;
		private int _spras;
		private int _paramid;
		private int _paramcode;
		private string _paramname;
		private int? _tagtype;
		private string _tagname;
		private int? _tagid;
		private int? _tagbitn;
		private int? _warnlevel;
		private int? _normalvalue;
		private int? _multiple;
		private int? _minvalue;
		private int? _maxvalue;
		private int? _lowlevelvalue;
		private int? _highlevelvalue;
		private DateTime? _epsubdatetime;
		private int? _methodid;
		private string _units;
		private int _paramtype;
		private int _partsparamno;
		private string _paramtypename;
		private int _partsid;
		private string _partsname;
		private int _systemid;
		private string _systemname;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SubDatetime
		{
			set{ _subdatetime=value;}
			get{return _subdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ParamValue
		{
			set{ _paramvalue=value;}
			get{return _paramvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ParamState
		{
			set{ _paramstate=value;}
			get{return _paramstate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? RecordTime
		{
			set{ _recordtime=value;}
			get{return _recordtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DataPack
		{
			set{ _datapack=value;}
			get{return _datapack;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortCode
		{
			set{ _airportcode=value;}
			get{return _airportcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortName
		{
			set{ _airportname=value;}
			get{return _airportname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Dimension
		{
			set{ _dimension=value;}
			get{return _dimension;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Longitude
		{
			set{ _longitude=value;}
			get{return _longitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentCode
		{
			set{ _equipmentcode=value;}
			get{return _equipmentcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentName
		{
			set{ _equipmentname=value;}
			get{return _equipmentname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ParamID
		{
			set{ _paramid=value;}
			get{return _paramid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ParamCode
		{
			set{ _paramcode=value;}
			get{return _paramcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParamName
		{
			set{ _paramname=value;}
			get{return _paramname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TagType
		{
			set{ _tagtype=value;}
			get{return _tagtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TagName
		{
			set{ _tagname=value;}
			get{return _tagname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TagID
		{
			set{ _tagid=value;}
			get{return _tagid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TagBitn
		{
			set{ _tagbitn=value;}
			get{return _tagbitn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? WarnLevel
		{
			set{ _warnlevel=value;}
			get{return _warnlevel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NormalValue
		{
			set{ _normalvalue=value;}
			get{return _normalvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Multiple
		{
			set{ _multiple=value;}
			get{return _multiple;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MinValue
		{
			set{ _minvalue=value;}
			get{return _minvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MaxValue
		{
			set{ _maxvalue=value;}
			get{return _maxvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LowLevelValue
		{
			set{ _lowlevelvalue=value;}
			get{return _lowlevelvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? HighLevelValue
		{
			set{ _highlevelvalue=value;}
			get{return _highlevelvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? EPSubDatetime
		{
			set{ _epsubdatetime=value;}
			get{return _epsubdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MethodID
		{
			set{ _methodid=value;}
			get{return _methodid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Units
		{
			set{ _units=value;}
			get{return _units;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ParamType
		{
			set{ _paramtype=value;}
			get{return _paramtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int PartsParamNo
		{
			set{ _partsparamno=value;}
			get{return _partsparamno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParamTypeName
		{
			set{ _paramtypename=value;}
			get{return _paramtypename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int PartsID
		{
			set{ _partsid=value;}
			get{return _partsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PartsName
		{
			set{ _partsname=value;}
			get{return _partsname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SystemID
		{
			set{ _systemid=value;}
			get{return _systemid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SystemName
		{
			set{ _systemname=value;}
			get{return _systemname;}
		}
		#endregion Model

	}
}

