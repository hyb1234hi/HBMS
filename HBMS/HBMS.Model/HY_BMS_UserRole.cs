﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_UserRole
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:21   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_UserRole:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_UserRole
	{
		public HY_BMS_UserRole()
		{}
		#region Model
		private string _usecode;
		private int _roleid;
		/// <summary>
		/// 
		/// </summary>
		public string UseCode
		{
			set{ _usecode=value;}
			get{return _usecode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RoleID
		{
			set{ _roleid=value;}
			get{return _roleid;}
		}
		#endregion Model

	}
}

