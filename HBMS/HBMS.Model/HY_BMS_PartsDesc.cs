﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_PartsDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:16   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_PartsDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_PartsDesc
	{
		public HY_BMS_PartsDesc()
		{}
		#region Model
		private int _partsid;
		private int _spras;
		private string _partsname;
		/// <summary>
		/// 
		/// </summary>
		public int PartsID
		{
			set{ _partsid=value;}
			get{return _partsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PartsName
		{
			set{ _partsname=value;}
			get{return _partsname;}
		}
		#endregion Model

	}
}

