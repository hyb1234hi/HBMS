﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：当前登录用户的全局信息
// Copyright (c) 2014 宋欣. All rights reserved.
// 
using System;
using System.Collections.Generic;

namespace HBMS.Model
{
    [Serializable]
    public class CurrentUser
    {
        /// <summary>
        ///     当前系统语言
        /// </summary>
        public int Spras { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        public HY_BMS_Users Users { get; set; }

        /// <summary>
        /// 用户角色
        /// </summary>
        public List<int> RoleCollection { get; set; }
    }
}