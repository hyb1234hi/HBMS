﻿// 
// 创建人：宋欣
// 创建时间：2015-01-31
// 功能：分页实体
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System;
using HBMS.Common;

namespace HBMS.Model
{
    public class Pagination
    {
        /// <summary>
        ///     页索引
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        ///     每页行数
        /// </summary>
        public int PageSize
        {
            get
            {
                return !string.IsNullOrEmpty(ConfigHelper.GetAppSettingString("PageSize")) ? Convert.ToInt32(ConfigHelper.GetAppSettingString("PageSize")) : 20;
            }
        }

        /// <summary>
        ///     总行数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalPage
        {
            get { return TotalCount % PageSize == 0 ? TotalCount / PageSize : TotalCount/PageSize+1; }
        }

        /// <summary>
        ///     可见页数
        /// </summary>
        public int VisiblePages
        {
            get
            {
                return TotalPage > 5 ? 5 : TotalPage;
            }
        }

        /// <summary>
        /// 开始行索引
        /// </summary>
        public int StartIndex
        {
            get { return (PageIndex-1)*PageSize; }
        }

        /// <summary>
        /// 结束行索引
        /// </summary>
        public int EndIndex
        {
            get { return StartIndex + PageSize; }
        }
    }
}