﻿// 
// 创建人：宋欣
// 创建时间：2015-03-06
// 功能：ztree 实体
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;

namespace HBMS.Model
{
    public class Ztree
    {
        public string id;
        public string pId;
        public string name;
        public bool open;
        public bool isParent;
        public bool @checked;
        public List<Ztree> children;
    }
}