﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_System
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:19   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_System:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_System
	{
		public HY_BMS_System()
		{}
		#region Model
		private int _systemid;
		private bool _isuse;
		private int _orderindex;
		/// <summary>
		/// 
		/// </summary>
		public int SystemID
		{
			set{ _systemid=value;}
			get{return _systemid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int OrderIndex
		{
			set{ _orderindex=value;}
			get{return _orderindex;}
		}
		#endregion Model

	}
}

