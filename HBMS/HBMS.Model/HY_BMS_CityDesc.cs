﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_CityDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:13   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_CityDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_CityDesc
	{
		public HY_BMS_CityDesc()
		{}
		#region Model
		private string _cityid;
		private int _spras;
		private string _cityname;
		/// <summary>
		/// 
		/// </summary>
		public string CityID
		{
			set{ _cityid=value;}
			get{return _cityid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CityName
		{
			set{ _cityname=value;}
			get{return _cityname;}
		}
		#endregion Model

	}
}

