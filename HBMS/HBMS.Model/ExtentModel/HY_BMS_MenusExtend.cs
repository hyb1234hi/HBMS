﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：菜单扩展
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;

namespace HBMS.Model
{
    public partial class HY_BMS_Menu
    {
        /// <summary>
        ///     当前语言
        /// </summary>
        public int CurSpras { get; set; }

        /// <summary>
        ///     当前语言下菜单名字
        /// </summary>
        public string CureMenuName { get; set; }

        /// <summary>
        ///     所有语言菜单名称集合
        /// </summary>
        public List<HY_BMS_MenuDesc> MenuDescsCollection { get; set; }
    }
}