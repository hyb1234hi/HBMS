﻿// 
// 创建人：宋欣
// 创建时间：2015-02-02
// 功能：
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;

namespace HBMS.Model.ExtentModel
{
    public class HY_BMS_Equipment
    {
        /// <summary>
        /// 当前语言
        /// </summary>
        public int CurSpras { get; set; }

        /// <summary>
        /// 当前设备
        /// </summary>
        public string CurEquipmentName { get; set; }

        /// <summary>
        /// 当前机场名字
        /// </summary>
        public string AirProtName { get; set; }

        /// <summary>
        /// 所有设备描述集合
        /// </summary>
        public List<HY_BMS_EquipmentDesc> EquipmentDescsCollection { get; set; }
    }
}