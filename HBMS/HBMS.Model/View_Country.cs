﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_Country
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:23   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_Country:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_Country
	{
		public View_Country()
		{}
		#region Model
		private string _countryid;
		private int _spras;
		private string _countryname;
		private string _continentname;
		/// <summary>
		/// 
		/// </summary>
		public string CountryID
		{
			set{ _countryid=value;}
			get{return _countryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CountryName
		{
			set{ _countryname=value;}
			get{return _countryname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ContinentName
		{
			set{ _continentname=value;}
			get{return _continentname;}
		}
		#endregion Model

	}
}

