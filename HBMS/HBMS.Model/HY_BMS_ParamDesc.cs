﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_ParamDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/3 23:29:18   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_ParamDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_ParamDesc
	{
		public HY_BMS_ParamDesc()
		{}
		#region Model
		private int _paramid;
		private int _spras;
		private string _paramname;
		/// <summary>
		/// 
		/// </summary>
		public int ParamID
		{
			set{ _paramid=value;}
			get{return _paramid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParamName
		{
			set{ _paramname=value;}
			get{return _paramname;}
		}
		#endregion Model

	}
}

