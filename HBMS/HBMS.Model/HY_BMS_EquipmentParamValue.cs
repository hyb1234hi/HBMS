﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace HBMS.Model{
	 	//HY_BMS_EquipmentParamValue
		public class HY_BMS_EquipmentParamValue
	{
   		     
      	/// <summary>
		/// ID
        /// </summary>		
		private int _id;
        public int ID
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// EquipmentID
        /// </summary>		
		private int _equipmentid;
        public int EquipmentID
        {
            get{ return _equipmentid; }
            set{ _equipmentid = value; }
        }        
		/// <summary>
		/// paramID
        /// </summary>		
		private string _paramid;
        public string paramID
        {
            get{ return _paramid; }
            set{ _paramid = value; }
        }        
		/// <summary>
		/// SubDatetime
        /// </summary>		
		private DateTime _subdatetime;
        public DateTime SubDatetime
        {
            get{ return _subdatetime; }
            set{ _subdatetime = value; }
        }        
		/// <summary>
		/// ParamValue
        /// </summary>		
		private decimal _paramvalue;
        public decimal ParamValue
        {
            get{ return _paramvalue; }
            set{ _paramvalue = value; }
        }        
		/// <summary>
		/// State
        /// </summary>		
		private int _state;
        public int State
        {
            get{ return _state; }
            set{ _state = value; }
        }        
		   
	}
}

