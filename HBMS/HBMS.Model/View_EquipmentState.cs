﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentState
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/8/12 0:21:12   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_EquipmentState:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_EquipmentState
	{
		public View_EquipmentState()
		{}
		#region Model
		private int _id;
		private int _equipmentid;
		private DateTime? _subdatetime;
		private int _state;
		private string _equipmentname;
		private int _spras;
		private string _equipmentcode;
		private string _airportname;
		private int _airportid;
		private string _airportcode;
		private string _longitude;
		private string _dimension;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SubDatetime
		{
			set{ _subdatetime=value;}
			get{return _subdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentName
		{
			set{ _equipmentname=value;}
			get{return _equipmentname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentCode
		{
			set{ _equipmentcode=value;}
			get{return _equipmentcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortName
		{
			set{ _airportname=value;}
			get{return _airportname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortCode
		{
			set{ _airportcode=value;}
			get{return _airportcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Longitude
		{
			set{ _longitude=value;}
			get{return _longitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Dimension
		{
			set{ _dimension=value;}
			get{return _dimension;}
		}
		#endregion Model

	}
}

