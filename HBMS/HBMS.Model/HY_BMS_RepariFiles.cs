﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_RepariFiles
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/4/9 0:06:19   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_RepariFiles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_RepariFiles
	{
		public HY_BMS_RepariFiles()
		{}
		#region Model
		private int _id;
		private string _repairid;
		private string _filename;
		private string _filepath;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RepairID
		{
			set{ _repairid=value;}
			get{return _repairid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FileName
		{
			set{ _filename=value;}
			get{return _filename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FilePath
		{
			set{ _filepath=value;}
			get{return _filepath;}
		}
		#endregion Model

	}
}

