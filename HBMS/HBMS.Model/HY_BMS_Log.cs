﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Log
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:14   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_Log:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_Log
	{
		public HY_BMS_Log()
		{}
		#region Model
		private int _logid;
		private string _ip;
		private string _mac;
		private string _usercode;
		private DateTime _operdatetime;
		private string _opertype;
		private string _menuname;
		private string _funcname;
		private string _operresult;
		private string _docno;
		private string _itemid;
		/// <summary>
		/// 
		/// </summary>
		public int LogID
		{
			set{ _logid=value;}
			get{return _logid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IP
		{
			set{ _ip=value;}
			get{return _ip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Mac
		{
			set{ _mac=value;}
			get{return _mac;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserCode
		{
			set{ _usercode=value;}
			get{return _usercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime Operdatetime
		{
			set{ _operdatetime=value;}
			get{return _operdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OperType
		{
			set{ _opertype=value;}
			get{return _opertype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MenuName
		{
			set{ _menuname=value;}
			get{return _menuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FuncName
		{
			set{ _funcname=value;}
			get{return _funcname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OperResult
		{
			set{ _operresult=value;}
			get{return _operresult;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DocNo
		{
			set{ _docno=value;}
			get{return _docno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ItemID
		{
			set{ _itemid=value;}
			get{return _itemid;}
		}
		#endregion Model

	}
}

