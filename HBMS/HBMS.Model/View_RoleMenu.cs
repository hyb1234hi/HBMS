﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_RoleMenu
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/5 23:51:29   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_RoleMenu:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_RoleMenu
	{
		public View_RoleMenu()
		{}
		#region Model
		private int _roleid;
		private string _menuid;
		private int _spras;
		private string _rolename;
		private bool _isroleuse;
		private bool _isadmin;
		private int? _roletype;
		private string _menuname;
		private string _url;
		private string _parentid;
		private int? _orderindex;
		private bool _ishide;
		private bool _ismenuuse;
		/// <summary>
		/// 
		/// </summary>
		public int RoleID
		{
			set{ _roleid=value;}
			get{return _roleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MenuID
		{
			set{ _menuid=value;}
			get{return _menuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int spras
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RoleName
		{
			set{ _rolename=value;}
			get{return _rolename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsRoleUse
		{
			set{ _isroleuse=value;}
			get{return _isroleuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsAdmin
		{
			set{ _isadmin=value;}
			get{return _isadmin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RoleType
		{
			set{ _roletype=value;}
			get{return _roletype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MenuName
		{
			set{ _menuname=value;}
			get{return _menuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string URL
		{
			set{ _url=value;}
			get{return _url;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParentID
		{
			set{ _parentid=value;}
			get{return _parentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? OrderIndex
		{
			set{ _orderindex=value;}
			get{return _orderindex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsHide
		{
			set{ _ishide=value;}
			get{return _ishide;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsMenuUse
		{
			set{ _ismenuuse=value;}
			get{return _ismenuuse;}
		}
		#endregion Model

	}
}

