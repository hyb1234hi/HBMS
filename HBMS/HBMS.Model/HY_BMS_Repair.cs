﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Repair
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:17   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_Repair:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_Repair
	{
		public HY_BMS_Repair()
		{}
		#region Model
		private string _id;
		private string _airportid;
		private string _equipmentid;
		private int? _systemid;
		private int? _partsid;
		private string _repairuse;
		private string _checkuser;
		private DateTime? _repairdate;
		private string _usercode;
		private DateTime? _subdate;
		private string _contect;
		private string _memo;
		/// <summary>
		/// 
		/// </summary>
		public string ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SystemID
		{
			set{ _systemid=value;}
			get{return _systemid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PartsID
		{
			set{ _partsid=value;}
			get{return _partsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RepairUse
		{
			set{ _repairuse=value;}
			get{return _repairuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CheckUser
		{
			set{ _checkuser=value;}
			get{return _checkuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? RepairDate
		{
			set{ _repairdate=value;}
			get{return _repairdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserCode
		{
			set{ _usercode=value;}
			get{return _usercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SubDate
		{
			set{ _subdate=value;}
			get{return _subdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Contect
		{
			set{ _contect=value;}
			get{return _contect;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string memo
		{
			set{ _memo=value;}
			get{return _memo;}
		}
		#endregion Model

	}
}

