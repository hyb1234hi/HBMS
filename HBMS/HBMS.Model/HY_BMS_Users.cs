﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Users
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/2/13 2:00:09   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_Users:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_Users
	{
		public HY_BMS_Users()
		{}
		#region Model
		private string _usercode;
		private string _username;
		private string _cname;
		private string _ename;
		private string _password;
		private string _phoneno;
		private string _email;
		private int? _sex;
		private string _captcha;
		private DateTime? _captchadatetime;
		private string _defaultspras;
		private bool _isuse;
		private string _defaultairportid;
		/// <summary>
		/// 
		/// </summary>
		public string UserCode
		{
			set{ _usercode=value;}
			get{return _usercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CName
		{
			set{ _cname=value;}
			get{return _cname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EName
		{
			set{ _ename=value;}
			get{return _ename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PassWord
		{
			set{ _password=value;}
			get{return _password;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PhoneNo
		{
			set{ _phoneno=value;}
			get{return _phoneno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Sex
		{
			set{ _sex=value;}
			get{return _sex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Captcha
		{
			set{ _captcha=value;}
			get{return _captcha;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? Captchadatetime
		{
			set{ _captchadatetime=value;}
			get{return _captchadatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string defaultSpras
		{
			set{ _defaultspras=value;}
			get{return _defaultspras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DefaultAirPortID
		{
			set{ _defaultairportid=value;}
			get{return _defaultairportid;}
		}
		#endregion Model

	}
}

