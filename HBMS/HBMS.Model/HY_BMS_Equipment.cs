﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Equipment
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 23:25:33   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_Equipment:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_Equipment
	{
		public HY_BMS_Equipment()
		{}
		#region Model
		private int _equipmentid;
		private string _equipmentcode;
		private string _airportid;
		private DateTime? _productdate;
		private DateTime? _userddate;
		private int? _guarantee;
		private string _airportpromptuser;
		private string _airportpromptphone;
		private string _airportwarninguser;
		private string _airportwarningphone;
		private string _airporterroruser;
		private string _airporterrorphone;
		private string _companypromptuser;
		private string _companypromptphone;
		private string _companywarninguser;
		private string _companywarningphone;
		private string _companyerroruser;
		private string _companyerrorphone;
		private string _memo;
		private bool _isuse;
		/// <summary>
		/// 
		/// </summary>
		public int EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentCode
		{
			set{ _equipmentcode=value;}
			get{return _equipmentcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ProductDate
		{
			set{ _productdate=value;}
			get{return _productdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? UserdDate
		{
			set{ _userddate=value;}
			get{return _userddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Guarantee
		{
			set{ _guarantee=value;}
			get{return _guarantee;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortPromptUser
		{
			set{ _airportpromptuser=value;}
			get{return _airportpromptuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortPromptPhone
		{
			set{ _airportpromptphone=value;}
			get{return _airportpromptphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortWarningUser
		{
			set{ _airportwarninguser=value;}
			get{return _airportwarninguser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortWarningPhone
		{
			set{ _airportwarningphone=value;}
			get{return _airportwarningphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortErrorUser
		{
			set{ _airporterroruser=value;}
			get{return _airporterroruser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortErrorPhone
		{
			set{ _airporterrorphone=value;}
			get{return _airporterrorphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CompanyPromptUser
		{
			set{ _companypromptuser=value;}
			get{return _companypromptuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CompanyPromptPhone
		{
			set{ _companypromptphone=value;}
			get{return _companypromptphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CompanyWarningUser
		{
			set{ _companywarninguser=value;}
			get{return _companywarninguser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CompanyWarningPhone
		{
			set{ _companywarningphone=value;}
			get{return _companywarningphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CompanyErrorUser
		{
			set{ _companyerroruser=value;}
			get{return _companyerroruser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CompanyErrorPhone
		{
			set{ _companyerrorphone=value;}
			get{return _companyerrorphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Memo
		{
			set{ _memo=value;}
			get{return _memo;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		#endregion Model

	}
}

