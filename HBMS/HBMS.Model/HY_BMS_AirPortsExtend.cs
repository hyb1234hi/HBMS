﻿// 
// 创建人：宋欣
// 创建时间：2015-02-01
// 功能：机场自增类
// Copyright (c) 2014 宋欣. All rights reserved.
// 

using System.Collections.Generic;

namespace HBMS.Model
{
    public partial class HY_BMS_AirPorts
    {
        private List<HY_BMS_AirPortDesc> _airPortDescsCollection = new List<HY_BMS_AirPortDesc>();

        /// <summary>
        ///     机场描述集合
        /// </summary>
        public List<HY_BMS_AirPortDesc> AirPortDescsCollection
        {
            get { return this._airPortDescsCollection; }
            set { this._airPortDescsCollection = value; }
        }
    }
}