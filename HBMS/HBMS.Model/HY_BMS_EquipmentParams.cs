﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace HBMS.Model{
	 	//HY_BMS_EquipmentParams
		public class HY_BMS_EquipmentParams
	{
   		     
      	/// <summary>
		/// ID
        /// </summary>		
		private int _id;
        public int ID
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// EquipmentID
        /// </summary>		
		private string _equipmentid;
        public string EquipmentID
        {
            get{ return _equipmentid; }
            set{ _equipmentid = value; }
        }        
		/// <summary>
		/// ParamID
        /// </summary>		
		private string _paramid;
        public string ParamID
        {
            get{ return _paramid; }
            set{ _paramid = value; }
        }        
		/// <summary>
		/// SubDatetime
        /// </summary>		
		private DateTime _subdatetime;
        public DateTime SubDatetime
        {
            get{ return _subdatetime; }
            set{ _subdatetime = value; }
        }        
		/// <summary>
		/// OrderIndex
        /// </summary>		
		private int _orderindex;
        public int OrderIndex
        {
            get{ return _orderindex; }
            set{ _orderindex = value; }
        }        
		   
	}
}

