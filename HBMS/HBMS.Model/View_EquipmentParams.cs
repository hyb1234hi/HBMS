﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace HBMS.Model{
	 	//View_EquipmentParams
		public class View_EquipmentParams
	{
   		     
      	/// <summary>
		/// ID
        /// </summary>		
		private int _id;
        public int ID
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// SubDatetime
        /// </summary>		
		private DateTime _subdatetime;
        public DateTime SubDatetime
        {
            get{ return _subdatetime; }
            set{ _subdatetime = value; }
        }        
		/// <summary>
		/// OrderIndex
        /// </summary>		
		private int _orderindex;
        public int OrderIndex
        {
            get{ return _orderindex; }
            set{ _orderindex = value; }
        }        
		/// <summary>
		/// AirPortID
        /// </summary>		
		private string _airportid;
        public string AirPortID
        {
            get{ return _airportid; }
            set{ _airportid = value; }
        }        
		/// <summary>
		/// EquipmentID
        /// </summary>		
		private string _equipmentid;
        public string EquipmentID
        {
            get{ return _equipmentid; }
            set{ _equipmentid = value; }
        }        
		/// <summary>
		/// EquipmentName
        /// </summary>		
		private string _equipmentname;
        public string EquipmentName
        {
            get{ return _equipmentname; }
            set{ _equipmentname = value; }
        }        
		/// <summary>
		/// SPRAS
        /// </summary>		
		private int _spras;
        public int SPRAS
        {
            get{ return _spras; }
            set{ _spras = value; }
        }        
		/// <summary>
		/// ParamID
        /// </summary>		
		private string _paramid;
        public string ParamID
        {
            get{ return _paramid; }
            set{ _paramid = value; }
        }        
		/// <summary>
		/// ParamName
        /// </summary>		
		private string _paramname;
        public string ParamName
        {
            get{ return _paramname; }
            set{ _paramname = value; }
        }        
		/// <summary>
		/// ParamTypeID
        /// </summary>		
		private int _paramtypeid;
        public int ParamTypeID
        {
            get{ return _paramtypeid; }
            set{ _paramtypeid = value; }
        }        
		/// <summary>
		/// boolValue
        /// </summary>		
		private decimal _boolvalue;
        public decimal boolValue
        {
            get{ return _boolvalue; }
            set{ _boolvalue = value; }
        }        
		/// <summary>
		/// PromptMin
        /// </summary>		
		private decimal _promptmin;
        public decimal PromptMin
        {
            get{ return _promptmin; }
            set{ _promptmin = value; }
        }        
		/// <summary>
		/// PromptMax
        /// </summary>		
		private decimal _promptmax;
        public decimal PromptMax
        {
            get{ return _promptmax; }
            set{ _promptmax = value; }
        }        
		/// <summary>
		/// WarningMin
        /// </summary>		
		private decimal _warningmin;
        public decimal WarningMin
        {
            get{ return _warningmin; }
            set{ _warningmin = value; }
        }        
		/// <summary>
		/// WarningMax
        /// </summary>		
		private decimal _warningmax;
        public decimal WarningMax
        {
            get{ return _warningmax; }
            set{ _warningmax = value; }
        }        
		/// <summary>
		/// ErrorMin
        /// </summary>		
		private decimal _errormin;
        public decimal ErrorMin
        {
            get{ return _errormin; }
            set{ _errormin = value; }
        }        
		/// <summary>
		/// ErrorMax
        /// </summary>		
		private decimal _errormax;
        public decimal ErrorMax
        {
            get{ return _errormax; }
            set{ _errormax = value; }
        }        
		/// <summary>
		/// PartsID
        /// </summary>		
		private int _partsid;
        public int PartsID
        {
            get{ return _partsid; }
            set{ _partsid = value; }
        }        
		/// <summary>
		/// PartsName
        /// </summary>		
		private string _partsname;
        public string PartsName
        {
            get{ return _partsname; }
            set{ _partsname = value; }
        }        
		/// <summary>
		/// SystemID
        /// </summary>		
		private int _systemid;
        public int SystemID
        {
            get{ return _systemid; }
            set{ _systemid = value; }
        }        
		/// <summary>
		/// SystemName
        /// </summary>		
		private string _systemname;
        public string SystemName
        {
            get{ return _systemname; }
            set{ _systemname = value; }
        }        
		   
	}
}

