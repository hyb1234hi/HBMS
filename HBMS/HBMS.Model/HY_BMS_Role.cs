﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Role
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:18   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_Role:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_Role
	{
		public HY_BMS_Role()
		{}
		#region Model
		private int _roleid;
		private bool _isuse;
		private bool _isadmin;
		private int? _type;
		/// <summary>
		/// 
		/// </summary>
		public int RoleID
		{
			set{ _roleid=value;}
			get{return _roleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsAdmin
		{
			set{ _isadmin=value;}
			get{return _isadmin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Type
		{
			set{ _type=value;}
			get{return _type;}
		}
		#endregion Model

	}
}

