﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_AirPortDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 22:59:02   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_AirPortDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_AirPortDesc
	{
		public HY_BMS_AirPortDesc()
		{}
		#region Model
		private int _airportid;
		private int _spras;
		private string _airportname;
		private string _airportaddress;
		/// <summary>
		/// 
		/// </summary>
		public int AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortName
		{
			set{ _airportname=value;}
			get{return _airportname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortAddress
		{
			set{ _airportaddress=value;}
			get{return _airportaddress;}
		}
		#endregion Model

	}
}

