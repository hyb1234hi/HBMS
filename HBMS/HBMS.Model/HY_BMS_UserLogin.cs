﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_UserLogin
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:20   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_UserLogin:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_UserLogin
	{
		public HY_BMS_UserLogin()
		{}
		#region Model
		private string _guid;
		private string _usercode;
		private string _mac;
		private string _ip;
		private int? _spras;
		private DateTime? _logindatetime;
		private DateTime? _modifydatetime;
		private DateTime? _quitdatetime;
		/// <summary>
		/// 
		/// </summary>
		public string GUID
		{
			set{ _guid=value;}
			get{return _guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserCode
		{
			set{ _usercode=value;}
			get{return _usercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MAC
		{
			set{ _mac=value;}
			get{return _mac;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IP
		{
			set{ _ip=value;}
			get{return _ip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LoginDateTime
		{
			set{ _logindatetime=value;}
			get{return _logindatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ModifyDateTime
		{
			set{ _modifydatetime=value;}
			get{return _modifydatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? QuitDateTime
		{
			set{ _quitdatetime=value;}
			get{return _quitdatetime;}
		}
		#endregion Model

	}
}

