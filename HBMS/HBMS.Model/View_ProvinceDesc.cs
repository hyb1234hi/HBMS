﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_ProvinceDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/2/13 0:48:59   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_ProvinceDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_ProvinceDesc
	{
		public View_ProvinceDesc()
		{}
		#region Model
		private string _provinceid;
		private string _provincename;
		private string _countryid;
		private bool _isuse;
		private int _spras;
		/// <summary>
		/// 
		/// </summary>
		public string ProvinceID
		{
			set{ _provinceid=value;}
			get{return _provinceid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProvinceName
		{
			set{ _provincename=value;}
			get{return _provincename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CountryID
		{
			set{ _countryid=value;}
			get{return _countryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		#endregion Model

	}
}

