﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_PartsDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/2/4 0:45:03   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_PartsDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_PartsDesc
	{
		public View_PartsDesc()
		{}
		#region Model
		private int _partsid;
		private string _partsname;
		private int _spras;
		private int _systemid;
		private string _systemname;
		private int? _orderindex;
		private bool _isuse;
		/// <summary>
		/// 
		/// </summary>
		public int PartsID
		{
			set{ _partsid=value;}
			get{return _partsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PartsName
		{
			set{ _partsname=value;}
			get{return _partsname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SystemID
		{
			set{ _systemid=value;}
			get{return _systemid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SystemName
		{
			set{ _systemname=value;}
			get{return _systemname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? OrderIndex
		{
			set{ _orderindex=value;}
			get{return _orderindex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		#endregion Model

	}
}

