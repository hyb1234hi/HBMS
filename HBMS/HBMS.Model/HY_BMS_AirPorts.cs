﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_AirPorts
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 22:57:08   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_AirPorts:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_AirPorts
	{
		public HY_BMS_AirPorts()
		{}
		#region Model
		private int _airportid;
		private string _airportcode;
		private string _countryid;
		private string _provinceid;
		private string _cityid;
		private string _dimension;
		private string _longitude;
		private string _head;
		private string _headphone;
		private string _heademail;
		/// <summary>
		/// 
		/// </summary>
		public int AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortCode
		{
			set{ _airportcode=value;}
			get{return _airportcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CountryID
		{
			set{ _countryid=value;}
			get{return _countryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProvinceID
		{
			set{ _provinceid=value;}
			get{return _provinceid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CityID
		{
			set{ _cityid=value;}
			get{return _cityid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Dimension
		{
			set{ _dimension=value;}
			get{return _dimension;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Longitude
		{
			set{ _longitude=value;}
			get{return _longitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Head
		{
			set{ _head=value;}
			get{return _head;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string HeadPhone
		{
			set{ _headphone=value;}
			get{return _headphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string HeadEmail
		{
			set{ _heademail=value;}
			get{return _heademail;}
		}
		#endregion Model

	}
}

