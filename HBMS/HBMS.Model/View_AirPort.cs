﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_AirPort
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/1/29 1:40:22   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_AirPort:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_AirPort
	{
		public View_AirPort()
		{}
		#region Model
		private string _airportid;
		private string _countryid;
		private string _provinceid;
		private string _cityid;
		private string _address;
		private string _dimension;
		private string _longitude;
		private string _head;
		private string _headphone;
		private string _heademail;
		private int _spras;
		private string _airportname;
		private string _countryname;
		private string _provincename;
		private string _cityname;
		/// <summary>
		/// 
		/// </summary>
		public string AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CountryID
		{
			set{ _countryid=value;}
			get{return _countryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProvinceID
		{
			set{ _provinceid=value;}
			get{return _provinceid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CityID
		{
			set{ _cityid=value;}
			get{return _cityid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Dimension
		{
			set{ _dimension=value;}
			get{return _dimension;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Longitude
		{
			set{ _longitude=value;}
			get{return _longitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Head
		{
			set{ _head=value;}
			get{return _head;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string HeadPhone
		{
			set{ _headphone=value;}
			get{return _headphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string HeadEmail
		{
			set{ _heademail=value;}
			get{return _heademail;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortName
		{
			set{ _airportname=value;}
			get{return _airportname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CountryName
		{
			set{ _countryname=value;}
			get{return _countryname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProvinceName
		{
			set{ _provincename=value;}
			get{return _provincename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CityName
		{
			set{ _cityname=value;}
			get{return _cityname;}
		}
		#endregion Model

	}
}

