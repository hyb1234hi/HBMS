﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_Param
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/4/8 23:17:32   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_Param:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_Param
	{
		public HY_BMS_Param()
		{}
		#region Model
		private int _paramid;
		private string _paramcode;
		private int? _partsid;
		private int? _paramtypeid;
		private bool _isuse;
		private int? _orderindex;
		private string _memo;
		private decimal? _boolvalue;
		private decimal? _promptmin;
		private decimal? _promptmax;
		private int? _promptmsgfw;
		private decimal? _warningmin;
		private decimal? _warningmax;
		private int? _warningmsgfw;
		private decimal? _errormin;
		private decimal? _errormax;
		private int? _errormsgfw;
		private int? _macparamid;
		private int? _infolevel;
		/// <summary>
		/// 
		/// </summary>
		public int ParamID
		{
			set{ _paramid=value;}
			get{return _paramid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParamCode
		{
			set{ _paramcode=value;}
			get{return _paramcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PartsID
		{
			set{ _partsid=value;}
			get{return _partsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ParamTypeID
		{
			set{ _paramtypeid=value;}
			get{return _paramtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? OrderIndex
		{
			set{ _orderindex=value;}
			get{return _orderindex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Memo
		{
			set{ _memo=value;}
			get{return _memo;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? boolValue
		{
			set{ _boolvalue=value;}
			get{return _boolvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PromptMin
		{
			set{ _promptmin=value;}
			get{return _promptmin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PromptMax
		{
			set{ _promptmax=value;}
			get{return _promptmax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PromptMsgFW
		{
			set{ _promptmsgfw=value;}
			get{return _promptmsgfw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? WarningMin
		{
			set{ _warningmin=value;}
			get{return _warningmin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? WarningMax
		{
			set{ _warningmax=value;}
			get{return _warningmax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? WarningMsgFW
		{
			set{ _warningmsgfw=value;}
			get{return _warningmsgfw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ErrorMin
		{
			set{ _errormin=value;}
			get{return _errormin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ErrorMax
		{
			set{ _errormax=value;}
			get{return _errormax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ErrorMsgFW
		{
			set{ _errormsgfw=value;}
			get{return _errormsgfw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MacParamID
		{
			set{ _macparamid=value;}
			get{return _macparamid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InfoLevel
		{
			set{ _infolevel=value;}
			get{return _infolevel;}
		}
		#endregion Model

	}
}

