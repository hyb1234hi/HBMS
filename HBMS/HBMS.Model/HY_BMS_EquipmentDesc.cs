﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_EquipmentDesc
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/2 23:25:34   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_EquipmentDesc:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_EquipmentDesc
	{
		public HY_BMS_EquipmentDesc()
		{}
		#region Model
		private int _equipmentid;
		private int _spras;
		private string _equipmentname;
		/// <summary>
		/// 
		/// </summary>
		public int EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentName
		{
			set{ _equipmentname=value;}
			get{return _equipmentname;}
		}
		#endregion Model

	}
}

