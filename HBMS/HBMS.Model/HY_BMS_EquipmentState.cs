﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：HY_BMS_EquipmentState
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/9 23:29:12   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// HY_BMS_EquipmentState:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class HY_BMS_EquipmentState
	{
		public HY_BMS_EquipmentState()
		{}
		#region Model
		private int _id;
		private int _equipmentid;
		private DateTime _subdatetime;
		private int _state;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime SubDatetime
		{
			set{ _subdatetime=value;}
			get{return _subdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

