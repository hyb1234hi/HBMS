﻿// 
// 创建人：宋欣
// 创建时间：2015-01-29
// 功能：View_EquipmentParamValuesHistory
// Copyright (c) 2014 宋欣. All rights reserved.
// Ver    变更日期             负责人  变更内容
// ───────────────────────────────────
// V0.01  2015/3/31 0:47:23   N/A    初版
// 
using System;
namespace HBMS.Model
{
	/// <summary>
	/// View_EquipmentParamValuesHistory:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class View_EquipmentParamValuesHistory
	{
		public View_EquipmentParamValuesHistory()
		{}
		#region Model
		private int _id;
		private DateTime? _subdatetime;
		private decimal? _paramvalue;
		private int _paramstate;
		private string _airportid;
		private string _airportcode;
		private string _airportname;
		private string _dimension;
		private string _longitude;
		private int _equipmentid;
		private string _equipmentcode;
		private string _equipmentname;
		private int _spras;
		private int _paramid;
		private string _paramcode;
		private string _paramname;
		private int? _paramtypeid;
		private decimal? _boolvalue;
		private decimal? _promptmin;
		private decimal? _promptmax;
		private decimal? _warningmin;
		private decimal? _warningmax;
		private decimal? _errormin;
		private decimal? _errormax;
		private int _partsid;
		private string _partsname;
		private int _systemid;
		private string _systemname;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SubDatetime
		{
			set{ _subdatetime=value;}
			get{return _subdatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ParamValue
		{
			set{ _paramvalue=value;}
			get{return _paramvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ParamState
		{
			set{ _paramstate=value;}
			get{return _paramstate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortID
		{
			set{ _airportid=value;}
			get{return _airportid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortCode
		{
			set{ _airportcode=value;}
			get{return _airportcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AirPortName
		{
			set{ _airportname=value;}
			get{return _airportname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Dimension
		{
			set{ _dimension=value;}
			get{return _dimension;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Longitude
		{
			set{ _longitude=value;}
			get{return _longitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int EquipmentID
		{
			set{ _equipmentid=value;}
			get{return _equipmentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentCode
		{
			set{ _equipmentcode=value;}
			get{return _equipmentcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EquipmentName
		{
			set{ _equipmentname=value;}
			get{return _equipmentname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SPRAS
		{
			set{ _spras=value;}
			get{return _spras;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ParamID
		{
			set{ _paramid=value;}
			get{return _paramid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParamCode
		{
			set{ _paramcode=value;}
			get{return _paramcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParamName
		{
			set{ _paramname=value;}
			get{return _paramname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ParamTypeID
		{
			set{ _paramtypeid=value;}
			get{return _paramtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? boolValue
		{
			set{ _boolvalue=value;}
			get{return _boolvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PromptMin
		{
			set{ _promptmin=value;}
			get{return _promptmin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PromptMax
		{
			set{ _promptmax=value;}
			get{return _promptmax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? WarningMin
		{
			set{ _warningmin=value;}
			get{return _warningmin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? WarningMax
		{
			set{ _warningmax=value;}
			get{return _warningmax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ErrorMin
		{
			set{ _errormin=value;}
			get{return _errormin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ErrorMax
		{
			set{ _errormax=value;}
			get{return _errormax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int PartsID
		{
			set{ _partsid=value;}
			get{return _partsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PartsName
		{
			set{ _partsname=value;}
			get{return _partsname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SystemID
		{
			set{ _systemid=value;}
			get{return _systemid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SystemName
		{
			set{ _systemname=value;}
			get{return _systemname;}
		}
		#endregion Model

	}
}

